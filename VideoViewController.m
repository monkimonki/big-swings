//
//  VideoViewController.m
//  testswings
//
//  Created by Aleksandr Padalko on 6/24/14.
//  Copyright (c) 2014 Oleksandr Padalko. All rights reserved.
//

#import "VideoViewController.h"
#import <MediaPlayer/MediaPlayer.h>
@interface VideoViewController ()

@end

@implementation VideoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationController setNavigationBarHidden:YES];
    [[UIApplication sharedApplication]setStatusBarHidden:YES];
    NSBundle *bundle = [NSBundle mainBundle];
    NSString *moviePath = [bundle pathForResource:@"SplashE" ofType:@"mp4"];
    NSURL *movieURL = [NSURL fileURLWithPath:moviePath];
    
MPMoviePlayerViewController*   theMoviPlayer = [[MPMoviePlayerViewController alloc] initWithContentURL:movieURL];
//    theMoviPlayer.controlStyle = MPMovieControlStyleFullscreen;
//
    [theMoviPlayer.moviePlayer setControlStyle:MPMovieControlStyleNone];
    
    [theMoviPlayer.view setFrame:self.view.bounds];
   // theMoviPlayer.view.transform = CGAffineTransformConcat(theMoviPlayer.view.transform, CGAffineTransformMakeRotation(M_PI_2));
//    UIWindow *backgroundWindow = [[UIApplication sharedApplication] keyWindow];
//    [theMoviPlayer.view setFrame:backgroundWindow.frame];
//    [backgroundWindow addSubview:theMoviPlayer.view];
   
    [self presentMoviePlayerViewControllerAnimated:theMoviPlayer];
    

    
    
    
    	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
