#define locBigSwingsTitle @"loc.bigswingstitle"
#define locAddPlaceTitle @"loc.addplace"
#define locTitle @"loc.title"
#define locDescr @"loc.descr"
#define locLocation @"loc.location"
#define locHeight @"loc.height"
#define locWarrinig @"loc.warning"
#define locLogin @"loc.Login"
#define locForgotPass @"loc.forgotpass"
#define locPassword @"loc.Password"
#define locRepeatPass @"loc.repeatPass"
#define locRegister @"loc.register"

#define locTopPlaces @"loc.topplaces"

#define locCurLocaction @"loc.curlocation"
#define locAddPhoto @"loc.addPhoto"


#define locSubmit @"loc.submit"
#define locRate @"loc.rate"
#define locOk @"loc.ok"
#define locRated @"loc.rated"

#define locShowGallery @"loc.showGallery"
#define locSendPhoto @"loc.sendPhoto"


#define locRateDepthWater @"loc.rate.depthwater"
#define locRateHeightOfSwings @"loc.rate.heightswings"
#define locRateDangerWarnings @"loc.rate.danger"

#define locNoRating @"loc.norating"

#define locImageVia @"loc.imageVia"
#define locGallery @"loc.gallery"
#define locCamera @"loc.camera"
#define locCancel @"loc.Cancel"


#define locNoPhotos @"loc.nopotos"

#define locUploadOne @"loc.uploadOne"

#define locSearch @"loc.search"
#define locSettings @"loc.settings"
#define locRemoveAds @"loc.removeAds"
#define locRestorePurchase @"loc.restre"
#define locContactUs @"loc.contactUs"
#define locResetPassErorr @"loc.resetpassErorr"
#define locResetYourPass @"loc.reset"
#define locResetPassMessage @"loc.resetPassmessage"
#define locEmail @"loc.email"

#define locOldPass @"loc.oldPass"
#define locRestore @"loc.restore"
#define locLabelRestorePass @"loc.labelrestore"