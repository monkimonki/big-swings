//
//  BSNavigationController.h
//  BigSwings
//
//  Created by Aleksandr Padalko on 6/19/14.
//  Copyright (c) 2014 Oleksandr Padalko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BSNavigationController : UINavigationController

@end
