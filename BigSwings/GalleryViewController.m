//
//  GalleryViewController.m
//  testswings
//
//  Created by mm7 on 06.07.14.
//  Copyright (c) 2014 Oleksandr Padalko. All rights reserved.
//

#import "GalleryViewController.h"
#import "Media.h"
#import "LoadVIew.h"
  #import <MobileCoreServices/UTCoreTypes.h>
@interface GalleryViewController ()<VVImageViewDelagate,UIActionSheetDelegate ,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    UIScrollView *scrollView ;
	
	UIScrollView * sv;
    NSMutableArray * imagesDataArray;
    CGSize botImageSize;
    float botSpace;
    
    int currentPage;
    
    NSMutableArray * botImagesArray;
}

@end

@implementation GalleryViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
    // [backButton setTitle:@"back" forState:UIControlStateNormal];
    [backButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [backButton setImage:[UIImage imageNamed:@"Back_ico.png"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
   
    [self loadElems];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setTitle:@"Gallery"];
}

-(void)loadElems{
    PFQuery * q=[PFQuery queryWithClassName:[Media parseClassName]];
    [q whereKey:@"placeId" equalTo:_place];
    
   LoadVIew * lv= [LoadVIew create];
    // set loading
    [[self.view viewWithTag:1111]removeFromSuperview];
    [[self.view viewWithTag:1112] removeFromSuperview];
    
    [q findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        [lv dissmiss];
        if (!error) {
            
            if (objects.count>0) {
          
                
                NSLog(@"%@",objects);
                imagesDataArray=[[NSMutableArray alloc]initWithArray:objects];
                [self createScroll];
                
                
                
                
            }else{
             
                
                
                
                UILabel * lab=[[UILabel alloc]initWithFrame:CGRectMake(15, 15, self.view.frame.size.width-30,36)];
                [lab setNumberOfLines:3];
                [lab setFont:[UIFont BSFontMediumLight]];
                [lab setTextColor:[UIColor BSDarkBlueColor]];
                [self.view addSubview:lab];
                [lab setTextAlignment:NSTextAlignmentCenter];
                [lab setTag:1111];
                [lab setText:[locNoPhotos localized]];
                
                [lab sizeToFit];
                
                [lab setCenter:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2-30)];
                
                
                UIButton * b=[UIButton buttonWithType:UIButtonTypeCustom];
                
                
                [b setFrame:CGRectMake(0,0, 165, 35)];
                [b setCenter:CGPointMake(self.view.frame.size.width/2,lab.frame.origin.y+lab.frame.size.height+10+35/2)];
                [b setTitle:[[locSendPhoto localized] uppercaseString] forState: UIControlStateNormal];
                [b setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                [b setBackgroundColor:[UIColor BSDarkBlueColor]];
                [b.titleLabel setFont:[UIFont BSFontLargeLight]];
                b.layer.cornerRadius=4;
                [b setTag:1112];
                [b addTarget:self action:@selector(addPhoto) forControlEvents:UIControlEventTouchUpInside];
                [self.view addSubview:b];
            }
        }
        
    }];
}

-(void)createScroll{
    
    botSpace=3;
    botImageSize=CGSizeMake(77, 77);
    
    
    int numberOfPages = imagesDataArray.count ;
    scrollView=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-8-176/2)];
    [scrollView setDelegate:self];
    // define the scroll view content size and enable paging
	[scrollView setPagingEnabled: YES] ;
	[scrollView setContentSize: CGSizeMake(scrollView.bounds.size.width * numberOfPages, scrollView.bounds.size.height)] ;
	[self.view addSubview:scrollView];
	// programmatically add the page control
	   
	sv=[[UIScrollView alloc]initWithFrame:CGRectMake(0, scrollView.frame.size.height+8, self.view.frame.size.width, 162/2)];
    
    [sv setScrollEnabled:NO];
	[self.view addSubview:sv];
    botImagesArray=[[NSMutableArray alloc]init];
    
    float botSize=0;
	
	for (int i = 0 ; i < imagesDataArray.count ; i++)
	{
        
        VVImageView * imgv=[[VVImageView alloc]initWithFrame:CGRectMake(i * scrollView.bounds.size.width+8, 8, scrollView.bounds.size.width-16, scrollView.bounds.size.height-2) ];
        Media * m=imagesDataArray[i];
        [imgv setContentMode:UIViewContentModeCenter];
		// determine the frame of the current page
		imgv.file=m.image;
		// add it to the scroll view
		[scrollView addSubview: imgv] ;
		[imgv setDelegate:self];
        [imgv setClipsToBounds:YES];
		imgv.tag=i;
//        imgv.layer.borderWidth=1.0;
//        imgv.layer.borderColor=[UIColor BSDarkBlueColor].CGColor;
		// determine and set its (random) background color
	
        UIButton * v=[UIButton buttonWithType:UIButtonTypeCustom];
        v.tag=i;
        [v setFrame:CGRectMake(3+(154/2+3)*i, 0, 77, 77)];
       [v setClipsToBounds:YES];
        v.layer.borderWidth=1.0;
        v.layer.borderColor=[UIColor BSDarkBlueColor].CGColor;
        [v addTarget:self action:@selector(buttonTiuched:) forControlEvents:UIControlEventTouchUpInside];
        UIView * b=[[UIView alloc]initWithFrame:v.bounds];
        [b setBackgroundColor:[UIColor colorWith8BitRed:0 green:0 blue:0 alpha:0.9]];
        [b setUserInteractionEnabled:NO];
        [v addSubview:b];
        [b setTag:100];
        
        
        botSize=v.frame.origin.x+v.frame.size.width;
       [[v imageView] setContentMode:UIViewContentModeScaleAspectFill];
        [botImagesArray addObject:v];
        [sv addSubview:v];
	}
    
    [sv setContentSize:CGSizeMake(botSize, sv.frame.size.height)];
   
    [sv setContentInset:UIEdgeInsetsMake(0, self.view.frame.size.width/2-77/2, 0, 0)];
    currentPage=0;
    [self setBotOffset:0];
    
                    [[botImagesArray[currentPage] viewWithTag:100] setAlpha:0];
    
}

-(void)setBotOffset:(int)idx{
        [sv setContentOffset: CGPointMake(- self.view.frame.size.width/2+botImageSize.width/2+(botImageSize.width+botSpace)*idx, scrollView.contentOffset.y) animated: YES] ;
    
}
-(void)back{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark -
#pragma mark DDPageControl triggered actions

- (void)pageControlClicked:(id)sender
{
//DDPageControl *thePageControl = (DDPageControl *)sender ;
	
//	// we need to scroll to the new index
//	[scrollView setContentOffset: CGPointMake(scrollView.bounds.size.width * thePageControl.currentPage, scrollView.contentOffset.y) animated: YES] ;
}


#pragma mark -
#pragma mark UIScrollView delegate methods

- (void)scrollViewDidScroll:(UIScrollView *)aScrollView
{
	CGFloat pageWidth = scrollView.bounds.size.width ;
    float fractionalPage = scrollView.contentOffset.x / pageWidth ;
	NSInteger nearestNumber = lround(fractionalPage) ;
	NSLog(@"%d",nearestNumber);
	if (currentPage!= nearestNumber)
	{
        
        [UIView animateWithDuration:0.2 animations:^{
            [[botImagesArray[currentPage] viewWithTag:100] setAlpha:1.0];
                   [[botImagesArray[nearestNumber] viewWithTag:100] setAlpha:0];
        }];
		currentPage = nearestNumber ;
        [self setBotOffset:currentPage];
		
	}
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)aScrollView
{
	// if we are animating (triggered by clicking on the page control), we update the page control
//	[pageControl updateCurrentPageDisplay] ;
}







- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning] ;
}
-(void)buttonTiuched:(UIButton*)but{
    
    [scrollView setContentOffset: CGPointMake(scrollView.bounds.size.width * but.tag, scrollView.contentOffset.y) animated: YES] ;
    
}
#pragma  mark - vvimageview delegate

-(void)vvImageView:(VVImageView *)vvImageView didLoadImage:(UIImage *)image{
    
    UIButton * imgv=botImagesArray[vvImageView.tag];
    
    [imgv setImage:image forState:UIControlStateNormal];
    
    
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
    
    
}
-(void)addPhoto{
    BOOL cameraDeviceAvailable = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
    BOOL photoLibraryAvailable = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary];
    
    if (cameraDeviceAvailable && photoLibraryAvailable) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:[locImageVia localized] delegate:self cancelButtonTitle:[locCancel localized] destructiveButtonTitle:nil otherButtonTitles:[locCamera localized], [locGallery localized], nil];
        [actionSheet showInView:self.view];
    } else {
        // if we don't have at least two options, we automatically show whichever is available (camera or roll)
        [self shouldPresentPhotoCaptureController];
    }
}

#pragma mark - PAPTabBarController

- (BOOL)shouldPresentPhotoCaptureController {
    BOOL presentedPhotoCaptureController = [self shouldStartCameraController];
    
    if (!presentedPhotoCaptureController) {
        presentedPhotoCaptureController = [self shouldStartPhotoLibraryPickerController];
    }
    
    return presentedPhotoCaptureController;
}
#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    
    
    if (buttonIndex == 0) {
        [self shouldStartCameraController];
    } else if (buttonIndex == 1) {
        [self shouldStartPhotoLibraryPickerController];
    }
    
}
- (BOOL)shouldStartCameraController {
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] == NO) {
        return NO;
    }
    
    UIImagePickerController *cameraUI = [[UIImagePickerController alloc] init];
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]
        && [[UIImagePickerController availableMediaTypesForSourceType:
             UIImagePickerControllerSourceTypeCamera] containsObject:(NSString *)kUTTypeImage]) {
        
        cameraUI.mediaTypes = [NSArray arrayWithObject:(NSString *) kUTTypeImage];
        cameraUI.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        if ([UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceRear]) {
            cameraUI.cameraDevice = UIImagePickerControllerCameraDeviceRear;
        } else if ([UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceFront]) {
            cameraUI.cameraDevice = UIImagePickerControllerCameraDeviceFront;
        }
        
    } else {
        return NO;
    }
    
    cameraUI.allowsEditing = YES;
    cameraUI.showsCameraControls = YES;
    cameraUI.delegate = self;
    
    [self presentViewController:cameraUI animated:YES completion:nil];
    
    return YES;
}


- (BOOL)shouldStartPhotoLibraryPickerController {
    if (([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary] == NO
         && [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum] == NO)) {
        return NO;
    }
    
    UIImagePickerController *cameraUI = [[UIImagePickerController alloc] init];
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]
        && [[UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypePhotoLibrary] containsObject:(NSString *)kUTTypeImage]) {
        
        cameraUI.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        cameraUI.mediaTypes = [NSArray arrayWithObject:(NSString *) kUTTypeImage];
        
    } else if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum]
               && [[UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeSavedPhotosAlbum] containsObject:(NSString *)kUTTypeImage]) {
        
        cameraUI.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        cameraUI.mediaTypes = [NSArray arrayWithObject:(NSString *) kUTTypeImage];
        
    } else {
        return NO;
    }
    
    cameraUI.allowsEditing = YES;
    
    cameraUI.delegate = self;
    
    [self presentViewController:cameraUI animated:YES completion:nil];
    
    return YES;
}
#pragma mark - UIImagePickerDelegate

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [self dismissViewControllerAnimated:NO completion:nil];
    
    UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
    
    
    LoadVIew* loadView=[LoadVIew create];
    
    
    
    Media *m=[[Media alloc]init];
    m.userId=[PFUser currentUser];
    m.placeId=_place;
    m.image=[PFFile fileWithData:UIImageJPEGRepresentation(image, 0.5)];
    
    
    [m saveInBackgroundWithBlock:^(BOOL suc,NSError *err){
        
        [loadView dissmiss];
        
        if (suc) {
            [self loadElems];
        }else{
            [self back];
        }
        
    }];
    
    
    
}

@end
