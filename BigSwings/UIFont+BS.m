//
//  UIFont+BS.m
//  testswings
//
//  Created by Aleksandr Padalko on 6/24/14.
//  Copyright (c) 2014 Oleksandr Padalko. All rights reserved.
//

#import "UIFont+BS.h"

@implementation UIFont (BS)

+(UIFont *)BSFontExstraSmallLight{
    
    return [UIFont fontWithName:@"Helvetica-Light" size:9];
    
}
+(UIFont *)BSFontSmallLight{
    
    return [UIFont fontWithName:@"Helvetica-Light" size:10];
    
}
+(UIFont *)BSFontSmallLightOblique{
    
    return [UIFont fontWithName:@"Helvetica-LightOblique" size:10];
    
}
+(UIFont *)BSFontMediumLight{
    
    return [UIFont fontWithName:@"Helvetica-Light" size:12];
    
}
+(UIFont *)BSFontMediumLightOblique{
    
    return [UIFont fontWithName:@"Helvetica-LightOblique" size:12];
    
}
+(UIFont *)BSFontLargeLight{
    
    return [UIFont fontWithName:@"Helvetica-Light" size:14];
    
}
+(UIFont *)BSFontLargeLightOblique{
    
    return [UIFont fontWithName:@"Helvetica-LightOblique" size:14];
    
}
@end
//sizes:
//28 Light
//28  Light Oblique
//24 Light
//24  Light Oblique
//20 Light
//20 Light Oblique
//18 light
//It's all...
