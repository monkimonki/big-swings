//
//  BSButton.m
//  testswings
//
//  Created by Aleksandr Padalko on 6/20/14.
//  Copyright (c) 2014 Oleksandr Padalko. All rights reserved.
//

#import "BSButton.h"
@interface BSButton()
@end
@implementation BSButton
+(id)create{
    
    BSButton * b=[BSButton buttonWithType:UIButtonTypeCustom];
    return b;
    
    
}


-(void)load{
    UIActivityIndicatorView * av=[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    
    [av setCenter:CGPointMake(self.frame.size.width/2, self.frame.size.height/2)];
    [av setTag:111];
    [av startAnimating];
    [self setTitle:@"" forState:UIControlStateNormal];
    [self addSubview:av];
    [self setUserInteractionEnabled:NO];
    
}
-(void)stopLoad{
 
    [self setUserInteractionEnabled:YES];
    [[self viewWithTag:111] removeFromSuperview];
    
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
