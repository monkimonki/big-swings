//
//  ELKAppDelegate.h
//  BigSwings
//
//  Created by Oleksandr Borovok on 03.06.14.
//  Copyright (c) 2014 Oleksandr Padalko. All rights reserved.
//

#import <GoogleMaps/GoogleMaps.h>

@interface ELKAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
