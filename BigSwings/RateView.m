//
//  RateView.m
//  testswings
//
//  Created by mm7 on 06.07.14.
//  Copyright (c) 2014 Oleksandr Padalko. All rights reserved.
//

#import "RateView.h"
#import "LineView.h"

#import "KramRatingView.h"
@interface RateView()
{
    NSMutableArray * ratersArray;
}
@end
@implementation RateView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        float singleSize=self.frame.size.height/3.0;
        
        
        float offset=15;
        ratersArray=[[NSMutableArray alloc]init];
        NSMutableArray * arr=[[NSMutableArray alloc]initWithObjects:locRateDepthWater,locRateHeightOfSwings,locRateDangerWarnings, nil];
        
        for(int a=0 ;a<arr.count;a++){
            
            NSString * name=arr[a];
            UILabel * label=[[UILabel alloc]initWithFrame:CGRectMake(offset, singleSize * a, 185, singleSize)];
            [label setTextColor:[UIColor BSDarkBlueColor]];
            [label setNumberOfLines:2];
            [label setFont:[UIFont BSFontMediumLight]];
            
            [label setText:[name localized]];
            [self addSubview:label];
            
            LineView * lv=[[LineView alloc]initWithFrame:CGRectMake(0, singleSize*(a+1)-0.5, self.frame.size.width, 0.5)];
            [self addSubview:lv];
            
            
            KramRatingView * krm=[[KramRatingView alloc]initWithFrame:CGRectMake(self.frame.size.width-offset-100, singleSize*a, 100, singleSize)];
            [krm setRaiting:3];
            [self addSubview:krm
             ];
            
            [ratersArray addObject:krm];
            
        }
        
        
        
    }
    return self;
}
-(int)getAllRating{
    
    int r=0;
    
    for (KramRatingView * krm in ratersArray) {
        r+=krm.raiting;
    }
    return r;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
