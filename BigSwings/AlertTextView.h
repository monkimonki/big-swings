//
//  AlertTextView.h
//  testswings
//
//  Created by Aleksandr Padalko on 6/20/14.
//  Copyright (c) 2014 Oleksandr Padalko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SZTextView.h"
@protocol AlertTextViewDelegate<NSObject>
-(void)textFieldShouldTabBack:(UITextField *)tf;
-(void)textFieldShouldTabForward:(UITextField *)tf;
@end
@interface AlertTextView : SZTextView
@property (assign,nonatomic)id<AlertTextViewDelegate> alertDelegate;
@property (retain,nonatomic)NSString * group;



@end
