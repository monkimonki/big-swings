//
//  TopPlacesViewController.m
//  bigswings
//
//  Created by mm7 on 12.07.14.
//  Copyright (c) 2014 Oleksandr Padalko. All rights reserved.
//

#import "TopPlacesViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "Place.h"
#import <Parse/Parse.h>
#import "VVListTableView.h"
#import "PlaceTableCell.h"
#import "PlaceDetailsViewController.h"
#import "LineView.h"
@interface MyMarker1 : GMSMarker
@property (nonatomic)int idx;

@end
@implementation MyMarker1



@end
@interface TopPlacesViewController ()<VVListTableViewDelegate,GMSMapViewDelegate,VVListTableViewDelegate>{
GMSMapView *mapView;
GMSCameraPosition *camera;
VVListTableView * tablePlaceView;
    NSMutableArray * curArray;
}
@end

@implementation TopPlacesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setTitle:@"Top Places"];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor whiteColor]];
 
    
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
    // [backButton setTitle:@"back" forState:UIControlStateNormal];
    [backButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [backButton setImage:[UIImage imageNamed:@"Back_ico.png"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    
    
    PFQuery * p=[PFQuery queryWithClassName:@"Place"];
    p.limit=5;
    [p orderByDescending:@"points"];
    [p findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
       
        if (objects.count>0) {
            
            NSMutableArray * arr=[[NSMutableArray alloc]init];
            for (Place * place  in objects) {
                
                place.realDistance=[[DataSingleton sharedInstance] getDistanceToPoint:place.location];
                place.rated=nil;
                [arr addObject:place];
                
                
            }

            curArray=arr;
            camera = [GMSCameraPosition cameraWithLatitude:49.868
                                                 longitude:-76.2086
                                                      zoom:1];
            mapView = [GMSMapView mapWithFrame:CGRectZero camera:camera];
            [mapView setMapType:kGMSTypeHybrid];
            [mapView setDelegate:self];
            [mapView setFrame:CGRectMake(0, 0, self.view.frame.size.width, 200)];
            [self.view  addSubview:mapView];
            LineView * lv=[[LineView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 1)];
            [lv setColor:[UIColor blackColor]];
            [self.view addSubview:lv];
            for (int a=0;a<arr.count;a++) {
                Place * place=(Place*)[arr objectAtIndex:a];
                
                MyMarker1 *marker = [[MyMarker1 alloc] init];
                marker.position = CLLocationCoordinate2DMake(place.location.latitude, place.location.longitude);
                if (place.textAddres.length>0) {
                    marker.snippet = [NSString stringWithFormat:@"%@\n%@",place.title,place.textAddres];
                }else{
                    marker.snippet = [NSString stringWithFormat:@"%@",place.title];
                }
                
                marker.appearAnimation = kGMSMarkerAnimationPop;
                marker.map = mapView;
                marker.icon = [UIImage imageNamed:@"point"];
                marker.idx=a;
                
            }
            
            
            tablePlaceView=[[VVListTableView alloc]initWithFrame:CGRectMake(0, mapView.frame.size.height+mapView.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height-mapView.frame.size.height-mapView.frame.origin.y)];
            [tablePlaceView insertData:arr toStaticCellAtSection:0];
            [self.view addSubview:tablePlaceView];
            [tablePlaceView setPullToRefreshEnabled:NO];
            LineView * lv2=[[LineView alloc]initWithFrame:CGRectMake(0, tablePlaceView.frame.origin.y-1, self.view.frame.size.width, 1)];
            [lv2 setColor:[UIColor blackColor]];
            [self.view addSubview:lv2];
            [tablePlaceView setVvListTableViewDelegate:self];
        }else{
       UIAlertView * av=     [[UIAlertView alloc]initWithTitle:@"" message:@"There are no places" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [av show];
            [self back];
        }
        
        
    }];
    

}
-(void)back{
    [UIView  beginAnimations:nil context:NULL];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:0.75];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:self.navigationController.view cache:NO];
    [UIView commitAnimations];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelay:0.375];
    [self.navigationController popViewControllerAnimated:YES];
    [UIView commitAnimations];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - listTableViewDelegate
-(CGFloat)listTableView:(UITableView *)tableView heightForLoadCellAtIndexPath:(NSIndexPath *)indexPath{
    
    
    return 20;
}
-(CGFloat)listTableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 51;
}
-(CGFloat)listTableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0;
}
-(void)listTableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath withObject:(Place *)object{
    
    PlaceDetailsViewController * pld=[[PlaceDetailsViewController alloc]init];
    pld.place=object;
    
    [self.navigationController pushViewController:pld animated:YES];
    
    
}
-(void)listTableViewDidLoadData:(id)data forHeader:(NSUInteger)section{
    
    NSLog(@"%@",data);
    
}
-(UIView *)listTableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    
    return nil;
}
-(CGFloat)listTableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0;
}
-(UIView *)listTableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section withObject:(id)object{
    return nil;
}
-(void)listTableView:(UITableView*)listTableView refresh:(void (^)( BOOL success ) )cb{
    
    cb(YES);
}
-(UITableViewCell *)listTableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath objectForRow:(VVObject *)object {
	NSLog(@"delegate open");
    NSLog(@"%@",object.objectId);
    static NSString *CellIdentifer = @"simpleCell";
	
	PlaceTableCell  * cell=[tableView dequeueReusableCellWithIdentifier:CellIdentifer];
	
	if (!cell) {
        [cell setBackgroundColor:[UIColor orangeColor]];
		cell=[[PlaceTableCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifer];
	}
    [cell setPlace:object andIndex:indexPath.row];
	return  cell;
	
}
-(VVParametrsObject *)paramsForLoad:(VVParametrsObject*)params forIndexPath:(NSIndexPath*)indexPath {
	
	return params;
}
-(UITableViewCell *)listTableView:(UITableView *)tableView loadingCellViewForRowAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *CellIdentifer = @"loadCell";
	
	UITableViewCell  * cell=[tableView dequeueReusableCellWithIdentifier:CellIdentifer];
	if (!cell) {
		cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifer];
	}
	return  cell;
}

- (void)mapView:(GMSMapView *)mapView
didTapInfoWindowOfMarker:(MyMarker1 *)marker{
    Place * object=[curArray objectAtIndex:marker.idx];
    
    PlaceDetailsViewController * pld=[[PlaceDetailsViewController alloc]init];
    pld.place=object;
    
    [self.navigationController pushViewController:pld animated:YES];
}
-(UIView *)mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker{
    UILabel * lab=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 54)];
    [lab setFont:[UIFont BSFontExstraSmallLight]];
    [lab setTextColor:[UIColor BSDarkBlueColor]];
    [lab setNumberOfLines:5];
    [lab setTextAlignment:NSTextAlignmentCenter];
    
    [lab setText:marker.snippet];
    
    [lab sizeToFit];
    
    
    UIView * v=[[UIView alloc]initWithFrame:CGRectMake(0, 0, lab.frame.size.width+40, lab.frame.size.height+10)];
    
    [lab setCenter:CGPointMake(v.frame.size.width/2, v.frame.size.height/2)];
    [v addSubview:lab];
    UIImageView * arrow=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"arrow"]];
    [arrow setFrame:CGRectMake(lab.frame.origin.x+lab.frame.size.width+7, v.frame.size.height/2-arrow.frame.size.height/2, arrow.frame.size.width, arrow.frame.size.height)];
    [v addSubview:arrow];
    [v setBackgroundColor:[UIColor whiteColor]];
    v.layer.cornerRadius=4;
    v.clipsToBounds=YES;
    UIView * mainView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, v.frame.size.width, v.frame.size.height+10)];
    [mainView setBackgroundColor:[UIColor clearColor]];
    
    [mainView addSubview:v];
    
    UIImageView * imgv=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"triangle"]];
    [imgv setFrame:CGRectMake(v.frame.size.width/2+v.frame.origin.x, v.frame.size.height+v.frame.origin.y-1, imgv.frame.size.width, imgv.frame.size.height)];
    
    [mainView addSubview:imgv];
    
    
    
    
    
    return mainView;
}

@end
