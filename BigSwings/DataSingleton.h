//
//  DataSingleton.h
//  testswings
//
//  Created by mm7 on 05.07.14.
//  Copyright (c) 2014 Oleksandr Padalko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <Parse/Parse.h>
#import <RevMobAds/RevMobAds.h>
#import <RevMobAds/RevMobAdsDelegate.h>
#import "RageIAPHelper.h"

#define kNameSettings @"settings"
#define kNameDetails @"details"


typedef void(^bBlock)(BOOL suc);
@class Place;
@interface DataSingleton : NSObject <BuyingDelegate>{
    NSMutableDictionary * adsDictionary;
    SKProduct * removeAdsProd;
}
static float distFrom(CLLocation * point1,CLLocation * point2 );
static float distToCurLocation(CLLocation * point);

+(DataSingleton*)sharedInstance;

@property (retain,nonatomic) CLLocation * curLocation;
-(float)getDistanceToPoint:(PFGeoPoint*)p;
-(float)getDistanceToPlace:(Place*)place;
@property (retain,nonatomic) PFGeoPoint * parceCurLocation;
-(void)updateCurLocation:(void(^)(CLLocation *loc))block;

-(void)loadMapObjects:(void(^)(BOOL suc))block;

@property (retain,nonatomic)NSMutableArray * mapObjectsArray;
@property (retain,nonatomic)NSMutableArray * mapObjectsArrayRating;


-(void)showBaneerOnController:(UIViewController*)contr andName:(NSString* )name;
-(void)buyProduct:(void(^)(BOOL suc))compBlock;
-(void)checkPurchased:(void(^)(BOOL suc))comBlock;
-(void)restore:(void(^)(BOOL suc))comBlock;
@property (readwrite,copy) bBlock buyingBlock;
@end
