//
//  UIColor+BS.h
//  testswings
//
//  Created by Aleksandr Padalko on 6/24/14.
//  Copyright (c) 2014 Oleksandr Padalko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (BS)
+(UIColor*)BSDarkBlueColor;
+(UIColor*)BSBlackColor;
+(UIColor*)BSGrayColor;
@end
