//
//  TextFieldHelper.m
//  TutoringOnDemand
//
//  Created by Aleksandr Padalko on 3/20/14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "VVTextFieldHelper.h"

@implementation VVTextFieldHelper


-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    
    return [self.delegate textField:textField shouldChangeCharactersInRange:range replacementString:string];
}


-(void)textFieldDidBeginEditing:(UITextField *)textField{
    [self.delegate textFieldDidBeginEditing:textField];
}
-(void) textFieldDidEndEditing:(UITextField *)textField{
    [self.delegate textFieldDidEndEditing:textField];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    return [self.delegate textFieldShouldReturn:textField];
}
-(BOOL)textFieldShouldClear:(UITextField *)textField{
    return [self.delegate textFieldShouldClear:textField];
}
@end
