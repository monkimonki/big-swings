//
//  RateView.h
//  testswings
//
//  Created by mm7 on 06.07.14.
//  Copyright (c) 2014 Oleksandr Padalko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RateView : UIView
-(int)getAllRating;
@end
