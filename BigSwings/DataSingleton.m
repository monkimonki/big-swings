//
//  DataSingleton.m
//  testswings
//
//  Created by mm7 on 05.07.14.
//  Copyright (c) 2014 Oleksandr Padalko. All rights reserved.
//

#import "DataSingleton.h"
#include <math.h>
#import "JMMLocationEngine.h"
#import "Place.h"

#define REVMOB_ID @"5106be9d0639b41100000052"

#define PLACEMENT_ID_BANNER @"5107dfde9c1d2f160000005f"
#define PLACEMENT_ID_FULLSCREEN @"5107dfdeb909351500000071"
#define PLACEMENT_ID_LINK @"5107dfddb90935a8030000a7"
#define PLACEMENT_ID_POPUP @"5107dfdc2513d00d0000007a"
#define PLACEMENT_ID_NOTIFICATION @"5107dfdc2513d01400000095"
@interface DataSingleton (){
    BOOL purch;
}
@end
@implementation DataSingleton
static DataSingleton *dataSingleton;
static float distToCurLocation(CLLocation * point){
    
    
    return distFrom([DataSingleton sharedInstance].curLocation, point);
    
}
static float distFrom(CLLocation * point1,CLLocation * point2 ) {
    
    
    float lat1;
    float lng1;
    float lat2;
    float lng2;
    double earthRadius = 3958.75;
    
    
    double dLat = (lat2 - lat1) * M_PI / 180.0;
    double dLng = (lng2 - lng1) * M_PI / 180.0;;
    double a = sin(dLat/2) * sin(dLat/2) +
    cos( (lat1) * M_PI / 180.0) * cos( (lat2) * M_PI / 180.0) *
   sin(dLng/2) * sin(dLng/2);
    double c = 2 * atan2(sqrt(a), sqrt(1-a));
    float dist = earthRadius * c;
    
    float meterConversion = 1609;
    
    return  dist*meterConversion;
}

-(float)getDistanceToPlace:(Place*)place{
    
    float dist=[self getDistanceToPoint:place.location];
    
    place.realDistance=dist;
    
    return dist;
    
}
-(float)getDistanceToPoint:(PFGeoPoint*)p{
    
    return (float)[_parceCurLocation distanceInMilesTo:p];
}
+(DataSingleton *)sharedInstance{
    
    
    if (dataSingleton==NULL) {
        dataSingleton=[[DataSingleton alloc]init];
    }
    
    return dataSingleton;
}

-(id)init{
    
    if (self=[super init]) {
//        [self updateCurLocation:^(CLLocation *loc) {
//
//        }];
        purch=NO;
        adsDictionary=[[NSUserDefaults standardUserDefaults] valueForKeyPath:@"ads"];
        
        if (!adsDictionary) {
                    adsDictionary=[[NSMutableDictionary alloc] init];
        }else{
            
            if ([[adsDictionary valueForKey:kNameDetails] intValue]<0) {
                [adsDictionary setValue:@"3" forKey:kNameDetails];
            }
            
            if ([[adsDictionary valueForKey:kNameSettings] intValue]<0) {
                [adsDictionary setValue:@"3" forKey:kNameSettings];
            }
        }

        
        [[RageIAPHelper sharedInstance] requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
            if (success) {
            }
        }];
        
        [[RageIAPHelper sharedInstance] setDelegate:self];
        [RevMobAds startSessionWithAppID:REVMOB_ID andDelegate:self];
        
        
    }
    return self;
}
-(void)updateCurLocation:(void(^)(CLLocation *loc))block{
    
    
    [JMMLocationEngine getBallParkLocationOnSuccess:^(CLLocation *loc) {
        dispatch_async(dispatch_get_main_queue(), ^{
          
            
            self.curLocation=loc;
            
            
            block(_curLocation);
            
        });
    } onFailure:^(NSInteger failCode) {
        
        
        block(nil);
    }];
}

-(void)loadMapObjects:(void(^)(BOOL suc))block{
    PFQuery * query=[PFQuery queryWithClassName:@"Place"];
    query.limit=1000;
    
    [query findObjectsInBackgroundWithBlock:^(NSArray* obj,NSError  * err){
        
        if (!err) {
            _mapObjectsArray=[[NSMutableArray alloc]initWithArray:obj];
            
            for (Place * place  in _mapObjectsArray) {
                
                place.realDistance=[self getDistanceToPoint:place.location];
                place.rated=nil;
                
                
                
            }
            _mapObjectsArrayRating=[[NSMutableArray alloc]initWithArray:_mapObjectsArray];
            [_mapObjectsArray sortUsingDescriptors:
             [NSArray arrayWithObjects:
              [NSSortDescriptor sortDescriptorWithKey:@"realDistance" ascending:YES], nil]];
            
            [_mapObjectsArrayRating sortUsingDescriptors:
             [NSArray arrayWithObjects:
              [NSSortDescriptor sortDescriptorWithKey:@"points" ascending:NO], nil]];
            block (YES);
        }else{
            block (NO);
        }
        
        }];
}
-(void)setCurLocation:(CLLocation *)curLocation{
    
    _curLocation=curLocation;
    _parceCurLocation=[PFGeoPoint geoPointWithLocation:curLocation];
}


-(void)checkPurchased:(void(^)(BOOL suc))comBlock{
    
    if (!removeAdsProd) {
        
        
        [[RageIAPHelper sharedInstance] requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
            if (success) {
                removeAdsProd=[products firstObject];
                if ([[RageIAPHelper sharedInstance] productPurchased:removeAdsProd.productIdentifier]) {
                    comBlock (YES);
                }else{
                  comBlock (NO);
                }
            }else{
               comBlock (NO);
            }
            }];
        
    }else{
    
        
         
    
     if ([[RageIAPHelper sharedInstance] productPurchased:removeAdsProd.productIdentifier]) {
         comBlock (YES);
     }else{
       comBlock (NO);
     }
    }
    
}
-(void)showBaneerOnController:(UIViewController*)contr andName:(NSString* )name{
    
    
   int a= [[adsDictionary valueForKey:name] intValue];
    a++;
    if (a>=3) {
        
        
        [self checkPurchased:^(BOOL suc) {
            if (suc) {
                
            }else {
            RevMobFullscreen *fs = [[RevMobAds session] fullscreen];
            //   fs.delegate = self;
            [fs showAd];
            
            }
        }];
    
        a=0;
        
        
     
    }
    [adsDictionary setValue:[NSString stringWithFormat:@"%d",a] forKey:name];
    
    
    [[NSUserDefaults standardUserDefaults]setValue:adsDictionary forKey:@"ads"];
}
-(void)buyProduct:(void(^)(BOOL suc))compBlock{
    _buyingBlock=compBlock;
    if (!removeAdsProd) {
        
   
    [[RageIAPHelper sharedInstance] requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
        if (success) {
            removeAdsProd=[products firstObject];
       
        [[RageIAPHelper sharedInstance] buyProduct:removeAdsProd];
        
        }else {
            compBlock(NO);
        }
    }];
     }else
        [[RageIAPHelper sharedInstance] buyProduct:removeAdsProd];
}
-(void)restore:(void(^)(BOOL suc))comBlock{
    
    _buyingBlock=comBlock;
    
    [[RageIAPHelper sharedInstance]restoreCompletedTransactions];
}

-(void)faildedToBuy{
    
    if (_buyingBlock)
    _buyingBlock(NO);
}
-(void)failedToRestore{
       if (_buyingBlock)
    _buyingBlock(NO);
}
-(void)didBuy{
       if (_buyingBlock)
     _buyingBlock(YES);
}
-(void)didRestore{
       if (_buyingBlock)
     _buyingBlock(YES);
    
}
@end
