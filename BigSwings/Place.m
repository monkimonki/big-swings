//
//  Place.m
//  testswings
//
//  Created by Aleksandr Padalko on 6/20/14.
//  Copyright (c) 2014 Oleksandr Padalko. All rights reserved.
//

#import "Place.h"

@implementation Place
+ (NSString *)parseClassName{
    return @"Place";
}

-(NSString *)getRaiting{
    if (!self.points) {
        return @"0.0";
    }
    
    return [NSString stringWithFormat:@"%.1f",((self.points/3.0)/self.rateCounts)];
    
}
@end
