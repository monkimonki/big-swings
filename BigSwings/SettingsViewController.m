//
//  SettingsViewController.m
//  testswings
//
//  Created by mm7 on 06.07.14.
//  Copyright (c) 2014 Oleksandr Padalko. All rights reserved.
//

#import "SettingsViewController.h"
#import "LineView.h"
#import "BSTextField.h"
#import "StartScreenViewController.h"
#import <MessageUI/MFMailComposeViewController.h>
#import "ResetPasswordViewController.h"
#import "ContactUsViewController.h"
#import "AboutUsViewController.h"
#import "LoadVIew.h"
#import "TextViewController.h"
@interface SettingsViewController ()<MFMailComposeViewControllerDelegate>

@end

@implementation SettingsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [self setTitle:[locSettings localized]];
    
    
    
    [[DataSingleton sharedInstance]showBaneerOnController:self andName:kNameDetails];
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
    // [backButton setTitle:@"back" forState:UIControlStateNormal];
    [backButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [backButton setImage:[UIImage imageNamed:@"Back_ico.png"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];

    
    
    [self.listTableView addStaticCellsWithData:[NSArray arrayWithObjects:locRemoveAds,locRestorePurchase,@"About Us",locContactUs,@"Terms of Use",locEmail,locResetYourPass ,nil]];
    
    
    [[DataSingleton sharedInstance] checkPurchased:^(BOOL suc) {
        if (suc) {
            [self.listTableView insertData:[NSArray arrayWithObjects:@"About Us",locContactUs,@"Terms of Use",locEmail,locResetYourPass ,nil] toStaticCellAtSection:0];
            [self.listTableView reloadData];
        }
    }];
    
    // Do any additional setup after loading the view.
}

-(void)back{
    [UIView  beginAnimations:nil context:NULL];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:0.75];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:self.navigationController.view cache:NO];
    [UIView commitAnimations];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelay:0.375];
    [self.navigationController popViewControllerAnimated:YES];
    [UIView commitAnimations];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(CGFloat)listTableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 46;
    
}
-(UITableViewCell *)listTableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath objectForRow:(NSString*)object{
    NSString *   CellIdentifer = [NSString stringWithFormat:@"c2%@",object];
    UITableViewCell  * cell=[tableView dequeueReusableCellWithIdentifier:CellIdentifer];
    if (!cell) {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifer];
        
        LineView * lv=[[LineView alloc]initWithFrame:CGRectMake(0, 46-0.5, self.view.frame.size.width, 0.5)];
        
        [cell addSubview:lv];
        [cell setSelectionStyle:UITableViewCellEditingStyleNone];
        if ([object isEqualToString:locRestorePurchase]||[object isEqualToString:locRemoveAds]||[object isEqualToString:locContactUs]||[object isEqualToString:@"About Us"]||[object isEqualToString:@"Terms of Use"]) {
            
            
            
            UILabel * lab=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 46)];
            [lab setBackgroundColor:[UIColor clearColor]];
            [lab setFont:[UIFont BSFontLargeLight]];
            [lab setTextAlignment:NSTextAlignmentCenter];
            [lab setTextColor:[UIColor BSDarkBlueColor]];
            
            [cell addSubview:lab];
            [lab setText:[object localized]];
            
            
        }else{
            
            if ([object isEqualToString:locEmail]) {
                
                BSTextField * b=[[BSTextField alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width-48, 45)];
                
                
                [b setFont:[UIFont BSFontMediumLightOblique]];
                [b addImage:[UIImage imageNamed:@"Login_ico"]];
                [b setPlaceholder:[locLogin localized]];
                [b setBackgroundColor:[UIColor whiteColor]];
                [b setTextColor:[UIColor BSDarkBlueColor]];

                [cell addSubview:b];
                [b setText:[[PFUser currentUser] email]];
                [b setUserInteractionEnabled:NO];
                
                UIView* v=[[UIView alloc]initWithFrame:CGRectMake(b.frame.size.width, 0, 0.5, 46)];
                [v setBackgroundColor:[UIColor blackColor]];
                
                [cell addSubview:v];
                
                
                UIButton * but=[UIButton buttonWithType:UIButtonTypeCustom];
                
                [but setFrame:CGRectMake(b.frame.size.width, 0, self.view.frame.size.width-b.frame.size.width, 46)];
                [but addTarget:self action:@selector(logOut) forControlEvents:UIControlEventTouchUpInside];
                [but setImage:[UIImage imageNamed:@"logout_ico"] forState:UIControlStateNormal];
                [cell addSubview:but];
            }else{
                 BSTextField * b=[[BSTextField alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 45)];
                [b setFont:[UIFont BSFontMediumLightOblique]];
                [b setTextColor:[UIColor BSDarkBlueColor]];
                
                [b addImage:[UIImage imageNamed:@"Locker_pass"]];
                [b setPlaceholder:[locPassword localized]];
                [b setBackgroundColor:[UIColor whiteColor]];
                
                [cell addSubview:b];
                [b setUserInteractionEnabled:NO];
                [b setText:[object localized]];
            }
            
        }
        
        
        
    }
    return  cell;
    
}
- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error;
{
    if (result == MFMailComposeResultSent) {
        NSLog(@"It's away!");
    }
    [self dismissModalViewControllerAnimated:YES];
}
-(void)listTableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath withObject:(NSString *)object{
    if ([object isEqualToString:locContactUs]) {
//        MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
//        controller.mailComposeDelegate = self;
//        [controller setToRecipients:[NSArray arrayWithObjects:@"support@bigswings.com", nil]];
//        [controller setSubject:@"My Subject"];
//        [controller setMessageBody:@"Hello there." isHTML:NO];
//        if (controller) [self presentModalViewController:controller animated:YES];
        
        [self.navigationController pushViewController:[[ContactUsViewController alloc] init] animated:YES];
        
    }else if ([object isEqualToString:locResetYourPass]){
        
        [self.navigationController pushViewController:[[ResetPasswordViewController alloc] init] animated:YES];
        
    }else if ([object isEqualToString:locRemoveAds]){
     
        LoadVIew * v=[LoadVIew create];
         
        [[DataSingleton sharedInstance]buyProduct:^(BOOL suc) {
            if (suc) {
                [self.listTableView insertData:[NSArray arrayWithObjects:@"About Us",locContactUs,locEmail,locResetYourPass ,nil] toStaticCellAtSection:0];
                [self.listTableView reloadData];
            }
            [v dissmiss];
        }];
        
    }else if ([object isEqualToString:locRestorePurchase]){
        
        LoadVIew * v=[LoadVIew create];
        
        [[DataSingleton sharedInstance]restore:^(BOOL suc) {
        
            if (suc) {
                
                 [[DataSingleton sharedInstance]checkPurchased:^(BOOL suc) {
                        [v dissmiss];
                     if (suc) {
                         [self.listTableView insertData:[NSArray arrayWithObjects:@"About Us",locContactUs,locEmail,locResetYourPass ,nil] toStaticCellAtSection:0];
                         [self.listTableView reloadData];
                     }
                 }];
                
            }else{
                  [v dissmiss];
            }
        
        }];
        
    }else if ([object isEqualToString:@"About Us"]){
        
        [self.navigationController pushViewController:[[AboutUsViewController alloc] init
                                                       ] animated:YES];
        
    }else if ([object isEqualToString:@"Terms of Use"]){
        
        TextViewController * t=[[TextViewController alloc] initWithText:[NSString stringWithFormat:@"%@",@"BSTERMS.docx"]];
        
        
        
        [self  presentViewController:t animated:YES completion:nil];
        
    }
}
-(void)logOut{
    [PFUser logOut ];
    [self.navigationController setViewControllers:[NSArray arrayWithObject:[[StartScreenViewController alloc] init]]];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
