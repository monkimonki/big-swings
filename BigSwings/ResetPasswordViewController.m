//
//  ResetPasswordViewController.m
//  bigswings
//
//  Created by mm7 on 07.07.14.
//  Copyright (c) 2014 Oleksandr Padalko. All rights reserved.
//

#import "ResetPasswordViewController.h"
#import "BSTextField.h"
#import "LineView.h"
@interface ResetPasswordViewController (){
    
    BSTextField * oldPass;
    BSTextField * newPass;
    BSTextField * newPass2;
    NSMutableArray * fieldsArr;
}

@end

@implementation ResetPasswordViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setTitle:@"Reset Password"];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    

    
    oldPass=[[BSTextField alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 45)];
    oldPass.botBorder=YES;
    
    [oldPass setFont:[UIFont BSFontMediumLightOblique]];
    [oldPass addImage:[UIImage imageNamed:@"Locker_pass"]];
    [oldPass setPlaceholder:[locOldPass localized]];
    [oldPass setBackgroundColor:[UIColor whiteColor]];
    [oldPass setTextColor:[UIColor BSDarkBlueColor]];

    newPass=[[BSTextField alloc]initWithFrame:CGRectMake(0,oldPass.frame.size.height, self.view.frame.size.width, 45)];
    newPass.botBorder=YES;
    
    [newPass setFont:[UIFont BSFontMediumLightOblique]];
    [newPass addImage:[UIImage imageNamed:@"Locker_pass"]];
    [newPass setPlaceholder:[locPassword localized]];
    [newPass setBackgroundColor:[UIColor whiteColor]];
    [newPass setTextColor:[UIColor BSDarkBlueColor]];

    newPass2=[[BSTextField alloc]initWithFrame:CGRectMake(0, oldPass.frame.size.height+newPass.frame.size.height, self.view.frame.size.width, 45)];
    newPass2.botBorder=YES;
    
    [newPass2 setFont:[UIFont BSFontMediumLightOblique]];
    [newPass2 addImage:[UIImage imageNamed:@"Locker_pass"]];
    [newPass2 setPlaceholder:[locRepeatPass localized]];
    [newPass2 setBackgroundColor:[UIColor whiteColor]];
    [newPass2 setTextColor:[UIColor BSDarkBlueColor]];

    [newPass2 setVvTextFieldDelegate:self];
    [newPass setVvTextFieldDelegate:self];
    [oldPass setVvTextFieldDelegate:self];
    fieldsArr=[[NSMutableArray alloc]init];
    [fieldsArr addObject:oldPass];
    [fieldsArr addObject:newPass];
    [fieldsArr addObject:newPass2];
      newPass2.secureTextEntry=YES;
    newPass.secureTextEntry=YES;
    oldPass.secureTextEntry=YES;
    [self.view addSubview:oldPass];
    [self.view addSubview:newPass];
    [self.view addSubview:newPass2];
    UIButton * b1=[UIButton buttonWithType:UIButtonTypeCustom];
    
    
    [b1 setFrame:CGRectMake(0,0, 165, 35)];
    [b1 setCenter:CGPointMake(self.view.frame.size.width/2,newPass2.frame.origin.y+newPass2.frame.size.height+20+35/2)];
    [b1 setTitle:[@"Reset" uppercaseString] forState: UIControlStateNormal];
    [b1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [b1 setBackgroundColor:[UIColor BSDarkBlueColor]];
    [b1.titleLabel setFont:[UIFont BSFontLargeLight]];
    b1.layer.cornerRadius=4;
    [b1 setTag:1112];
    [b1 addTarget:self action:@selector(addPhoto) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:b1];
    
    
    
    
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
    // [backButton setTitle:@"back" forState:UIControlStateNormal];
    [backButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [backButton setImage:[UIImage imageNamed:@"Back_ico.png"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    // Do any additional setup after loading the view.
}
-(void)back{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)addPhoto{
    [PFUser logInWithUsernameInBackground:[PFUser currentUser].username password:oldPass.text block:^(PFUser *user, NSError *error) {
      
        if (!error) {
            if (newPass.text.length==0) {
                UIAlertView * av=[[UIAlertView alloc] initWithTitle:@"" message:@"Enter new Password" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                
                [av show];
            }
            
            if (![newPass2.text isEqualToString:newPass2.text]) {
                UIAlertView * av=[[UIAlertView alloc] initWithTitle:@"" message:@"Password does not match" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                
                [av show];
                return;
            }
            
            [PFUser currentUser].password=newPass2.text;
            
            
            [[PFUser currentUser]saveEventually];
            
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            UIAlertView * av=[[UIAlertView alloc]initWithTitle:@"" message:@"Wrong old password" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [av show];
            return;
        }
       
        
    
    }];
  
    
    
    
    
    
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void) editingBegin:(UIView*)v{
    
}
-(void) editingEndWithView:(UIView*)v{
    
}
-(void) editingEnd{
    
}
-(void) editingEndWithText:(NSString*)str{
    
}

-(void)clear{
    
}
-(void)clearWithObj:(id)obj{
    
}


-(void)editingWithText:(NSString*)text{
    
}



-(void)textFieldShouldTabBack:(UITextField*)tf{
    int idx=[fieldsArr indexOfObject:tf];
    
    if (idx>0) {
        [[fieldsArr objectAtIndex:idx-1] becomeFirstResponder];
    }
  
}
-(void)textFieldShouldTabForward:(UITextField*)tf{
    int idx=[fieldsArr indexOfObject:tf];
    
    if (idx<fieldsArr.count-1) {
        [[fieldsArr objectAtIndex:idx+1] becomeFirstResponder];
    }
}
@end
