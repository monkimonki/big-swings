//
//  UIColor+BS.m
//  testswings
//
//  Created by Aleksandr Padalko on 6/24/14.
//  Copyright (c) 2014 Oleksandr Padalko. All rights reserved.
//

#import "UIColor+BS.h"

@implementation UIColor (BS)
+(UIColor*)BSDarkBlueColor{
    return [UIColor colorWithHex:@"#012752" alpha:1.0];
}
+(UIColor*)BSBlackColor{
        return [UIColor colorWithHex:@"#000000" alpha:1.0];

}
+(UIColor*)BSGrayColor{
    return  [UIColor colorWith8BitRed:233 green:233 blue:233 alpha:1.0];
}
@end
