//
//  ContactUsViewController.m
//  bigswings
//
//  Created by mm7 on 17.07.14.
//  Copyright (c) 2014 Oleksandr Padalko. All rights reserved.
//

#import "ContactUsViewController.h"
#import <MessageUI/MFMailComposeViewController.h>

@interface ContactUsViewController ()<MFMailComposeViewControllerDelegate>

@end

@implementation ContactUsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setTitle:@"Contact Us"];
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
    // [backButton setTitle:@"back" forState:UIControlStateNormal];
    [backButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [backButton setImage:[UIImage imageNamed:@"Back_ico.png"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    self.navigationItem.rightBarButtonItem=nil;
    
    UILabel * lab=[[UILabel alloc]initWithFrame:CGRectMake(15, 10, self.view.frame.size.width-30, 120)];
    [lab setBackgroundColor:[UIColor whiteColor]];
    [lab setTextColor:[UIColor BSDarkBlueColor]];
    [lab setFont:[UIFont BSFontMediumLight]];
    [lab setText:@"Please feel free to contact Big Swings through our contact form.  We welcome any questions or concerns.  We will get back to you as fast as possible.  Thank you for swinging by!"];
    [lab setTextAlignment:NSTextAlignmentCenter];
    [lab setNumberOfLines:5];
    UIButton * b1=[UIButton buttonWithType:UIButtonTypeCustom];
    
    
    [b1 setFrame:CGRectMake(0,0, 165, 35)];
    [b1 setCenter:CGPointMake(self.view.frame.size.width/2,lab.frame.origin.y+lab.frame.size.height+20+35/2)];
    [b1 setTitle:[@"Contact" uppercaseString] forState: UIControlStateNormal];
    [b1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [b1 setBackgroundColor:[UIColor BSDarkBlueColor]];
    [b1.titleLabel setFont:[UIFont BSFontLargeLight]];
    b1.layer.cornerRadius=4;
    [b1 setTag:1112];
    
    [b1 addTarget:self action:@selector(contact) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:b1];
    [self.view addSubview:lab];
    // Do any additional setup after loading the view.
}
-(void)contact{
    MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
            controller.mailComposeDelegate = self;
           [controller setToRecipients:[NSArray arrayWithObjects:@"Ropeswingme@gmail.com", nil]];
           [controller setSubject:@"My Subject"];
         [controller setMessageBody:@"Hello there." isHTML:NO];
           if (controller) [self presentModalViewController:controller animated:YES];
}
-(void)back{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    //Add an alert in case of failure
    [self dismissViewControllerAnimated:YES completion:nil];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
