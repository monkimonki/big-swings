//
//  KramTextField.h
//  TutoringOnDemand
//
//  Created by mm7 on 13.03.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "VVBaseTextField.h"

@interface VVTextField : VVBaseTextField{
    UIImageView * okImageView;
    UIImageView * xImageView;
}

@property(nonatomic)BOOL topBorder;
@property(nonatomic)BOOL botBorder;
@property(nonatomic)BOOL leftBorder;
@property (nonatomic)BOOL rightBorder;

@property(nonatomic)NSString * errorString;
@property (nonatomic,retain)UIFont * errorFont;

@property (nonatomic,retain)    UILabel * errorLabel;
@property (nonatomic,retain)UIColor *errorColor;

@property (retain,nonatomic)UIColor * borderColor;
@property (retain,nonatomic)UIColor * selectionBorderColor;
@property (retain,nonatomic)UIColor * selectionBorderErorrColor;
@property  (nonatomic)BOOL isLoad;
@property (retain,nonatomic)  UIImage * okImg;
@property (retain,nonatomic)  UIImage * xImg;

-(void)setOk;
-(void)setX;
-(UILabel*)createErrorLabel:(NSString*)errorString;

-(void)startLoad;

-(void)stopLoad;


-(void)viewSetup;

@end
