//
//  TextViewController.h
//  tutoringondemand
//
//  Created by mm7 on 21.04.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TextViewController : UIViewController
-(id)initWithText:(NSString*)text;
@end
