//
//  LoadVIew.m
//  testswings
//
//  Created by Aleksandr Padalko on 6/20/14.
//  Copyright (c) 2014 Oleksandr Padalko. All rights reserved.
//

#import "LoadVIew.h"
#import "ELKAppDelegate.h"
@implementation LoadVIew

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.94]];
        
        [self setAlpha:0];
        
        UIActivityIndicatorView * av=[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        
        [av setCenter:self.center];
        
        
        [self addSubview:av];
        
        [av startAnimating];
        
        UILabel * label=[[UILabel alloc]initWithFrame:CGRectMake(20, self.frame.size.height/2-80, self.frame.size.width-40,40 )];
        [label setText:@"loading"];
        [label setTextColor:[UIColor whiteColor]];
        [label setTextAlignment:NSTextAlignmentCenter];
        [self addSubview:label];
        
        [UIView animateWithDuration:0.334 animations:^{
            [self setAlpha:1];
        
        
        }];
        
    }
    return self;
}
+(LoadVIew*)create{
    
    ELKAppDelegate * ap=[UIApplication sharedApplication].delegate;
    
    
    LoadVIew * lv=[[LoadVIew alloc]initWithFrame:ap.window.bounds];
    
    
    [ap.window addSubview:lv];
    
    return lv;
    
}
-(void)dissmiss{
    
    [UIView animateWithDuration:0.33 animations:^{
        [self setAlpha:0];
    }completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
