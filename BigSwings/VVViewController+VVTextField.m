//
//  UIViewController+VVTextField.m
//  FGApp
//
//  Created by mm7 on 10.05.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "VVViewController+VVTextField.h"

@implementation UIViewController (VVTextField)
static float offset;
static CGSize  keyBoardSize;
static UIView  * curResponder;
static NSMutableArray * observArr;

-(void)vvTextFieldAdded{
    
  //  id vc= [super init];
    
    if (!observArr) {
        observArr=[[NSMutableArray alloc]init];
    }
    if (![observArr containsObject:self]) {
        
 
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardWillChangeFrame:)
                                                     name:UIKeyboardWillChangeFrameNotification
                                                   object:nil];
        [observArr addObject:self];
   }
    
    
   // return vc;
    
    
}
- (void)keyboardWasHide:(NSNotification *)notification
{
     keyBoardSize=CGSizeZero;
    curResponder=nil;
    if (offset>0) {
        [self moveObject:self.view andPos:CGPointMake(self.view.center.x, self.view.center.y+offset) andTime:0.33];
        
        
    }
    offset=0;
}
-(void)keyboardWillChangeFrame:(NSNotification *)notification{
    
}

-(void)keyboardWasShown:(NSNotification *)notification{
//    keyBoardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
//    
//    [self showAction];
}

-(void)showAction{

    float keyLastY=self.view.frame.size.height-keyBoardSize.height;
    float vLastY=curResponder.frame.origin.y+curResponder.frame.size.height;
    if (vLastY>=keyLastY) {
        offset=(vLastY-keyLastY)+10;
        [self moveObject:self.view andPos:CGPointMake(self.view.center.x, self.view.center.y-offset) andTime:0.33];
    }else{
        offset=0;
       [self moveObject:self.view andPos:CGPointMake(self.view.center.x, self.view.center.y-offset) andTime:0.33];
    }
}
-(void)moveObject:(UIView *)v andPos:(CGPoint)pos andTime:(float)t {
    [UIView animateWithDuration:t animations:^{
        v.center = pos;
    }];
}

-(void)editingBegin:(UIView *)v{
    curResponder=v;
    
}
-(void) editingEndWithView:(UIView*)v{
    
}
-(void) editingEnd{
    curResponder=nil;
}
-(void) editingEndWithText:(NSString*)str{
    
}

-(void)clear{
    
}
-(void)clearWithObj:(id)obj{
    
}


-(void)editingWithText:(NSString*)text{
    
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    if (curResponder) 
    [curResponder resignFirstResponder];
    [super touchesBegan:touches withEvent:event];
}

-(UIView *)curResponder
{
	return curResponder;
}

@end
