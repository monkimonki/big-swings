//
//  RegistrationViewController.m
//  testswings
//
//  Created by Aleksandr Padalko on 6/20/14.
//  Copyright (c) 2014 Oleksandr Padalko. All rights reserved.
//

#import "RegistrationViewController.h"
#import "BSTextField.h"
#import "PlacesViewController.h"
#import "BSButton.h"
#import <Parse/Parse.h>
@interface RegistrationViewController (){
    BSTextField * login;
    BSTextField * pass1;
    BSTextField * pass2;
    NSMutableArray * fieldsArr;
}

@end

@implementation RegistrationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
   
    fieldsArr=[[NSMutableArray alloc]init];
    self.navigationItem.rightBarButtonItem=nil;
    login=[[BSTextField alloc]initWithFrame:CGRectMake(0, 64, self.view.frame.size.width, 72)];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    [login setFont:[UIFont systemFontOfSize:12]];
    [login addImage:Nil];
    [login setPlaceholder:[locLogin localized]];
    [login setBackgroundColor:[UIColor whiteColor]];
    login.botBorder=YES;
    login.group=locLogin;
    [login setVvTextFieldDelegate:self];
    [fieldsArr addObject:login];
    [self.view addSubview:login];
    
    
    
    
    
    
    
    
    

    self.navigationItem.rightBarButtonItem=nil;
    pass1=[[BSTextField alloc]initWithFrame:CGRectMake(0,64+ 72, self.view.frame.size.width, 72)];
    
    [pass1 setFont:[UIFont systemFontOfSize:12]];
    [pass1 addImage:Nil];
    [pass1 setPlaceholder:[locPassword localized]];
    [pass1 setBackgroundColor:[UIColor whiteColor]];
    pass1.botBorder=YES;
    pass1.group=locLogin;
    [login setVvTextFieldDelegate:self];
    [fieldsArr addObject:pass1];
    [self.view addSubview:pass1];

    pass2=[[BSTextField alloc]initWithFrame:CGRectMake(0,64+ 144, self.view.frame.size.width, 72)];
    
    [pass2 setFont:[UIFont systemFontOfSize:12]];
    [pass2 addImage:Nil];
    [pass2 setPlaceholder:[locRepeatPass localized]];
    [pass2 setBackgroundColor:[UIColor whiteColor]];
    pass2.botBorder=YES;
    pass2.group=locLogin;
    [pass2 setVvTextFieldDelegate:self];
    [fieldsArr addObject:pass2];
    [self.view addSubview:pass2];
    
    BSButton * b=[UIButton buttonWithType:UIButtonTypeCustom];
    
    

    [b setFrame:CGRectMake(0,0, 165, 35)];
    [b setCenter:CGPointMake(self.view.frame.size.width/2,pass2.frame.origin.y+pass2.frame.size.height+ (self.view.frame.size.height-pass2.frame.origin.y-pass2.frame.size.height)/2)];
    [b setTitle:@"Submit" forState: UIControlStateNormal];
    [b setBackgroundColor:[UIColor blueColor]];
    [b addTarget:self action:@selector(submit) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:b];
    
}
-(void)submit{
    if (login.text.length==0) {
        UIAlertView * av=[[UIAlertView alloc]initWithTitle:@"" message:@"Enter Login" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [av show];
        
        return;
    }
    
    
    if (pass1.text.length==0) {
        
        UIAlertView * av=[[UIAlertView alloc]initWithTitle:@"" message:@"Enter a password" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [av show];
        
        return;
    }
    
    if (pass2.text.length==0) {
        UIAlertView * av=[[UIAlertView alloc]initWithTitle:@"" message:@"Enter a password" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [av show];
        
        return;
    }
    
    if (![pass1.text isEqualToString:pass2.text]) {
        
        UIAlertView * av=[[UIAlertView alloc]initWithTitle:@"" message:@"Passwords not match" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [av show];
        
        return;
    }
    
    
    PFUser *user = [PFUser user];
    user.username =login.text;
    user.password = pass1.text;
    [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (succeeded) {
             [self.navigationController setViewControllers:[NSArray arrayWithObject:[[PlacesViewController alloc] init]] animated:YES];
        }
    }];
 
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
