//
//  ELKAppDelegate.m
//  BigSwings
//
//  Created by Oleksandr Borovok on 03.06.14.
//  Copyright (c) 2014 Oleksandr Padalko. All rights reserved.
//

#import "ELKAppDelegate.h"
#import "BSNavigationController.h"
#import "PlacesViewController.h"
#import <Parse/Parse.h>
#import "Place.h"
#import "StartScreenViewController.h"
#import "VideoViewController.h"
#import "Ratings.h"
#import "Media.h"
#import "VideoViewController.h"

#import <MediaPlayer/MediaPlayer.h>
@implementation ELKAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    NSMutableArray *fontNames = [[NSMutableArray alloc] init];
    
    // get font family
    NSArray *fontFamilyNames = [UIFont familyNames];
    
    // loop
    for (NSString *familyName in fontFamilyNames)
    {
        NSLog(@"Font Family Name = %@", familyName);
        
        // font names under family
        NSArray *names = [UIFont fontNamesForFamilyName:familyName];
        
        NSLog(@"Font Names = %@", fontNames);
        
        // add to array
        [fontNames addObjectsFromArray:names];
    }
    
    [[VVServer inst] setDataType:kVVServerDataTypeParse];
    [Place registerSubclass];
    [Ratings registerSubclass];
    [Media registerSubclass];
    [Parse setApplicationId:@"xDRPuDUAQn7Yjwlz9nXuMV9Xyh3QrrBmFPr1pGXg"
                  clientKey:@"QKxVsblhYlguU8MnRzraHEHRXBSwU142u0lVy0Yj"];
    [GMSServices provideAPIKey:@"AIzaSyAqK6CCF3Tt6HVHvOPyc3FpNFeC92nE8Z0"];
    BSNavigationController * nav;
    
    
    
  //  nav =[[BSNavigationController alloc]initWithRootViewController:[[VideoViewController alloc]init]];
  	if ([PFUser currentUser]) {
        
        NSLog(@"user existis");
        nav =[[BSNavigationController alloc]initWithRootViewController:[[PlacesViewController alloc] init]];

    } else {
         nav =[[BSNavigationController alloc]initWithRootViewController:[[StartScreenViewController alloc] init]];
      
        

	}
   // nav=[[BSNavigationController alloc]initWithRootViewController:[[VideoViewController alloc] init]];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    [self.window setRootViewController:nav];
    
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
