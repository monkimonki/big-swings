//
//  UIFont+BS.h
//  testswings
//
//  Created by Aleksandr Padalko on 6/24/14.
//  Copyright (c) 2014 Oleksandr Padalko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (BS)

+(UIFont *)BSFontExstraSmallLight;
+(UIFont *)BSFontSmallLight;
+(UIFont *)BSFontSmallLightOblique;
+(UIFont *)BSFontMediumLight;
+(UIFont *)BSFontMediumLightOblique;
+(UIFont *)BSFontLargeLight;
+(UIFont *)BSFontLargeLightOblique;
@end
