//
//  BSTextField.h
//  testswings
//
//  Created by Aleksandr Padalko on 6/19/14.
//  Copyright (c) 2014 Oleksandr Padalko. All rights reserved.
//

#import "VVTextField.h"

@interface BSTextField : VVTextField
-(void)addImage:(UIImage*)img;
@property (retain,nonatomic)NSString * group;
@end
