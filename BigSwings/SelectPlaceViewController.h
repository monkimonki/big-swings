//
//  SelectPlaceViewController.h
//  bigswings
//
//  Created by mm7 on 09.07.14.
//  Copyright (c) 2014 Oleksandr Padalko. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol SlectPlaceDelegate <NSObject>

-(void)placeSelectedWithAddress:(NSString*)adr andLocation:(CLLocation * )loc;

-(void)placeSelectCancel;
@end
@interface SelectPlaceViewController : UIViewController
@property (assign,nonatomic)id<SlectPlaceDelegate> delegate;
@end
