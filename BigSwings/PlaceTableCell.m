//
//  PlaceTableCell.m
//  testswings
//
//  Created by Aleksandr Padalko on 6/20/14.
//  Copyright (c) 2014 Oleksandr Padalko. All rights reserved.
//

#import "PlaceTableCell.h"
#import "LineView.h"
@interface PlaceTableCell(){
    
    UILabel * nameLabel;
    UILabel * addrLabel;
    
    
    UILabel * labelDistance;
    UILabel * labelRaiting;
    
}
@end
@implementation PlaceTableCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        nameLabel =[[UILabel alloc]initWithFrame:CGRectMake(11, 4, 220, 15)];
        [nameLabel setBackgroundColor:[UIColor clearColor]];
        [nameLabel setTextAlignment:NSTextAlignmentLeft];
        [nameLabel setFont:[UIFont BSFontMediumLight]];
        [nameLabel setTextColor:[UIColor BSDarkBlueColor]];
        [self addSubview:nameLabel];
        
        
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        addrLabel=[[UILabel alloc]initWithFrame:CGRectMake(11, 4+15, 210, 30)];
        [addrLabel setBackgroundColor:[UIColor clearColor]];
        [addrLabel setTextAlignment:NSTextAlignmentLeft];
        [addrLabel setNumberOfLines:2];
        [addrLabel setFont:[UIFont BSFontMediumLight]];
             [addrLabel setTextColor:[UIColor BSDarkBlueColor]];
        [self addSubview:addrLabel];
        
        
        UIImageView * starImageView=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"star"]];
        
        [starImageView setCenter:CGPointMake(234, 22)];
        [self addSubview:starImageView];
        labelRaiting=[[UILabel alloc]initWithFrame:CGRectMake(starImageView.frame.origin.x-10, starImageView.frame.size.height+starImageView.frame.origin.y+5, starImageView.frame.size.width+20,10)];
        [labelRaiting setTextAlignment:NSTextAlignmentCenter];
        [self addSubview:labelRaiting];
        
        
        [labelRaiting setFont:[UIFont BSFontExstraSmallLight]];
        [labelRaiting setTextColor:[UIColor BSDarkBlueColor]];
        
        
        
        
        
        
        
        
        UIImageView * routeImageView=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"Distance"]];
        
        [routeImageView setCenter:CGPointMake(starImageView.frame.size.width/2+starImageView.center.x+35+routeImageView.frame.size.width/2, starImageView.center.y)];
        [self addSubview:routeImageView];
        
        labelDistance=[[UILabel alloc]initWithFrame:CGRectMake(routeImageView.frame.origin.x-14, routeImageView.frame.size.height+starImageView.frame.origin.y+5, routeImageView.frame.size.width+28,10)];
        [labelDistance setTextAlignment:NSTextAlignmentCenter];
        [self addSubview:labelDistance];
        
        
        [labelDistance setFont:[UIFont BSFontExstraSmallLight]];
        [labelDistance setTextColor:[UIColor BSDarkBlueColor]];
        
//        LineView * lv =[[LineView alloc]initWithFrame:CGRectMake(0, 51-0.5, [UIScreen mainScreen].bounds.size.width, 0.5)];
//        [self addSubview:lv];
    }
    return self;
}
-(void)setPlace:(Place*)place andIndex:(int)index{
 
    if (index%2==0) {
        [self setBackgroundColor:[UIColor BSGrayColor]];
    }else{
        [self setBackgroundColor:[UIColor whiteColor]];
    }
    
    
    [labelRaiting  setText:[place getRaiting]];
    [nameLabel setText:place.title];
    [addrLabel setText:place.textAddres];
    
    
    [labelDistance setText:[NSString stringWithFormat:@"%.1f mi.",[[DataSingleton sharedInstance] getDistanceToPlace:place]]];
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
