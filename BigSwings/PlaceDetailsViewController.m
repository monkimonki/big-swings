//
//  PlaceDetailsViewController.m
//  testswings
//
//  Created by mm7 on 06.07.14.
//  Copyright (c) 2014 Oleksandr Padalko. All rights reserved.
//
#import "LineView.h"
#import "PlaceDetailsViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "Place.h"
#import "BSButton.h"
#import "RateView.h"
#import "Ratings.h"
#import "LoadVIew.h"
#import "GalleryViewController.h"
#import <MessageUI/MFMailComposeViewController.h>

  #import <MobileCoreServices/UTCoreTypes.h>
#import "Media.h"
@interface PlaceDetailsViewController ()<UIActionSheetDelegate ,UIImagePickerControllerDelegate,UINavigationControllerDelegate,MFMailComposeViewControllerDelegate>
{
    UIView * topBar;
    UIView * botBar;
    
    UILabel * descriptioLabel;
    UILabel * warrningLabel;
    GMSMapView *mapView;
    GMSCameraPosition *camera;
    
    
    float mapViewSize;
    float rateButtonPanelSize;
    
    
    RateView * rateView;
    BOOL firstTime;
    BSButton * rateButton;
    
    
    UIImageView * curRateImageView;
    UILabel *  curRateCountLabel;
    LoadVIew * loadView;
}
@end

@implementation PlaceDetailsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

    }
    return self;
}
-(void)createTopbar{
    UIView * v=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];

    LineView * lv=[[LineView alloc]initWithFrame:CGRectMake(0, 44-1, self.view.frame.size.width, 1)];
        lv.color=[UIColor BSGrayColor];
    [v addSubview:lv
     ];
    
    UIImageView * imgv=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, v.frame.size.height, v.frame.size.height)];
    
    [imgv setContentMode:UIViewContentModeCenter];
    
    [imgv setImage:[UIImage imageNamed:@"loc.location.png"]];
    
    
    [v addSubview:imgv];
    
    UILabel * labelDescr=[[UILabel alloc]initWithFrame:CGRectMake(imgv.frame.size.width, 0, v.frame.size.width-imgv.frame.size.width*2-v.frame.size.height, v.frame.size.height)];
    
    
    [v addSubview:labelDescr];
    
    [labelDescr setFont:[UIFont BSFontMediumLight]];
    [labelDescr setTextColor:[UIColor BSDarkBlueColor]];
    
    [labelDescr setNumberOfLines:2];
    
    if (_place.textAddres.length>0) {
        
        [labelDescr setText:[NSString stringWithFormat:@"%@\n%@",_place.title,_place.textAddres]];
    }else{
        
        [labelDescr setText:[NSString stringWithFormat:@"%@",_place.title]];
    }
 
    
    [v addSubview:labelDescr];
    
    
     curRateImageView=[[UIImageView alloc]initWithFrame:CGRectMake(labelDescr.frame.size.width+labelDescr.frame.origin.x+v.frame.size.height, 0, v.frame.size.height, v.frame.size.height)];
    [curRateImageView setImage:[UIImage imageNamed:@"Star_yellow"]];
    [curRateImageView setContentMode:UIViewContentModeCenter];
    curRateImageView.transform=CGAffineTransformMakeScale(0.8, 0.8);
    [v addSubview:curRateImageView];
    curRateCountLabel=[[UILabel alloc]initWithFrame:CGRectMake(curRateImageView.frame.origin.x+4, v.frame.size.height-1-10, curRateImageView.frame.size.width-8,10)];
    [curRateCountLabel setTextAlignment:NSTextAlignmentCenter];
    [v addSubview:curRateCountLabel];
 
    
    [curRateCountLabel setFont:[UIFont BSFontExstraSmallLight]];
    [curRateCountLabel setTextColor:[UIColor BSDarkBlueColor]];
     [curRateCountLabel setText:[_place getRaiting]];
    topBar=v;
    
    UIButton * but=[UIButton buttonWithType:UIButtonTypeCustom];
    
    [but setFrame:CGRectMake(curRateImageView.frame.origin.x-topBar.frame.size.height-10, 0, topBar.frame.size.height, topBar.frame.size.height)];
    
    [but.titleLabel setFont:[UIFont BSFontMediumLight]];
    [but setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    
   // [but setTitle:@"Flag\nPost" forState:UIControlStateNormal];
   // [but setBackgroundColor:[UIColor orangeColor]];
    
    UILabel * lab1=[[UILabel alloc]initWithFrame:but.bounds];
    [lab1 setFont:[UIFont BSFontMediumLight]];
    [lab1 setTextColor:[UIColor redColor]];
    [lab1 setTextAlignment:NSTextAlignmentCenter];
    [lab1 setNumberOfLines:2];
    [but addSubview:lab1];
    [lab1 setText:@"Flag\nPost"];
    [but addTarget:self action:@selector(flagPost) forControlEvents:UIControlEventTouchUpInside];
    [but.titleLabel setNumberOfLines:2];
    [topBar addSubview:but];
    
    
    [self.view addSubview:topBar];
    
}
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    //Add an alert in case of failure
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)flagPost{
    MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
    controller.mailComposeDelegate = self;
    [controller setToRecipients:[NSArray arrayWithObjects:@"Ropeswingme@gmail.com", nil]];
    [controller setSubject:[NSString stringWithFormat:@"%@.%@",_place.objectId,_place.title]];
    [controller setMessageBody:[NSString stringWithFormat:@"%@.%@",_place.objectId,_place.title] isHTML:NO];
    if (controller) [self presentModalViewController:controller animated:YES];
}
-(void)createMap{
    camera = [GMSCameraPosition cameraWithLatitude:_place.location.latitude
                                         longitude:_place.location.longitude
                                              zoom:14];
    mapView = [GMSMapView mapWithFrame:CGRectZero camera:camera];
    [mapView setMapType:kGMSTypeHybrid];
    [mapView setFrame:CGRectMake(0, topBar.frame.size.height, self.view.frame.size.width,mapViewSize)];
    [self.view  addSubview:mapView];
    
    
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake(_place.location.latitude, _place.location.longitude);
    marker.snippet = _place.title;
    marker.appearAnimation = kGMSMarkerAnimationPop;
    marker.map = mapView;
    marker.icon = [UIImage imageNamed:@"point"];
    [descriptioLabel setFrame:CGRectMake(descriptioLabel.frame.origin.x, mapView.frame.size.height+mapView.frame.origin.y, descriptioLabel.frame.size.width, descriptioLabel.frame.size.height)];
    [warrningLabel setFrame:CGRectMake(warrningLabel.frame.origin.x, descriptioLabel.frame.size.height+descriptioLabel.frame.origin.y, warrningLabel.frame.size.width, warrningLabel.frame.size.height)];
    
    UIImageView *imgv=[[UIImageView alloc]initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png",locHeight]]];
    
    UILabel * lab=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 14)];
    [lab setFont:[UIFont BSFontMediumLight]];
    [lab setTextAlignment:NSTextAlignmentCenter];
    [lab setTextColor:[UIColor BSDarkBlueColor]];
    [lab setText:[NSString stringWithFormat:@"%.1f mi.",[_place.height floatValue]]];
    [lab sizeToFit];
    
    UIView * v=[[UIView alloc]initWithFrame:CGRectMake(0, mapView.frame.origin.y, imgv.frame.size.width+5+lab.frame.size.width+10+15, imgv.frame.size.height+10)];
    [imgv setFrame:CGRectMake(15, v.frame.size.height/2-imgv.frame.size.height/2, imgv.frame.size.width, imgv.frame.size.height)];
    [lab setFrame:CGRectMake(imgv.frame.size.width+5+imgv.frame.origin.x, v.frame.size.height/2-lab.frame.size.height/2, lab.frame.size.width, lab.frame.size.height)];
    
    [v setBackgroundColor:[UIColor whiteColor]];
    [v addSubview:lab];
    [v addSubview:imgv];
    [self.view addSubview:v];

}
-(void)createDisctiption{
    
    UIImageView * imgv=[[UIImageView alloc]initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png",locDescr]]];
    [imgv setFrame:CGRectMake(-imgv.frame.size.width-5, 38/2-imgv.frame.size.height/2, imgv.frame.size.width, imgv.frame.size.height)];
    [imgv setBackgroundColor:[UIColor clearColor]];
    descriptioLabel=[[UILabel alloc]initWithFrame:CGRectMake(imgv.frame.size.width+15+7, 44, self.view.frame.size.width-30-5-imgv.frame.size.width, 38)];
    [descriptioLabel setFont:[UIFont BSFontMediumLight]];
    [descriptioLabel setNumberOfLines:2];
    [descriptioLabel setBackgroundColor:[UIColor whiteColor]];
    [descriptioLabel setTextColor:[UIColor BSDarkBlueColor]];
    if (_place.descriptionText.length>0) {
            [descriptioLabel setText:_place.descriptionText];
    }else{
    [descriptioLabel setText:@"no description"];
    }
    [descriptioLabel setNumberOfLines:2];
    [descriptioLabel setClipsToBounds:NO];
         [descriptioLabel addSubview:imgv];
    LineView * lv=[[LineView alloc]initWithFrame:CGRectMake(-imgv.frame.size.width-20, descriptioLabel.frame.size.height-1, self.view.frame.size.width+30, 1)];
    lv.color=[UIColor BSGrayColor];
    [descriptioLabel addSubview:lv
     ];
  
    LineView * lv2=[[LineView alloc]initWithFrame:CGRectMake(-imgv.frame.size.width-20, 0, self.view.frame.size.width+30, 1)];
    lv2.color=[UIColor BSGrayColor];
    [descriptioLabel addSubview:lv2
     ];
 
    

    [self.view addSubview:descriptioLabel];
    
}
-(void)createWarrning{
    UIImageView * imgv=[[UIImageView alloc]initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png",locWarrinig]]];
    [imgv setFrame:CGRectMake(-imgv.frame.size.width-5, 38/2-imgv.frame.size.height/2, imgv.frame.size.width, imgv.frame.size.height)];
    [imgv setBackgroundColor:[UIColor clearColor]];
    warrningLabel=[[UILabel alloc]initWithFrame:CGRectMake(imgv.frame.size.width+15+5, 44, self.view.frame.size.width-30-5-imgv.frame.size.width, 38)];
    [warrningLabel setFont:[UIFont BSFontMediumLight]];
    [warrningLabel setNumberOfLines:2];
    [warrningLabel setBackgroundColor:[UIColor whiteColor]];
    [warrningLabel setTextColor:[UIColor BSDarkBlueColor]];

    if (_place.warrnings.length>0) {
        [warrningLabel setText:_place.warrnings];
    }else{
        [warrningLabel setText:@"no warrnings"];
    }
    [warrningLabel setNumberOfLines:2];
    [warrningLabel setClipsToBounds:NO];
    [warrningLabel addSubview:imgv];
    LineView * lv=[[LineView alloc]initWithFrame:CGRectMake(-imgv.frame.size.width-20, descriptioLabel.frame.size.height-1, self.view.frame.size.width+30, 1)];
    lv.color=[UIColor BSGrayColor];
    [warrningLabel addSubview:lv
     ];
    
    
    
    
    [self.view addSubview:warrningLabel];
}
-(void)createRateButtonPanel{
    UIView * v=[[UIView alloc]initWithFrame:CGRectMake(0, warrningLabel.frame.size.height+warrningLabel.frame.origin.y, self.view.frame.size.width, rateButtonPanelSize)];
    
    rateButton =[BSButton create];
    [rateButton setFrame:CGRectMake(0,0, 165, 35)];
    [rateButton setCenter:CGPointMake(v.frame.size.width/2,v.frame.size.height/2)];
    [rateButton setTitle:[[locSubmit localized] uppercaseString] forState: UIControlStateNormal];
    [rateButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [rateButton setBackgroundColor:[UIColor BSDarkBlueColor]];
    [rateButton.titleLabel setFont:[UIFont BSFontLargeLight]];
    rateButton.layer.cornerRadius=4;
    [rateButton addTarget:self action:@selector(rate) forControlEvents:UIControlEventTouchUpInside];
    [v addSubview:rateButton];
    [v setBackgroundColor:[UIColor whiteColor]];
    [rateButton load];
    [v setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:v];
}

-(void)createBotBar{
    botBar=[[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height-44, self.view.frame.size.width, 44)];
    
    LineView * lv=[[LineView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 1)];
    lv.color=[UIColor BSGrayColor];
    [botBar addSubview:lv
     ];
    
    
    UIView * mid=[[UIView alloc]initWithFrame:CGRectMake(botBar.frame.size.width/2, 0, 0.5, botBar.frame.size.height)];
    
    [mid setBackgroundColor:[UIColor BSGrayColor]];
    
    [botBar addSubview:mid];
    
    
    UIButton * butShowGall=[self botButtonWithTitle:[locShowGallery localized]];
    
    UIButton * butSendPhtot=[self botButtonWithTitle:[locSendPhoto localized]];
    
    [butSendPhtot setFrame:CGRectMake(butShowGall.frame.size.width, 0, butSendPhtot.frame.size.width, butSendPhtot.frame.size.height)];
    
    [butSendPhtot addTarget:self action:@selector(sendPhoto) forControlEvents:UIControlEventTouchUpInside];
    
    [butShowGall addTarget:self action:@selector(showGallery) forControlEvents:UIControlEventTouchUpInside];
    [botBar addSubview:butSendPhtot
     ];
    [botBar addSubview:butShowGall];
    
    [self.view addSubview:botBar];
}
-(UIButton*)botButtonWithTitle:(NSString*)str{
    
    UIButton * but=[UIButton buttonWithType:UIButtonTypeCustom];
    [but setBackgroundColor:[UIColor clearColor]];
    [but setFrame:CGRectMake(0, 0, botBar.frame.size.width/2, botBar.frame.size.height)];
    [but.titleLabel setFont:[UIFont BSFontMediumLight]];
    [but setTitleColor:[UIColor BSDarkBlueColor] forState:UIControlStateNormal];
    [but setTitle:str forState:UIControlStateNormal];
    return but;
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setTitle:@"Place"];
    
        [[DataSingleton sharedInstance]showBaneerOnController:self andName:kNameDetails];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
    // [backButton setTitle:@"back" forState:UIControlStateNormal];
    [backButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [backButton setImage:[UIImage imageNamed:@"Back_ico.png"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];

    firstTime=YES;
 
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    
    // Do any additional setup after loading the view.
}

-(void)back{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    if (!firstTime) {
        return;
    }
    firstTime=NO;
    [self createBotBar];
    [self createTopbar];
    [self createDisctiption];
    [self createWarrning];
    NSLog(@"%f",warrningLabel.frame.size.height);
    float elseSize=self.view.frame.size.height-topBar.frame.size.height-descriptioLabel.frame.size.height-warrningLabel.frame.size.height-botBar.frame.size.height;
    
    
    //574
    //184
    float kof=(574.0/2.0)/(184.0/2.0);
    rateButtonPanelSize=elseSize/kof;
    mapViewSize=elseSize-rateButtonPanelSize;
    
    [self createMap];
    
    [self createRateButtonPanel];
    
    if (!_place.rated) {
        
        PFQuery * q=[PFQuery queryWithClassName:@"Ratings"];
        [q whereKey:@"placeId" equalTo:_place];
        [q whereKey:@"userId" equalTo:[PFUser currentUser]];
        
        
        
        [q findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
           
            if (!error) {
                
          
            if (objects.count==0) {
                _place.rated=nil;
                    [curRateImageView setImage:[UIImage imageNamed:@"Star_yellow"]];
            }else{
                _place.rated=@"true";
                    [curRateImageView setImage:[UIImage imageNamed:@"Star_yellow"]];
            }
                [self updateView];
            }
        }];
        
        
    }
    
    [self.view sendSubviewToBack:mapView];
    
    
}
-(void)updateView{
    [rateButton stopLoad];
    
    if (_place.rated) {
        [rateButton setBackgroundColor:[UIColor BSGrayColor]];
        [rateButton setTitle:[[locRated localized] uppercaseString] forState:UIControlStateNormal];
        [rateButton setUserInteractionEnabled:NO];
    }else{
        [rateButton setBackgroundColor:[UIColor BSDarkBlueColor]];
        [rateButton setTitle:[[locRate localized] uppercaseString] forState:UIControlStateNormal];
        [rateButton setTag:1];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma  mark actions 
-(void)rate{
 
    if (rateButton.tag==1) {
        [self showRatePanel];
        rateButton.tag=2;
        [rateButton setTitle:[[locOk localized] uppercaseString] forState:UIControlStateNormal];
    }else{
        [rateButton setBackgroundColor:[UIColor BSGrayColor]];
        [rateButton setTitle:[[locRated localized] uppercaseString] forState:UIControlStateNormal];
        [rateButton setUserInteractionEnabled:NO];
        
        
       int r= [rateView getAllRating];
        
        _place.rateCounts++;
        _place.points+=r;
        _place.rated=nil;
        [curRateImageView setImage:[UIImage imageNamed:@"star"]];
        [curRateCountLabel setText:[_place getRaiting]];
        [_place saveEventually:^(BOOL suc,NSError * err){
        
        
        
        }];
        Ratings * rat=[[Ratings alloc]init];
        rat.userId=[PFUser currentUser];
        rat.placeId=_place;
        
        rat.points=r;
        [rat saveEventually:^(BOOL succeeded, NSError *error) {
            
        }];
        
        
        [UIView animateWithDuration:0.33 animations:^{
            [rateView setCenter:CGPointMake(rateView.center.x, rateView.center.y+rateView.frame.size.height)
             ];
        }completion:^(BOOL finished) {
            [rateView removeFromSuperview];
        }];
        
        
    }
}

-(void)sendPhoto{
   
    BOOL cameraDeviceAvailable = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
    BOOL photoLibraryAvailable = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary];
    
    if (cameraDeviceAvailable && photoLibraryAvailable) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:[locImageVia localized] delegate:self cancelButtonTitle:[locCancel localized] destructiveButtonTitle:nil otherButtonTitles:[locCamera localized], [locGallery localized], nil];
        [actionSheet showInView:self.view];
    } else {
        // if we don't have at least two options, we automatically show whichever is available (camera or roll)
        [self shouldPresentPhotoCaptureController];
    }
}
-(void)showGallery{
    GalleryViewController * g=[[GalleryViewController alloc]init];
    g.place=_place;
    [self.navigationController pushViewController:g animated:YES];
}
-(void)showRatePanel{
    UIView * v=[[UIView alloc]initWithFrame:CGRectMake(0, descriptioLabel.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height-descriptioLabel.frame.origin.y)];
    
    [v setBackgroundColor:[UIColor whiteColor]];
    [self.view insertSubview:v aboveSubview:mapView];
    rateView=[[RateView alloc]initWithFrame:CGRectMake(0, mapView.frame.origin.y+mapView.frame.size.height, self.view.frame.size.width,95)];

    
    [rateView setBackgroundColor:[UIColor BSGrayColor]];
    
    [self.view insertSubview:rateView  belowSubview:v];
    
    
[UIView animateWithDuration:0.33 animations:^{
   
    rateView.center=CGPointMake(rateView.center.x, rateView.center.y-rateView.frame.size.height);
    
}];
    
    
    
}



#pragma mark - PAPTabBarController

- (BOOL)shouldPresentPhotoCaptureController {
    BOOL presentedPhotoCaptureController = [self shouldStartCameraController];
    
    if (!presentedPhotoCaptureController) {
        presentedPhotoCaptureController = [self shouldStartPhotoLibraryPickerController];
    }
    
    return presentedPhotoCaptureController;
}
#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {

        
        
        if (buttonIndex == 0) {
            [self shouldStartCameraController];
        } else if (buttonIndex == 1) {
            [self shouldStartPhotoLibraryPickerController];
        }
    
}
- (BOOL)shouldStartCameraController {
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] == NO) {
        return NO;
    }
    
    UIImagePickerController *cameraUI = [[UIImagePickerController alloc] init];
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]
        && [[UIImagePickerController availableMediaTypesForSourceType:
             UIImagePickerControllerSourceTypeCamera] containsObject:(NSString *)kUTTypeImage]) {
        
        cameraUI.mediaTypes = [NSArray arrayWithObject:(NSString *) kUTTypeImage];
        cameraUI.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        if ([UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceRear]) {
            cameraUI.cameraDevice = UIImagePickerControllerCameraDeviceRear;
        } else if ([UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceFront]) {
            cameraUI.cameraDevice = UIImagePickerControllerCameraDeviceFront;
        }
        
    } else {
        return NO;
    }
    
    cameraUI.allowsEditing = YES;
    cameraUI.showsCameraControls = YES;
    cameraUI.delegate = self;
    
    [self presentViewController:cameraUI animated:YES completion:nil];
    
    return YES;
}


- (BOOL)shouldStartPhotoLibraryPickerController {
    if (([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary] == NO
         && [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum] == NO)) {
        return NO;
    }
    
    UIImagePickerController *cameraUI = [[UIImagePickerController alloc] init];
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]
        && [[UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypePhotoLibrary] containsObject:(NSString *)kUTTypeImage]) {
        
        cameraUI.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        cameraUI.mediaTypes = [NSArray arrayWithObject:(NSString *) kUTTypeImage];
        
    } else if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum]
               && [[UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeSavedPhotosAlbum] containsObject:(NSString *)kUTTypeImage]) {
        
        cameraUI.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        cameraUI.mediaTypes = [NSArray arrayWithObject:(NSString *) kUTTypeImage];
        
    } else {
        return NO;
    }
    
    cameraUI.allowsEditing = YES;
    
    cameraUI.delegate = self;
    
    [self presentViewController:cameraUI animated:YES completion:nil];
    
    return YES;
}
#pragma mark - UIImagePickerDelegate

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [self dismissViewControllerAnimated:NO completion:nil];
    
    UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
    
    
    loadView=[LoadVIew create];
    
    
    
    Media *m=[[Media alloc]init];
    m.userId=[PFUser currentUser];
    m.placeId=_place;
    m.image=[PFFile fileWithData:UIImageJPEGRepresentation(image, 0.5)];
    
    
    [m saveInBackgroundWithBlock:^(BOOL suc,NSError *err){
    
        [loadView dissmiss];
        
    
    
    }];
    
    
    
}



@end
