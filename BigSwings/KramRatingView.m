//
//  KramRatingView.m
//  tutoringondemand
//
//  Created by mm7 on 07.04.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "KramRatingView.h"
@interface KramRatingView(){
    

    CGSize oneStarSize;
    UIImage * starImage;
    UIImage * starImageGold;
}
@end
@implementation KramRatingView
-(id)initWithCoder:(NSCoder *)aDecoder{
    if (self=[super initWithCoder:aDecoder]) {
        [self setup];
        
        [self setBackgroundColor:[UIColor clearColor]];
    }
    return self;
    
    
}
-(void)setup{
   starImage=[UIImage imageNamed:@"Star_gray"];
 starImageGold=[UIImage imageNamed:@"Star_yellow"];
    _raiting=0;
   oneStarSize=CGSizeMake(self.frame.size.width/5, self.frame.size.height);
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setup];
        
                [self setBackgroundColor:[UIColor clearColor]];
        
        
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{

    

    float y=self.frame.size.height/2-starImage.size.height/2;
    for(int a=0 ; a<5  ; a++){
        
        float x=oneStarSize.width*a+oneStarSize.width/2-starImage.size.width/2;
       // [starPostions addObject:[NSValue valueWithCGPoint:CGPointMake(x, y)]];
        if (a<_raiting) {
                    [starImageGold drawInRect:CGRectMake(x, y, starImage.size.width, starImage.size.height)];
        }else
        [starImage drawInRect:CGRectMake(x, y, starImage.size.width, starImage.size.height)];
 
        
      
        
        
        
        
    }
    
}

-(void)setRaiting:(int)raiting{
    
    _raiting=raiting;
    
    
    [self setNeedsDisplay];
    
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchLocation = [touch locationInView:self];
    
    
    for(int a=0;a<5;a++){
        
        float x=oneStarSize.width*(a+1);
        if (touchLocation.x<=x) {
                       [self setRaiting:a+1];
            break;
        }
 
        
    }
    
}

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchLocation = [touch locationInView:self];
    
    
    for(int a=0;a<5;a++){
        
         float x=oneStarSize.width*(a+1);
        if (touchLocation.x<=x) {
            if (_raiting!=a+1) {
                [self setRaiting:a+1];
            }
        
            break;
        }
        
        
    }
}



@end
