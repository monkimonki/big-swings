//
//  Helper.h
//  testswings
//
//  Created by Aleksandr Padalko on 6/19/14.
//  Copyright (c) 2014 Oleksandr Padalko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIColor+BS.h"
#import "UIFont+BS.h"
@interface UIColor (CreateMethods)
+ (UIColor*)colorWith8BitRed:(NSInteger)red green:(NSInteger)green blue:(NSInteger)blue alpha:(CGFloat)alpha;
+ (UIColor*)colorWithHex:(NSString*)hex alpha:(CGFloat)alpha;
@end

@interface NSString (Loc)
-(NSString*)localized;
@end
@interface Helper : NSObject

@end
