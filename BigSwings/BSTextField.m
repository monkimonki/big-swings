//
//  BSTextField.m
//  testswings
//
//  Created by Aleksandr Padalko on 6/19/14.
//  Copyright (c) 2014 Oleksandr Padalko. All rights reserved.
//

#import "BSTextField.h"
@interface BSTextField (){
    
    UIImageView * leftImageView;
}
@end
@implementation BSTextField

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.leftOffset=22;
    }
    return self;
}


-(void)addImage:(UIImage *)img{
    
    if (!leftImageView) {
        leftImageView=[[UIImageView alloc]init];
          [leftImageView setFrame:CGRectMake(self.leftOffset, 0, self.frame.size.height*0.5, self.frame.size.height)];
        if (!img) {
          
            
            [leftImageView setBackgroundColor:[UIColor purpleColor]];
            
       
            
            
        }else{
            [leftImageView setContentMode:UIViewContentModeCenter];
            [leftImageView setImage:img];
            
        }
        
        [self addSubview:leftImageView];
    }
    
    
    
}

-(CGRect)textRectForBounds:(CGRect)bounds{
    if (leftImageView) {
        
        return [super textRectForBounds:CGRectMake(bounds.origin.x+leftImageView.frame.size.width+15, bounds.origin.y, bounds.size.width-leftImageView.frame.size.width-15, bounds.size.height)];
    }else{
        return  [super textRectForBounds:bounds];
    }
   
    
}
-(CGRect)editingRectForBounds:(CGRect)bounds{
    
    
    
    if (leftImageView) {
        
              return [super editingRectForBounds:CGRectMake(bounds.origin.x+leftImageView.frame.size.width+15, bounds.origin.y, bounds.size.width-leftImageView.frame.size.width-15, bounds.size.height)];
    }else{
        return  [super editingRectForBounds:bounds];
    }
  
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
