//
//  AboutUsViewController.m
//  bigswings
//
//  Created by mm7 on 17.07.14.
//  Copyright (c) 2014 Oleksandr Padalko. All rights reserved.
//

#import "AboutUsViewController.h"

@interface AboutUsViewController ()

@end

@implementation AboutUsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self setTitle:@"About Us"];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
    // [backButton setTitle:@"back" forState:UIControlStateNormal];
    [backButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [backButton setImage:[UIImage imageNamed:@"Back_ico.png"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    self.navigationItem.rightBarButtonItem=nil;
      [self.view setBackgroundColor:[UIColor whiteColor]];
    UILabel * lab=[[UILabel alloc]initWithFrame:CGRectMake(15, 15, self.view.frame.size.width-30, self.view.frame.size.height-160)];
    [lab setNumberOfLines:50];
    [lab setTextAlignment:NSTextAlignmentCenter];
    [lab setFont:[UIFont BSFontMediumLight]];
    [lab setTextColor:[UIColor BSDarkBlueColor]];
    [lab setText:@"Big Swings was started to make the task of finding a swimming hole easy.  CEO, Marc Kelly spent his childhood in the woods, enjoying the beauty provided to him from nature.  If there is one thing he knows: it's that finding the BEST spots is no easy task.\nThus, the creation of Big Swings.\nBig Swings wants to share the beauties of the world. No need to get on an Airplane either, Big Swings shows you the closest spots to your exact location.\nWe made this App so the user can share their favorite swimming holes and jumping spots with the rest of the world.   Making it easy and FREE.The way it's meant to be."];
    
    
    [self.view addSubview:lab];
    
    // Do any additional setup after loading the view.
}
-(void)back{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
