//
//  LineView.h
//  Chooos
//
//  Created by Aleksandr Padalko on 5/26/14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LineView : UIView
@property (retain,nonatomic)UIColor * color;
@end
