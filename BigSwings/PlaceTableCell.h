//
//  PlaceTableCell.h
//  testswings
//
//  Created by Aleksandr Padalko on 6/20/14.
//  Copyright (c) 2014 Oleksandr Padalko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Place.h"
@interface PlaceTableCell : UITableViewCell
-(void)setPlace:(Place*)place andIndex:(int)index;
@end
