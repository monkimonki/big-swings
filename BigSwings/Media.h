//
//  Media.h
//  testswings
//
//  Created by mm7 on 06.07.14.
//  Copyright (c) 2014 Oleksandr Padalko. All rights reserved.
//

#import <Parse/Parse.h>

@interface Media : PFObject<PFSubclassing>
+ (NSString *)parseClassName;
@property (retain)PFUser * userId;
@property (retain)Place * placeId;
@property (retain)PFFile * image;
@end
