//
//  ResetPassViewController.m
//  testswings
//
//  Created by mm7 on 06.07.14.
//  Copyright (c) 2014 Oleksandr Padalko. All rights reserved.
//

#import "ResetPassViewController.h"
#import "LineView.h"
#import "BSTextField.h"
@interface ResetPassViewController ()
{
    BSTextField * b;
}
@end

@implementation ResetPassViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    UILabel * label=[[UILabel alloc]initWithFrame:CGRectMake(0,0, self.view.frame.size.width, 46)];
    [label setFont:[UIFont BSFontMediumLightOblique]];
    [label setTextAlignment:NSTextAlignmentCenter];
    
    [label setTextColor:[UIColor BSDarkBlueColor]];
    
    [label setText:[locLabelRestorePass localized]];
    [self.view addSubview:label];
    
    LineView * lv=[[LineView alloc]initWithFrame:CGRectMake(0, label.frame.size.height, self.view.frame.size.width, 1)];
    
    [self.view addSubview:lv];
    
    
 b=[[BSTextField alloc]initWithFrame:CGRectMake(0, label.frame.size.height+1, self.view.frame.size.width, 45)];
    b.botBorder=YES;
    
    [b setFont:[UIFont BSFontMediumLightOblique]];
    [b addImage:[UIImage imageNamed:@"Login_ico"]];
    [b setPlaceholder:[locEmail localized]];
    [b setBackgroundColor:[UIColor whiteColor]];
    [b setTextColor:[UIColor BSDarkBlueColor]];
    
    [self.view addSubview:b];
    [b setText:[[PFUser currentUser] email]];
   
    UIButton * b1=[UIButton buttonWithType:UIButtonTypeCustom];
    
    
    [b1 setFrame:CGRectMake(0,0, 165, 35)];
    [b1 setCenter:CGPointMake(self.view.frame.size.width/2,b.frame.origin.y+b.frame.size.height+20+35/2)];
    [b1 setTitle:[[locRestore localized] uppercaseString] forState: UIControlStateNormal];
    [b1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [b1 setBackgroundColor:[UIColor BSDarkBlueColor]];
    [b1.titleLabel setFont:[UIFont BSFontLargeLight]];
    b1.layer.cornerRadius=4;
    [b1 setTag:1112];
    [b1 addTarget:self action:@selector(addPhoto) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:b1];

    
    
    
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
    // [backButton setTitle:@"back" forState:UIControlStateNormal];
    [backButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [backButton setImage:[UIImage imageNamed:@"Back_ico.png"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    // Do any additional setup after loading the view.
}
-(void)back{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)addPhoto{
    
 
        
        if (b.text.length<1) {
            [b setErrorString:@"Please Enter Email"];
            return;
        }
        
        PFQuery *query = [PFUser query];
        
        [query whereKey:@"email" equalTo:b.text];
        
        [query findObjectsInBackgroundWithBlock:^(NSArray *comments, NSError *error) {
            
            if (comments.count==0) {
                UIAlertView * av=       [[UIAlertView alloc]initWithTitle:@"" message:[locResetPassErorr localized] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [av show];
           
            }else{
                PFUser * user=[comments lastObject];
                NSLog(@"%@",user);
                [PFUser requestPasswordResetForEmailInBackground:b.text];
                
         UIAlertView * av=       [[UIAlertView alloc]initWithTitle:@"" message:[locResetPassMessage localized] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [av show];
                
                
                [self back];
                
            }
            
        }];

    
 
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
