//
//  StartScreenViewController.m
//  testswings
//
//  Created by Aleksandr Padalko on 6/20/14.
//  Copyright (c) 2014 Oleksandr Padalko. All rights reserved.
//

#import "StartScreenViewController.h"
#import "BSTextField.h"
#import "BSButton.h"
#import "PlacesViewController.h"
#import "RegistrationViewController.h"
#import <Parse/Parse.h>
#import <MediaPlayer/MediaPlayer.h>
#import "ResetPassViewController.h"
#import "TextViewController.h"
@interface StartScreenViewController (){
    NSMutableArray * fieldsArr;
    BOOL keyBoardShown;
    CGSize keyBoardSize;
    BOOL registerMode;
    BSTextField * tf;
    BSTextField * tf2;
    UIButton * butBackToLogin;
    
    BSTextField  *rLogin;
    BSTextField * rPass1;
    BSTextField * rPass2;
    
    BOOL firsTimeLoginCreate;
    BOOL firsTimeRegisterCreate;
}

@end

@implementation StartScreenViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (![[NSUserDefaults standardUserDefaults] valueForKey:@"firstTime"]) {
        NSBundle *bundle = [NSBundle mainBundle];
        NSString *moviePath = [bundle pathForResource:@"SplashE" ofType:@"mp4"];
        NSURL *movieURL = [NSURL fileURLWithPath:moviePath];
        
        MPMoviePlayerViewController*   theMoviPlayer = [[MPMoviePlayerViewController alloc] initWithContentURL:movieURL];
        //    theMoviPlayer.controlStyle = MPMovieControlStyleFullscreen;
        //
        [theMoviPlayer.moviePlayer setControlStyle:MPMovieControlStyleNone];
        
        [theMoviPlayer.view setFrame:self.view.bounds];
        // theMoviPlayer.view.transform = CGAffineTransformConcat(theMoviPlayer.view.transform, CGAffineTransformMakeRotation(M_PI_2));
        //    UIWindow *backgroundWindow = [[UIApplication sharedApplication] keyWindow];
        //    [theMoviPlayer.view setFrame:backgroundWindow.frame];
        //    [backgroundWindow addSubview:theMoviPlayer.view];
        
        [self presentMoviePlayerViewControllerAnimated:theMoviPlayer];
        
        [[NSUserDefaults standardUserDefaults]setValue:@"sadasd" forKey:@"firstTime"];
    }
 
    
    
    
    
    registerMode=NO;
    
    self.listTableView.bounces=YES;
    UIImage *img=[UIImage imageNamed:@"bg_w_name"];
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:img]];
      [self.navigationController setNavigationBarHidden:YES];
    fieldsArr=[[NSMutableArray alloc]init];
    self.navigationItem.rightBarButtonItem=nil;
        [self.listTableView setPullToRefreshEnabled:NO];
    
       [self.listTableView setContentInset:UIEdgeInsetsMake(200, 0, -400, 0)];
    
    [self createLoginView];
    
    
    
    
  //  [NSTimer scheduledTimerWithTimeInterval:0.016 target:self selector:@selector(update) userInfo:nil repeats:YES];
    
    

	// Do any additional setup after loading the view.
}
-(void)update{
    if (registerMode) {
        [self createLoginView];
    }else{
        [self createRegisterView];
    }
}
-(void)createRegisterView{
    registerMode=YES;
        [self cleanViews];
    butBackToLogin=[UIButton buttonWithType:UIButtonTypeCustom];
    [butBackToLogin setFrame:CGRectMake(8, 4, 45, 45)];
    [butBackToLogin setImage:[UIImage imageNamed:@"Back_ico"] forState:UIControlStateNormal];
    [butBackToLogin addTarget:self action:@selector(goBackToLogin) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:butBackToLogin];
    
    [UIView animateWithDuration:0.33 animations:^{
        [self.listTableView setContentInset:UIEdgeInsetsMake(45, 0, -200, 0)];
    }];
    
    
    
    
    rLogin=[[BSTextField alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 72)];
    
    [rLogin setFont:[UIFont BSFontMediumLightOblique]];
    [rLogin addImage:[UIImage imageNamed:@"Login_ico"]];
    [rLogin setPlaceholder:[locEmail localized]];
    [rLogin setBackgroundColor:[UIColor whiteColor]];
    [rLogin setTextColor:[UIColor BSDarkBlueColor]];
    rLogin.botBorder=YES;
    rLogin.group=locLogin;
    [rLogin setVvTextFieldDelegate:self];
    
    rLogin.layer.masksToBounds = NO;
    rLogin.layer.cornerRadius = 0; // if you like rounded corners
    rLogin.layer.shadowOffset = CGSizeMake(1, -10);
    rLogin.layer.shadowRadius = 10;
    rLogin.layer.shadowOpacity = 0.8;
    
    
    [fieldsArr addObject:rLogin];
    rPass1=[[BSTextField alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 72)];
    rPass1.secureTextEntry=YES;
    [rPass1 setFont:[UIFont BSFontMediumLightOblique]];
    [rPass1 setTextColor:[UIColor BSDarkBlueColor]];
    
    [rPass1 addImage:[UIImage imageNamed:@"Locker_pass"]];
    [rPass1 setPlaceholder:[locPassword localized]];
    [rPass1 setBackgroundColor:[UIColor whiteColor]];
    rPass1.botBorder=YES;
    rPass1.group=locPassword;
    [rPass1 setVvTextFieldDelegate:self];
    [fieldsArr addObject:rPass1];
    
    
    rPass2=[[BSTextField alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 72)];
    
    [rPass2 setFont:[UIFont BSFontMediumLightOblique]];
    [rPass2 setTextColor:[UIColor BSDarkBlueColor]];
    rPass2.secureTextEntry=YES;
    [rPass2 addImage:[UIImage imageNamed:@"Locker_pass"]];
    [rPass2 setPlaceholder:[locRepeatPass localized]];
    [rPass2 setBackgroundColor:[UIColor whiteColor]];
    rPass2.botBorder=YES;
    rPass2.group=locPassword;
    [rPass2 setVvTextFieldDelegate:self];
    [fieldsArr addObject:rPass2];
    
    [self addViewToCell:rLogin toSection:0];
    [self addViewToCell:rPass1 toSection:0];
        [self addViewToCell:rPass2 toSection:0];
    
    UIView * butsView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-45-72*3)];
    
    [butsView setBackgroundColor:[UIColor whiteColor]];
    
    [self addViewToCell:butsView toSection:0];
    BSButton * b=[UIButton buttonWithType:UIButtonTypeCustom];
    
    
    [b setFrame:CGRectMake(0,0, 165, 35)];
    [b setCenter:CGPointMake(butsView.frame.size.width/2,butsView.frame.size.height/2)];
    [b setTitle:[[locRegister localized] uppercaseString] forState: UIControlStateNormal];
    [b setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [b setBackgroundColor:[UIColor BSDarkBlueColor]];
    [b.titleLabel setFont:[UIFont BSFontLargeLight]];
    b.layer.cornerRadius=4;
    [b addTarget:self action:@selector(registerUser) forControlEvents:UIControlEventTouchUpInside];
    [butsView addSubview:b];
    UIView * v=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 200)];
    [v setBackgroundColor:[UIColor whiteColor]];
    
    UILabel * lab=[[UILabel alloc]initWithFrame:CGRectMake(15, 0, self.view.frame.size.width, 34)];
    [lab setFont:[UIFont BSFontSmallLight]];
    [lab setTextColor:[UIColor BSDarkBlueColor]];
    [lab setText:@"By registering I agree with "];
    [lab sizeToFit];
    
    
    [lab setFrame:CGRectMake(self.view.frame.size.width/2-lab.frame.size.width/2-40, butsView.frame.size.height-lab.frame.size.height-70, lab.frame.size.width, lab.frame.size.width)];
    
    
    UIButton * but=[UIButton buttonWithType:UIButtonTypeCustom];
    [but setFrame:CGRectMake(lab.frame.origin.x+lab.frame.size.width, lab.frame.origin.y,40, lab.frame.size.height)];
    [but setTitleColor:[UIColor BSDarkBlueColor] forState:UIControlStateNormal];
    [but.titleLabel setFont:[UIFont BSFontMediumLightOblique]];
    [but setTitle:@"Terms" forState:UIControlStateNormal];
    [butsView addSubview:but];
    [but addTarget:self action:@selector(openTerms) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    [butsView addSubview:lab];
    
    
    
    
    [self addViewToCell:v toSection:0];
    [self.listTableView insertData:[NSArray arrayWithObjects:@"login",@"pass1",@"pass2",@"actionssss",@"lol", nil] toStaticCellAtSection:0];
    
    [self.listTableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
}
-(void)openTerms{
    TextViewController * t=[[TextViewController alloc] initWithText:[NSString stringWithFormat:@"%@",@"BSTERMS.docx"]];
    
    
    
    [self  presentViewController:t animated:YES completion:nil];
}
-(void)goBackToLogin{
    [UIView animateWithDuration:0.33 animations:^{
        [butBackToLogin setAlpha:0];
    }completion:^(BOOL fin){
    
        [butBackToLogin removeFromSuperview];
    
    }];
    
    [self createLoginView];
}
-(void)createLoginView{
    registerMode=NO;
    [self cleanViews];
    [UIView animateWithDuration:0.33 animations:^{
      [self.listTableView setContentInset:UIEdgeInsetsMake(216, 0, -400, 0)];
    }];
     
    tf=[[BSTextField alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 72)];
    
    [tf setFont:[UIFont BSFontMediumLightOblique]];
    [tf addImage:[UIImage imageNamed:@"Login_ico"]];
    [tf setPlaceholder:[locEmail localized]];
    [tf setBackgroundColor:[UIColor whiteColor]];
    [tf setTextColor:[UIColor BSDarkBlueColor]];
    tf.botBorder=YES;
    tf.group=locLogin;
    [tf setClipsToBounds:NO];
    [tf setVvTextFieldDelegate:self];
    [fieldsArr addObject:tf];
    //  [self.view addSubview:tf];
    
    tf.layer.masksToBounds = NO;
    tf.layer.cornerRadius = 0; // if you like rounded corners
    tf.layer.shadowOffset = CGSizeMake(1, -10);
    tf.layer.shadowRadius = 10;
    tf.layer.shadowOpacity = 0.8;
    
    tf2=[[BSTextField alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 72)];
    
    [tf2 setFont:[UIFont BSFontMediumLightOblique]];
    [tf2 setTextColor:[UIColor BSDarkBlueColor]];
    
    [tf2 addImage:[UIImage imageNamed:@"Locker_pass"]];
    [tf2 setPlaceholder:[locPassword localized]];
    [tf2 setBackgroundColor:[UIColor whiteColor]];
    tf2.botBorder=YES;
    tf2.group=locPassword;
    tf2.secureTextEntry=YES;
    [tf2 setVvTextFieldDelegate:self];
    
    [fieldsArr addObject:tf2];
    //  [self.view addSubview:tf2];
    
    
    [self addViewToCell:tf toSection:0];
    [self addViewToCell:tf2 toSection:0];
    
    [self.listTableView insertData:[NSArray arrayWithObjects:@"log",@"pass",@"buttons",@"lol1", nil] toStaticCellAtSection:0];
    
    UIView * butsView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-216-72*2-10)];
    
    [butsView setBackgroundColor:[UIColor whiteColor]];
    
    [self addViewToCell:butsView toSection:0];
    
    UIButton * loginByt=[UIButton buttonWithType:UIButtonTypeCustom];
    [loginByt  setFrame:CGRectMake(butsView.frame.size.width-120, 0, 90, 37)];
    [loginByt setTitle:[locLogin localized] forState:UIControlStateNormal];
    [loginByt.titleLabel setFont:[UIFont BSFontMediumLight]];
    [loginByt setTitleColor:[UIColor BSDarkBlueColor] forState:UIControlStateNormal];
    [loginByt addTarget:self action:@selector(login) forControlEvents:UIControlEventTouchUpInside];
    [butsView addSubview:loginByt];
    UIImageView * arrowImgaeView=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"login_button"]];
    [butsView addSubview:arrowImgaeView];
    
    [arrowImgaeView setCenter:CGPointMake(loginByt.center.x+loginByt.frame.size.width/2, loginByt.center.y)];
    
    [butsView addSubview:arrowImgaeView];
    
    
    
    
    
    UIButton * forgotPass=[UIButton buttonWithType:UIButtonTypeCustom];
    [forgotPass  setFrame:CGRectMake(15, 0, 180, 37)];
    forgotPass.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [forgotPass setTitle:[locForgotPass localized] forState:UIControlStateNormal];
    [forgotPass.titleLabel setFont:[UIFont BSFontMediumLight]];
    [forgotPass setTitleColor:[UIColor BSDarkBlueColor] forState:UIControlStateNormal];
    [forgotPass addTarget:self action:@selector(forgotPass) forControlEvents:UIControlEventTouchUpInside];
    [butsView addSubview:forgotPass];
    
    
    BSButton * b=[UIButton buttonWithType:UIButtonTypeCustom];
    
    
    [b setFrame:CGRectMake(0,0, 165, 35)];
    [b setCenter:CGPointMake(butsView.frame.size.width/2,loginByt.frame.size.height+(butsView.frame.size.height-loginByt.frame.size.height)/2)];
    [b setTitle:[[locRegister localized] uppercaseString] forState: UIControlStateNormal];
    [b setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [b setBackgroundColor:[UIColor BSDarkBlueColor]];
    [b.titleLabel setFont:[UIFont BSFontLargeLight]];
    b.layer.cornerRadius=4;
    [b addTarget:self action:@selector(submit) forControlEvents:UIControlEventTouchUpInside];
    [butsView addSubview:b];
    
    UIView * v=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 400)];
    [v setBackgroundColor:[UIColor whiteColor]];
    
    [self addViewToCell:v toSection:0];
    
    
[self.listTableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
}
-(void)viewDidLayoutSubviews{
    
    [super viewDidLayoutSubviews];
  //  [self.listTableView setFrame:CGRectMake(0, 236-20, self.view.frame.size.width, self.view.frame.size.height-236+20)];

  
    
}

-(CGFloat)listTableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UIView * v=[self getViewOfCellInIndexPath:indexPath];
    return v.frame.size.height;
    
}
-(UITableViewCell *)listTableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath objectForRow:(id)object{
    NSString *   CellIdentifer = [NSString stringWithFormat:@"c2%@",object];
//    UITableViewCell  * cell=[tableView dequeueReusableCellWithIdentifier:CellIdentifer];
//    if (!cell) {
  UITableViewCell*   cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifer];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        //  UIView * v=[self getViewOfCellInSection:indexPath.section atIndex:indexPath.row];
        [cell addSubview:[self getViewOfCellInSection:indexPath.section atIndex:indexPath.row]];
        [cell setClipsToBounds:NO];
        [cell setBackgroundColor:[UIColor clearColor]];
        
        
  //  }
    return  cell;
    
}

-(void)login{
    
    if (tf.text.length==0) {
        UIAlertView * av=  [[UIAlertView alloc]initWithTitle:@"" message:@"Please enter email address" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [av show];
        return;
    }
    
    if (tf2.text.length==0) {
        UIAlertView * av=  [[UIAlertView alloc]initWithTitle:@"" message:@"Please enter password" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [av show];
        return;
    }
    
 
  
    
    [PFUser logInWithUsernameInBackground:tf.text password:tf2.text block:^(PFUser * user,NSError*err){
        if (!err) {
            [self.navigationController setViewControllers:[NSArray arrayWithObject:[[PlacesViewController alloc] init]] animated:YES];
        }else{
            UIAlertView * av=  [[UIAlertView alloc]initWithTitle:@"" message:[err.userInfo valueForKey:@"error"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [av show];
             
        }
    }];
}
-(void)submit{
    
    [self createRegisterView];
    
}
#pragma mark - text field delegates
-(void)textFieldShouldTabBack:(UITextField *)tf{
    
    int idx=[fieldsArr indexOfObject:tf];
    
    if (idx>0) {
        [[fieldsArr objectAtIndex:idx-1] becomeFirstResponder];
    }
    
}
-(void)editingBegin:(UIView *)v{
    
    BSTextField * tf=(BSTextField*)v;
    
  
    
    
    [super editingBegin:v];
    if (keyBoardShown) {
        [self animationMove];
    }
    
    
    
}
-(void)textFieldShouldTabForward:(UITextField *)tf{
    int idx=[fieldsArr indexOfObject:tf];
    
    if (idx<fieldsArr.count-1) {
        [[fieldsArr objectAtIndex:idx+1] becomeFirstResponder];
    }
}

-(void)keyboardWillChangeFrame:(NSNotification *)notification{
    keyBoardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
}

-(void)keyboardWasShown:(NSNotification *)notification{
    keyBoardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    if (!keyBoardShown) {
        [self animationMove];
    }
    
    keyBoardShown=YES;
    
}
-(void)keyboardWasHide:(NSNotification *)notification{
    keyBoardShown=NO;
    [UIView animateWithDuration:0.33 animations:^{
        [self.view setFrame:CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height)];
    }];
}
-(void)animationMove{
    UIView * cr=[self curResponder];
    CGPoint p=  [self.navigationController.view.superview convertPoint:[self curResponder].superview.frame.origin fromView:[self curResponder]];
    
    NSLog(@"%f",p.y);
    
    //flo  deltaMove=keyBoardSize.height-p.y;
    
    
    
    
    float keyY=[UIScreen mainScreen].bounds.size.height- keyBoardSize.height;
    if (p.y+cr.frame.size.height>keyY) {
        [UIView animateWithDuration:0.33 animations:^{
            [self.view setCenter:CGPointMake(self.view.center.x,self.view.center.y- ((cr.frame.size.height+p.y)-keyY)-40)];
        }];
    }else {
        [UIView animateWithDuration:0.33 animations:^{
            [self.view setFrame:CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height)];
        }];
    }
    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)registerUser{
  
        if (rLogin.text.length==0) {
            UIAlertView * av=  [[UIAlertView alloc]initWithTitle:@"" message:@"Please enter email address" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [av show];
         
            return;
        }
        
        
        if (rPass1.text.length==0) {
            UIAlertView * av=  [[UIAlertView alloc]initWithTitle:@"" message:@"Please enter password" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [av show];
            return;
        }
        
        if (rPass2.text.length==0) {
            UIAlertView * av=  [[UIAlertView alloc]initWithTitle:@"" message:@"Please repeat password" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [av show];
            return;
        }
        
        if (![rPass1.text isEqualToString:rPass2.text]) {
            UIAlertView * av=  [[UIAlertView alloc]initWithTitle:@"" message:@"Password's doesn't match" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [av show];
            return;
        }
    
    
    if ([[rLogin.text componentsSeparatedByString:@"@"] count]!=2) {
        UIAlertView * av=  [[UIAlertView alloc]initWithTitle:@"" message:@"Wrong email address" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [av show];
        
        return;
    }else{
        
        
        if ([[[[rLogin.text componentsSeparatedByString:@"@"] lastObject] componentsSeparatedByString:@"."] count]==1) {
            UIAlertView * av=  [[UIAlertView alloc]initWithTitle:@"" message:@"Wrong email address" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [av show];
             return;
        }
        
        
       
        
    }
        
        
        PFUser *user = [PFUser user];
    user.username=rLogin.text;
        user.email =rLogin.text;
        user.password = rPass2.text;
        [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (succeeded) {
                [self.navigationController setViewControllers:[NSArray arrayWithObject:[[PlacesViewController alloc] init]] animated:YES];
            }else{
                UIAlertView * av=  [[UIAlertView alloc]initWithTitle:@"" message:[error.userInfo valueForKey:@"error"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [av show];
                
            }
        }];
        
   
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView.contentOffset.y > self.view.frame.size.height)
    {
        [scrollView setScrollEnabled:NO];
        [scrollView setContentOffset:CGPointMake(0, self.view.frame.size.height)];
    }
    [scrollView setScrollEnabled:YES];
}

-(void)forgotPass{
    ResetPassViewController * r=[[ResetPassViewController alloc]init];
    [self.navigationController pushViewController:r animated:YES];
}

@end
