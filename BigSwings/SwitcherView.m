//
//  SwitcherView.m
//  testswings
//
//  Created by Aleksandr Padalko on 6/20/14.
//  Copyright (c) 2014 Oleksandr Padalko. All rights reserved.
//

#import "SwitcherView.h"
#import "LineView.h"
@interface SwitcherView(){
    
    int switchState;
    UIImageView *stateButtonImage;
}
@end

@implementation SwitcherView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
 LineView * lv =[[LineView alloc]initWithFrame:CGRectMake(0, self.frame.size.height-1 , self.frame.size.width,1)];
        [self addSubview:lv];
        
        
        UIButton * button=[UIButton buttonWithType:UIButtonTypeCustom];
        
        [button setFrame:CGRectMake(self.frame.size.width/2-128/2, 0, 128, self.frame.size.height)];
        
        [button.titleLabel setFont:[UIFont BSFontMediumLight]];
        [button setTitleColor:[UIColor BSDarkBlueColor] forState:UIControlStateNormal];
               [self addSubview:button];
        [button addTarget:self action:@selector(switchOrder) forControlEvents:UIControlEventTouchUpInside];

        
        stateButtonImage=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"tumbler1"]];
        
        [stateButtonImage setCenter:CGPointMake(button.frame.size.width/2, button.frame.size.height/2)];
        
        [button addSubview:stateButtonImage];
        UILabel * lab=[[UILabel alloc]initWithFrame:CGRectMake(0, stateButtonImage.frame.origin.y-13, button.frame.size.width, 11)];
        
        [lab setFont:[UIFont BSFontSmallLight]];
        [lab setTextColor:[UIColor BSBlackColor]];
        [lab setTextAlignment:NSTextAlignmentCenter];
        [lab setText:@"ORDER BY"];
        [button addSubview:lab];
        
        switchState=0;
        
        
        
        UILabel * labD=[[UILabel alloc]initWithFrame:CGRectMake(stateButtonImage.frame.origin.x-stateButtonImage.frame.size.width-4, stateButtonImage.frame.origin.y, stateButtonImage.frame.size.width, stateButtonImage.frame.size.height)];
        
        [labD setFont:[UIFont BSFontExstraSmallLight]];
        [labD setTextColor:[UIColor BSBlackColor]];
        [labD setTextAlignment:NSTextAlignmentRight];
        [labD setText:@"Distance"];
        [button addSubview:labD];
        
        
        
        UILabel * labR=[[UILabel alloc]initWithFrame:CGRectMake(stateButtonImage.frame.origin.x+stateButtonImage.frame.size.width+4, stateButtonImage.frame.origin.y, stateButtonImage.frame.size.width, stateButtonImage.frame.size.height)];
        
        [labR setFont:[UIFont BSFontExstraSmallLight]];
        [labR setTextColor:[UIColor BSBlackColor]];
        [labR setTextAlignment:NSTextAlignmentLeft];
        [labR setText:@"Rating"];
        [button addSubview:labR];
        
        switchState=0;
        

    }
    return self;
}

-(void)switchOrder{
    
    if (switchState==0) {
        switchState=1;
        [stateButtonImage setImage:[UIImage imageNamed:@"tumbler2"]];
         }else{
                     switchState=0;
               [stateButtonImage setImage:[UIImage imageNamed:@"tumbler1"]];
         }

    
    [self.delegate didSwitch:switchState];
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
