//
//  LineView.m
//  Chooos
//
//  Created by Aleksandr Padalko on 5/26/14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "LineView.h"

@implementation LineView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setup];
        [self setBackgroundColor:[UIColor clearColor]];
    }
    return self;
}


-(id)initWithCoder:(NSCoder *)aDecoder{
    
    if (self=[super initWithCoder:aDecoder]){
        [self setup];
        [self setBackgroundColor:[UIColor clearColor]];
    }
    return self;
}
-(void)setup{
    
    _color=[UIColor blackColor];
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetStrokeColorWithColor(context, [_color CGColor]);
    CGContextSetLineWidth(context,1);
    CGContextMoveToPoint(context, 0, 0);
    CGContextAddLineToPoint(context, self.frame.size.width,0);
    CGContextDrawPath(context, kCGPathStroke);
}


@end
