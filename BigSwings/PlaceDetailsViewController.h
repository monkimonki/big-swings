//
//  PlaceDetailsViewController.h
//  testswings
//
//  Created by mm7 on 06.07.14.
//  Copyright (c) 2014 Oleksandr Padalko. All rights reserved.
//

#import "BSViewController.h"

@interface PlaceDetailsViewController : BSViewController
@property (retain,nonatomic)Place * place;
@end
