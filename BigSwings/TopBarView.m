//
//  TopBarView.m
//  BigSwings
//
//  Created by Aleksandr Padalko on 6/19/14.
//  Copyright (c) 2014 Oleksandr Padalko. All rights reserved.
//
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#import "TopBarView.h"
#import "BSTextField.h"
@interface TopBarView ()<VVTextFieldProtocol>{
    
    
    float textFieldSpace;
    BSTextField * searchTextField;
    
}
@end
@implementation TopBarView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        
        
        
        [self setup];
        
    }
    return self;
}
-(void)setup{
        [self setBackgroundColor:[UIColor whiteColor]];
    textFieldSpace=264;
    
 
    
    UIButton * button=[UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"List_view_ico"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(switcherPressed:) forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(textFieldSpace, 0, self.frame.size.width-textFieldSpace, self.frame.size.height)];
    [button setTag:kMapViewTypeMap];
    [self addSubview:button];
    
    self.layer.borderWidth=1.0;
    self.layer.borderColor=[UIColor BSGrayColor].CGColor;
    
    searchTextField =[[BSTextField alloc]initWithFrame:CGRectMake(9, 9, self.frame.size.width-18-(self.frame.size.width- button.frame.origin.x), self.frame.size.height-18)];
    searchTextField.layer.cornerRadius=4;
    [searchTextField setBackgroundColor:[UIColor BSGrayColor]];
    [searchTextField setTextColor:[UIColor BSDarkBlueColor]];
    [searchTextField setPlaceHolderColor:[UIColor grayColor]];
    [searchTextField setPlaceholder:[locSearch localized]];
    [searchTextField setVvTextFieldDelegate:self];
    searchTextField.inputAccessoryView=nil;
    searchTextField.returnKeyType=UIReturnKeySearch;
    
    searchTextField.clearButtonMode = UITextFieldViewModeAlways;
    [self addSubview:searchTextField];
    


}

-(void)switcherPressed:(UIButton*)but{
    
    if (but.tag==kMapViewTypeMap) {
        but.tag=kMapViewTypeList;
        [but setImage:[UIImage imageNamed:@"map_view"] forState:UIControlStateNormal];

        
    }
    else{
            [but setImage:[UIImage imageNamed:@"List_view_ico"] forState:UIControlStateNormal];
        but.tag=kMapViewTypeMap;
    }
    
    [self.delegate topBarView:self didSwitchViewType:but.tag];
    
    
}
#pragma  mark -text field delegate
-(void) editingBegin:(UIView*)v{


}
-(void) editingEndWithView:(UIView*)v{
    
}
-(void) editingEnd{
    [self.delegate topBarView:self searchWithText:searchTextField.text];
}
-(void) editingEndWithText:(NSString*)str{
    
}

-(void)clear{
    
}
-(void)clearWithObj:(id)obj{
    
}


-(void)editingWithText:(NSString*)text{
    
}

#pragma mark -end
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetStrokeColorWithColor(context, [[UIColor BSGrayColor] CGColor]);
    CGContextSetLineWidth(context, 1.0);
    CGContextMoveToPoint(context, textFieldSpace, 0);
    CGContextAddLineToPoint(context, textFieldSpace ,rect.size.height);
    CGContextDrawPath(context, kCGPathStroke);
    
}


@end
