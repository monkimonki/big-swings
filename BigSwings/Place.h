//
//  Place.h
//  testswings
//
//  Created by Aleksandr Padalko on 6/20/14.
//  Copyright (c) 2014 Oleksandr Padalko. All rights reserved.
//

#import <Parse/Parse.h>

@interface Place : PFObject<PFSubclassing>
+ (NSString *)parseClassName;
@property (retain) PFUser *userId;
@property (retain) PFGeoPoint * location;
@property (retain) NSString * textAddres;

@property (retain)NSString * title;
@property (retain)NSNumber * height;
@property (retain)NSString * descriptionText;
@property (retain)NSString * warrnings;





@property (nonatomic)int points;
@property (nonatomic)int rateCounts;
@property (nonatomic,retain)NSString * rated;
@property (nonatomic)float realDistance;

-(NSString*)getRaiting;
@end
