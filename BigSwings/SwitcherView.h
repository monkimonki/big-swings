//
//  SwitcherView.h
//  testswings
//
//  Created by Aleksandr Padalko on 6/20/14.
//  Copyright (c) 2014 Oleksandr Padalko. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol SwitcherViewDelegate <NSObject>
-(void)didSwitch:(int)idx;
@end
@interface SwitcherView : UIView
@property (assign,nonatomic)id<SwitcherViewDelegate>delegate;
@end
