//
//  PlacesViewController.m
//  BigSwings
//
//  Created by Aleksandr Padalko on 6/19/14.
//  Copyright (c) 2014 Oleksandr Padalko. All rights reserved.
//

#import "PlacesViewController.h"
#import "AddPlaceViewController.h"
#import "AddPlaceLitsViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "TopBarView.h"
#import "Place.h"
#import <Parse/Parse.h>
#import "VVListTableView.h"
#import "SwitcherView.h"
#import "PlaceTableCell.h"
#import "PlaceDetailsViewController.h"
#import "TopPlacesViewController.h"
@interface MyMarker : GMSMarker
@property (nonatomic)int idx;

@end
@implementation MyMarker



@end

@interface PlacesViewController ()<VVListTableViewDelegate,SwitcherViewDelegate,GMSMapViewDelegate>
{
    NSMutableArray * placesArr;
    GMSMapView *mapView;
    GMSCameraPosition *camera;
    VVListTableView * tablePlaceView;
    TopBarView * tbv;
    SwitcherView * switcher;
    
    UIButton *but;
    
    BOOL firstTime;
    NSMutableArray * dataArray;
    int swithcState;
    
    NSString * searchedText;
    
    NSMutableArray * curArray;
}
@end

@implementation PlacesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self setTitle:[locBigSwingsTitle localized]];
    
    if (mapView) {
        [self setupMap];
    }else{
        [self setupTable];
    }
}
-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    if (!firstTime) {
        
        firstTime=YES;
    tbv=[[TopBarView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    
    [self.view addSubview:tbv];
    [tbv setDelegate:self];
    
    but=[UIButton buttonWithType:UIButtonTypeCustom];
    
    [but addTarget:self action:@selector(topAction) forControlEvents:UIControlEventTouchUpInside];
    [but setFrame:CGRectMake(0, self.view.frame.size.height-44, self.view.frame.size.width, 44)];
        but.layer.borderWidth=1.0;
        but.layer.borderColor=[UIColor BSGrayColor].CGColor;
        [but.titleLabel setFont:[UIFont BSFontLargeLight]];
        [but setTitleColor:[UIColor BSDarkBlueColor] forState:UIControlStateNormal];
        
        [but setTitle:[locTopPlaces localized] forState:UIControlStateNormal];
        
    [self.view addSubview:but];
    
    
    
    
    
    
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
 //   [backButton setTitle:@"Add" forState:UIControlStateNormal];
    [backButton setImage:[UIImage imageNamed:@"add_place_ico"] forState:UIControlStateNormal];
    [backButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(addPlace) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
        [[DataSingleton sharedInstance]updateCurLocation:^(CLLocation *loc) {
            [[DataSingleton sharedInstance]loadMapObjects:^(BOOL suc){
                
                [self setupMap];
                
            }];
            if (loc) {
                
                
                
                
            }
        }];
}
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    self.navigationController.interactivePopGestureRecognizer.delegate = (id<UIGestureRecognizerDelegate>)self;

    [self.navigationController setNavigationBarHidden:NO animated:YES];
    firstTime=NO;
    
 
    	// Do any additional setup after loading the view.
}
-(void)setupTable{
    
    if (tablePlaceView) {
        [tablePlaceView removeFromSuperview];
        tablePlaceView=nil;
       
        
    }
    if (switcher) {
        [switcher removeFromSuperview];
        switcher=nil;
    }
    if (mapView) {
        [mapView removeFromSuperview];
        mapView=nil;
        camera=nil;
        
    }
    
    switcher=[[SwitcherView alloc]initWithFrame:CGRectMake(0, tbv.frame.size.height+tbv.frame.origin.y, self.view.frame.size.width, 44)];
    [self.view addSubview:switcher];
    
    tablePlaceView=[[VVListTableView alloc]initWithFrame:CGRectMake(0, switcher.frame.size.height+switcher.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height-switcher.frame.origin.y-switcher.frame.size.height-but.frame.size.height)];
    [switcher setDelegate:self];
    VVEntity * en=[VVEntity create];
    en.name=[Place parseClassName];
    
    VVParametrsObject * par=[VVParametrsObject createWithEntity:en];
    par.pageOrigin=0;
    par.pageSize=10;

    [tablePlaceView setVvListTableViewDelegate:self];
    
    if (searchedText.length>0) {
        
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"title contains[c] %@",searchedText]; // if you need case sensitive search avoid '[c]' in the predicate
        
        NSArray *results = [[DataSingleton sharedInstance].mapObjectsArray filteredArrayUsingPredicate:predicate];
          [tablePlaceView insertData:results toStaticCellAtSection:0];
        
    }else{
          [tablePlaceView insertData:[DataSingleton sharedInstance].mapObjectsArray toStaticCellAtSection:0];
    }
    
    
    [self.view addSubview:tablePlaceView];
}
-(void)setupMap{
    
    if (mapView) {
        [mapView removeFromSuperview];
        mapView=nil;
        camera=nil;
   
    }
    if (tablePlaceView) {
        [tablePlaceView removeFromSuperview];
        tablePlaceView=nil;
    }
    camera = [GMSCameraPosition cameraWithLatitude:49.868
                                         longitude:-76.2086
                                              zoom:1];
    mapView = [GMSMapView mapWithFrame:CGRectZero camera:camera];
    [mapView setMapType:kGMSTypeHybrid];
    [mapView setDelegate:self];
    [mapView setFrame:CGRectMake(0, tbv.frame.size.height+tbv.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height-tbv.frame.origin.y-tbv.frame.size.height-but.frame.size.height)];
    [self.view  addSubview:mapView];
    [self load];
}
-(void)load{
    
    
    if (searchedText.length>0) {
       
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"title contains[c] %@ OR textAddres contains[c] %@",searchedText,searchedText]; // if you need case sensitive search avoid '[c]' in the predicate
        
        NSArray *results = [[DataSingleton sharedInstance].mapObjectsArray filteredArrayUsingPredicate:predicate];
        
        
        for (int a=0;a<results.count;a++) {
            Place * place=(Place*)[results objectAtIndex:a];
            
            MyMarker *marker = [[MyMarker alloc] init];
            marker.position = CLLocationCoordinate2DMake(place.location.latitude, place.location.longitude);
            if (place.textAddres.length>0) {
                    marker.snippet = [NSString stringWithFormat:@"%@\n%@",place.title,place.textAddres];
            }else{
                      marker.snippet = [NSString stringWithFormat:@"%@",place.title];
            }
     
            marker.appearAnimation = kGMSMarkerAnimationPop;
            marker.map = mapView;
            marker.icon = [UIImage imageNamed:@"point"];
            marker.idx=a;
           
        }
        
        
        curArray=results;
        
    }else{
    
        for (int a=0;a<[DataSingleton sharedInstance].mapObjectsArray.count;a++) {
              Place * place=(Place*)[[DataSingleton sharedInstance].mapObjectsArray objectAtIndex:a];
    
    
                    MyMarker *marker= [[MyMarker alloc] init];
                      marker.position = CLLocationCoordinate2DMake(place.location.latitude, place.location.longitude);
                  marker.snippet = place.title;
                      marker.appearAnimation = kGMSMarkerAnimationPop;
              marker.map = mapView;
            if (place.textAddres.length>0) {
                marker.snippet = [NSString stringWithFormat:@"%@\n%@",place.title,place.textAddres];
            }else{
                marker.snippet = [NSString stringWithFormat:@"%@",place.title];
            }
            marker.icon = [UIImage imageNamed:@"point"];
        
           marker.idx=a;
        
    }
        
        curArray=[DataSingleton sharedInstance].mapObjectsArray;
    }
    
//    PFQuery * query=[PFQuery queryWithClassName:@"Place"];
//    query.limit=1000;
//    
//    [query findObjectsInBackgroundWithBlock:^(NSArray* obj,NSError  * err){
//    
//        if (!err) {
//            
//            dataArray=[[NSMutableArray alloc]initWithArray:obj];
//            for (int a=0; a<obj.count; a++) {
//                
//                Place * p=[obj objectAtIndex:a];
//                
//                GMSMarker *marker = [[GMSMarker alloc] init];
//                marker.position = CLLocationCoordinate2DMake(p.location.latitude, p.location.longitude);
//                marker.snippet = p.title;
//                marker.appearAnimation = kGMSMarkerAnimationPop;
//                marker.map = mapView;
//                
//                
//                
//            }
//        }
//        
//    
//    
//    }];
//    
}




-(void)addPlace{
    AddPlaceLitsViewController * adPlaceVC=[[AddPlaceLitsViewController alloc]init];
    
    

    [UIView  beginAnimations:nil context:NULL];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:0.75];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:self.navigationController.view cache:NO];
    [UIView commitAnimations];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelay:0.375];
    [self.navigationController pushViewController:adPlaceVC animated:NO];
    [UIView commitAnimations];
    
   
}
-(void)topAction{
    
    [UIView  beginAnimations:nil context:NULL];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:0.75];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:self.navigationController.view cache:NO];
    [UIView commitAnimations];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelay:0.375];
    [self.navigationController pushViewController:[[TopPlacesViewController alloc] init] animated:NO];
    [UIView commitAnimations];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - listTableViewDelegate
-(CGFloat)listTableView:(UITableView *)tableView heightForLoadCellAtIndexPath:(NSIndexPath *)indexPath{
    
    
    return 20;
}
-(CGFloat)listTableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 51;
}
-(CGFloat)listTableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0;
}
-(void)listTableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath withObject:(Place *)object{
    
    PlaceDetailsViewController * pld=[[PlaceDetailsViewController alloc]init];
    pld.place=object;
    
    [self.navigationController pushViewController:pld animated:YES];
    
    
}
-(void)listTableViewDidLoadData:(id)data forHeader:(NSUInteger)section{
    
    NSLog(@"%@",data);
    
}
-(UIView *)listTableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    
    return nil;
}
-(CGFloat)listTableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0;
}
-(UIView *)listTableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section withObject:(id)object{
    return nil;
}
-(void)listTableView:(UITableView*)listTableView refresh:(void (^)( BOOL success ) )cb{
    
    cb(YES);
}
-(UITableViewCell *)listTableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath objectForRow:(VVObject *)object {
	NSLog(@"delegate open");
    NSLog(@"%@",object.objectId);
    static NSString *CellIdentifer = @"simpleCell";
	
	PlaceTableCell  * cell=[tableView dequeueReusableCellWithIdentifier:CellIdentifer];
	
	if (!cell) {
        [cell setBackgroundColor:[UIColor orangeColor]];
		cell=[[PlaceTableCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifer];
	}
    [cell setPlace:object andIndex:indexPath.row];
	return  cell;
	
}
-(VVParametrsObject *)paramsForLoad:(VVParametrsObject*)params forIndexPath:(NSIndexPath*)indexPath {
	
	return params;
}
-(UITableViewCell *)listTableView:(UITableView *)tableView loadingCellViewForRowAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *CellIdentifer = @"loadCell";
	
	UITableViewCell  * cell=[tableView dequeueReusableCellWithIdentifier:CellIdentifer];
	if (!cell) {
		cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifer];
	}
	return  cell;
}

#pragma  mark - top bar delegate
-(void)topBarView:(TopBarView*)topBarView didSwitchViewType:(enum kMapViewType)type{
    switch (type) {
        case kMapViewTypeList:
            [self setupTable];
            break;
        case kMapViewTypeMap:
                        [self setupMap];
            break;
        default:
            break;
    }
}
-(void)topBarView:(TopBarView*)topBarView startEdidtingTextField:(UITextField*)tf{
    
}
-(void)topBarView:(TopBarView*)topBarView endEdidtingTextField:(UITextField*)tf{
    
}
-(void)topBarView:(TopBarView *)topBarView searchWithText:(NSString*)text{
  
    if (text.length==0) {
        
    }else{
        
    }
 
      searchedText=text;
    if (tablePlaceView) {
        [self setupTable];
    }else{
        [self setupMap];
    }
}
#pragma mark - swithc deelgate
-(void)didSwitch:(int)idx{
    
    swithcState=idx;
    
    if (swithcState==0) {
          [tablePlaceView insertData:[DataSingleton sharedInstance].mapObjectsArray toStaticCellAtSection:0];
    }else{
              [tablePlaceView insertData:[DataSingleton sharedInstance].mapObjectsArrayRating toStaticCellAtSection:0];
    }
    
    [tablePlaceView reloadData];
    
}

- (void)mapView:(GMSMapView *)mapView
didTapInfoWindowOfMarker:(MyMarker *)marker{
    Place * object=[curArray objectAtIndex:marker.idx];
    
    PlaceDetailsViewController * pld=[[PlaceDetailsViewController alloc]init];
    pld.place=object;
    
    [self.navigationController pushViewController:pld animated:YES];
}
-(UIView *)mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker{
    UILabel * lab=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 54)];
    [lab setFont:[UIFont BSFontExstraSmallLight]];
    [lab setTextColor:[UIColor BSDarkBlueColor]];
    [lab setNumberOfLines:5];
    [lab setTextAlignment:NSTextAlignmentCenter];
    
    [lab setText:marker.snippet];
    
    [lab sizeToFit];
    
    
    UIView * v=[[UIView alloc]initWithFrame:CGRectMake(0, 0, lab.frame.size.width+40, lab.frame.size.height+10)];
    
    [lab setCenter:CGPointMake(v.frame.size.width/2, v.frame.size.height/2)];
    [v addSubview:lab];
    UIImageView * arrow=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"arrow"]];
    [arrow setFrame:CGRectMake(lab.frame.origin.x+lab.frame.size.width+7, v.frame.size.height/2-arrow.frame.size.height/2, arrow.frame.size.width, arrow.frame.size.height)];
    [v addSubview:arrow];
    [v setBackgroundColor:[UIColor whiteColor]];
    v.layer.cornerRadius=4;
    v.clipsToBounds=YES;
    UIView * mainView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, v.frame.size.width, v.frame.size.height+10)];
    [mainView setBackgroundColor:[UIColor clearColor]];
    
    [mainView addSubview:v];
    
    UIImageView * imgv=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"triangle"]];
    [imgv setFrame:CGRectMake(v.frame.size.width/2+v.frame.origin.x, v.frame.size.height+v.frame.origin.y-1, imgv.frame.size.width, imgv.frame.size.height)];
    
    [mainView addSubview:imgv];
    
    
    
    
    
    return mainView;
}
@end
