//
//  FrameSettings.m
//  testswings
//
//  Created by Aleksandr Padalko on 6/19/14.
//  Copyright (c) 2014 Oleksandr Padalko. All rights reserved.
//

#import "FrameSettings.h"

@implementation FrameSettings

static FrameSettings * inst;

+(FrameSettings*)sharedInstance{
    if (inst==NULL) {
        inst=[[self alloc]init];
    }
    return inst;
}


-(id)init{
    if (self=[super init]) {
        
    }
    return self;
}
@end
