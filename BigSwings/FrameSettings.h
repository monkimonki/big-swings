//
//  FrameSettings.h
//  testswings
//
//  Created by Aleksandr Padalko on 6/19/14.
//  Copyright (c) 2014 Oleksandr Padalko. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface FrameSettings : NSObject
+(FrameSettings*)sharedInstance;
@end
