//
//  TextViewController.m
//  tutoringondemand
//
//  Created by mm7 on 21.04.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "TextViewController.h"

@interface TextViewController ()<UIWebViewDelegate>{
    NSString * str;
}

@end

@implementation TextViewController
-(id)initWithText:(NSString*)text{
    if (self=[super init]) {
        str=text;
        
    }
    return self;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
        [super viewDidLoad];
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    UIView *toolBar;
    
    toolBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0,self.view.frame.size.width , 64)];
    toolBar.frame = CGRectMake(0, 0, self.view.frame.size.width, 64);
   
    [toolBar setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_w_name"]]];
    
   
  
    
    
    //    UIView * blueView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 20)];
//    [blueView setBackgroundColor:[FrameSettings sharedInstace].mainBlueColor];
//    [self.view addSubview:blueView];
   


    
    

 
    
    
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(20, 20, 44, 44)];
    // [backButton setTitle:@"back" forState:UIControlStateNormal];
    [backButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [backButton setImage:[UIImage imageNamed:@"Back_ico.png"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(doneAction) forControlEvents:UIControlEventTouchUpInside];
   
    
    [toolBar addSubview:backButton];
    
    UILabel * lab=[[UILabel alloc]initWithFrame:CGRectMake(backButton.frame.size.width+backButton.frame.origin.x, backButton.frame.origin.y, self.view.frame.size.width-(backButton.frame.size.width+backButton.frame.origin.x)*2 , backButton.frame.size.height)];
    
    [lab setFont:[UIFont BSFontLargeLight]];
    
    [lab setText:@"Terms of Use"];
    [lab setTextAlignment:NSTextAlignmentCenter];
    [lab setTextColor:[UIColor whiteColor]];
    [lab setBackgroundColor:[UIColor clearColor]];
    [toolBar addSubview:lab];
    
    
    
    
    
    [self.view addSubview:toolBar];
    // UITextView * tv=[[UITextView alloc]initWithFrame:CGRectMake(0, toolBar.frame.origin.y+toolBar.frame.size.height, toolBar.frame.size.width, self.view.frame.size.height-toolBar.frame.size.height-toolBar.frame.origin.y)];
//    [tv setFont:[FrameSettings sharedInstace].regularFont2];
    
  //  [tv setTextColor:[UIColor blackColor]];

  //  [tv setText:@"asda sd asd sad asdasd asd a"];
  //
    
   // [self.view addSubview:tv];
    
    
    NSString *path = [[NSBundle mainBundle] pathForResource:str ofType:nil];
    NSURL *url = [NSURL fileURLWithPath:path];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    UIWebView * wv=[[UIWebView alloc]initWithFrame:CGRectMake(0, toolBar.frame.origin.y+toolBar.frame.size.height, toolBar.frame.size.width, self.view.frame.size.height-toolBar.frame.size.height-toolBar.frame.origin.y)];
    [wv loadRequest:request];
    [wv setDelegate:self];
   wv.scalesPageToFit = YES;
    [self.view addSubview:wv];
    
	// Do any additional setup after loading the view.
}


- (void)webViewDidFinishLoad:(UIWebView *)theWebView
{
    CGSize contentSize = theWebView.scrollView.contentSize;
    CGSize viewSize = self.view.bounds.size;
    
    float rw = viewSize.width / contentSize.width;
    
    theWebView.scrollView.minimumZoomScale = rw;
    theWebView.scrollView.maximumZoomScale = rw;
    theWebView.scrollView.zoomScale = rw;
}
-(void)doneAction{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
