//
//  SelectPlaceViewController.m
//  bigswings
//
//  Created by mm7 on 09.07.14.
//  Copyright (c) 2014 Oleksandr Padalko. All rights reserved.
//

#import "SelectPlaceViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "LoadVIew.h"
@interface SelectPlaceViewController ()<GMSMapViewDelegate>
{
    GMSMapView *mapView;
    GMSCameraPosition *camera;
}
@end

@implementation SelectPlaceViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
    // [backButton setTitle:@"back" forState:UIControlStateNormal];
    [backButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [backButton setImage:[UIImage imageNamed:@"Back_ico.png"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    
    camera = [GMSCameraPosition cameraWithLatitude:49.868
                                         longitude:-76.2086
                                              zoom:1];
    mapView = [GMSMapView mapWithFrame:CGRectZero camera:camera];
    [mapView setMapType:kGMSTypeHybrid];
    [mapView setDelegate:self];
    
    UILabel * label=[[UILabel alloc]initWithFrame:CGRectMake(15, 10, self.view.frame.size.width-30, 50-20)];
    [label setNumberOfLines:2];
    [label setTextColor:[UIColor blackColor]];
    [label setFont:[UIFont BSFontMediumLight]];
    [label setText:@"Tap enywhere on map to select your place"];
    [label setTextAlignment:NSTextAlignmentCenter];
    UIView * v=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    
    [v setBackgroundColor:[UIColor whiteColor]];
    
    [label setCenter:CGPointMake(v.frame.size.width/2, v.frame.size.height/2)];
    
    [v addSubview:label];
    
    [self.view addSubview:v];
    [mapView setFrame:CGRectMake(0, v.frame.size.height+v.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height-v.frame.origin.y-v.frame.size.height)];
    [self.view  addSubview:mapView];

    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setTitle:@"Select Place"];
}
-(void)back{
    CATransition *transition = [CATransition animation];
    transition.duration = 0.45;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault];
    transition.type = kCATransitionFromBottom;
    [transition setType:kCATransitionPush];
    transition.subtype = kCATransitionFromBottom;
    transition.delegate = self;
    [self.navigationController.view.layer addAnimation:transition forKey:nil];
    
    self.navigationController.navigationBarHidden = NO;
    [self.navigationController popViewControllerAnimated:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate{
  LoadVIew * lv=  [LoadVIew create];
    CLLocation * l=[[CLLocation alloc]initWithLatitude:coordinate.latitude longitude:coordinate.longitude];
    CLGeocoder *geo = [[CLGeocoder alloc] init];
    [geo reverseGeocodeLocation:l completionHandler:^(NSArray *placemarks, NSError *error){
        [lv dissmiss];
        if (placemarks.count) {
            CLPlacemark *place=[placemarks firstObject];
            NSArray * arr=[[place addressDictionary] objectForKey:@"FormattedAddressLines"];
            NSString * totaStr=[arr firstObject];
            for(int a=1 ;a<arr.count;a++){
                totaStr=[NSString stringWithFormat:@"%@, %@",totaStr,[arr objectAtIndex:a]];
            }
            
            [self.delegate placeSelectedWithAddress:totaStr andLocation:l];
            
            

        }else{
        UIAlertView * av =     [[UIAlertView alloc]initWithTitle:@"" message:@"some error of getting coordinates" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [av show];
        }
        [self back];
    }];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
