//
//  AddPlaceViewController.m
//  testswings
//
//  Created by Aleksandr Padalko on 6/19/14.
//  Copyright (c) 2014 Oleksandr Padalko. All rights reserved.
//

#import "AddPlaceViewController.h"
#import "BSTextField.h"
#import "VVViewController+VVTextField.h"
#import "LineView.h"
#import "AlertTextView.h"
#import "BSButton.h"
#import "JMMLocationEngine.h"
#import "SNImagePickerNC.h"
#import "LoadVIew.h"
#import "Place.h"

@interface AddPlaceViewController ()<UITextViewDelegate,AlertTextViewDelegate,SNImagePickerDelegate >{
    float textFieldSize;
    
    NSMutableArray * fieldsArr;
    CGSize keyBoardSize;
    BOOL keyBoardShown;
    NSMutableArray * photosArr;
    CLLocation * curLocation;
    
    LoadVIew * loadView;
}
@property (strong, nonatomic) SNImagePickerNC *imagePickerNavigationController;
@end

@implementation AddPlaceViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    textFieldSize=58;
    [self.view setBackgroundColor:[UIColor whiteColor]];
    fieldsArr =[[NSMutableArray alloc]init];
    NSMutableArray * arr=[[NSMutableArray alloc]initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"AddPlace" ofType:@".plist"]];
    
    for (int a=0; a<arr.count; a++) {
        NSDictionary * singleDict=[arr objectAtIndex:a];
        
        if ([[singleDict valueForKey:@"name"] isEqualToString:locDescr]||[[singleDict valueForKey:@"name"] isEqualToString:locLocation]) {
            AlertTextView *atv=[[AlertTextView alloc]initWithFrame:CGRectMake(22+textFieldSize+15, 64+textFieldSize*a,self.view.frame.size.width -(22+textFieldSize+15)-22, textFieldSize)];
            [atv setDelegate:self];
            [atv setPlaceholder:[[singleDict valueForKey:@"name"] localized]];
            [atv setPlaceholderTextColor:[UIColor grayColor]];
            [atv setAlertDelegate:self];
                     [atv setFont:[UIFont systemFontOfSize:12]];
            [self.view addSubview:atv];
            atv.group=[singleDict valueForKey:@"name"];
            UIImageView * imgv=[[UIImageView alloc]initWithFrame:CGRectMake(22, atv.frame.origin.y  , textFieldSize*.5, textFieldSize)];
            //[imgv setBackgroundColor:[UIColor purpleColor]];
            [imgv setContentMode:UIViewContentModeCenter];
            [imgv setImage:[UIImage imageNamed:[singleDict valueForKey:@"name"]]];
            [self.view addSubview:imgv];
            LineView * lv=[[LineView alloc]initWithFrame:CGRectMake(0, atv.frame.origin.y+atv.frame.size.height-0.5, self.view.frame.size.width, 1)];
            
            [self.view addSubview:lv];
            
            [fieldsArr addObject: atv];
        }else{
        BSTextField * tf=[[BSTextField alloc]initWithFrame:CGRectMake(0, 64+textFieldSize*a, self.view.frame.size.width, textFieldSize)];
            
            [tf setFont:[UIFont systemFontOfSize:12]];
        [tf addImage:[UIImage imageNamed:[singleDict valueForKey:@"name"]]];
        [tf setPlaceholder:[[singleDict valueForKey:@"name"] localized]];
        //[tf setBackgroundColor:[UIColor orangeColor]];
        tf.botBorder=YES;
        tf.group=[singleDict valueForKey:@"name"];
        [tf setVvTextFieldDelegate:self];
        [fieldsArr addObject:tf];
        [self.view addSubview:tf];
            
        }
        
        
    }
    
    
    AlertTextView *atv=[[AlertTextView alloc]initWithFrame:CGRectMake(22+textFieldSize+15,64+arr.count*textFieldSize ,self.view.frame.size.width -(22+textFieldSize+15)-22, 64)];
    [atv setDelegate:self];
    [atv setAlertDelegate:self];
    [atv setGroup:locWarrinig];
    [self.view addSubview:atv];
       [fieldsArr addObject:atv];
    UIImageView * imgv=[[UIImageView alloc]initWithFrame:CGRectMake(22, atv.frame.origin.y  , textFieldSize, textFieldSize)];
   // [imgv setBackgroundColor:[UIColor purpleColor]];
    [imgv setContentMode:UIViewContentModeCenter];
    [imgv setImage:[UIImage imageNamed:locWarrinig]];
    [self.view addSubview:imgv];
    LineView * lv=[[LineView alloc]initWithFrame:CGRectMake(0, atv.frame.origin.y+atv.frame.size.height, self.view.frame.size.width, 1)];
    
    [self.view addSubview:lv];
    
    
    
    
    UIButton * butCurrentLocation=[UIButton buttonWithType:UIButtonTypeCustom];
    [butCurrentLocation setTitle:@"current" forState:UIControlStateNormal];
    [butCurrentLocation setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    [butCurrentLocation setFrame:CGRectMake(0, lv.frame.origin.y+lv.frame.size.height, lv.frame.size.width/2, 47)];
    [butCurrentLocation addTarget:self action:@selector(curentLocation) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:butCurrentLocation];
    
    
    UIButton * butAddPhoto=[UIButton buttonWithType:UIButtonTypeCustom];
    [butAddPhoto setTitle:@"addPhoto" forState:UIControlStateNormal];
    [butAddPhoto setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [butAddPhoto addTarget:self action:@selector(addPhoto) forControlEvents:UIControlEventTouchUpInside];
    [butAddPhoto setFrame:CGRectMake( lv.frame.size.width/2, lv.frame.origin.y+lv.frame.size.height, lv.frame.size.width/2,47)];
    
    [self.view addSubview:butAddPhoto];
    LineView * lv2=[[LineView alloc]initWithFrame:CGRectMake(0, butAddPhoto.frame.origin.y+butAddPhoto.frame.size.height, self.view.frame.size.width, 1)];
    
    
    
    BSButton * b=[UIButton buttonWithType:UIButtonTypeCustom];
    
    
    [b setFrame:CGRectMake(0,0, 165, 35)];
    [b setCenter:CGPointMake(self.view.frame.size.width/2,lv2.frame.origin.y+ (self.view.frame.size.height-lv2.frame.origin.y)/2)];
    [b setTitle:@"Submit" forState: UIControlStateNormal];
    [b setBackgroundColor:[UIColor blueColor]];
    [b addTarget:self action:@selector(submit) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:b];
    
    
    
    
    [self.view addSubview:lv2];
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
   // [backButton setTitle:@"back" forState:UIControlStateNormal];
       [backButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [backButton setImage:[UIImage imageNamed:@"Back_ico.png"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];

    
	// Do any additional setup after loading the view.
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    
    [self editingBegin:textView];
    
    return YES;
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    return YES;
}



-(void)back{
    [UIView  beginAnimations:nil context:NULL];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:0.75];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:self.navigationController.view cache:NO];
    [UIView commitAnimations];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelay:0.375];
    [self.navigationController popViewControllerAnimated:NO];
    [UIView commitAnimations];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - text field delegates
-(void)textFieldShouldTabBack:(UITextField *)tf{
    
    int idx=[fieldsArr indexOfObject:tf];
    
    if (idx>0) {
        [[fieldsArr objectAtIndex:idx-1] becomeFirstResponder];
    }
 
}
-(void)editingBegin:(UIView *)v{
    
    BSTextField * tf=(BSTextField*)v;
    
    if ([tf.group isEqualToString:locLocation]&&curLocation) {
        curLocation=nil;
        tf.text=@"";
    }
    
    
    [super editingBegin:v];
    if (keyBoardShown) {
        [self animationMove];
    }
    
    
    
}
-(void)textFieldShouldTabForward:(UITextField *)tf{
    int idx=[fieldsArr indexOfObject:tf];
    
    if (idx<fieldsArr.count-1) {
        [[fieldsArr objectAtIndex:idx+1] becomeFirstResponder];
    }
}

-(void)keyboardWillChangeFrame:(NSNotification *)notification{
    keyBoardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
}

-(void)keyboardWasShown:(NSNotification *)notification{
    keyBoardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    if (!keyBoardShown) {
        [self animationMove];
    }
    
    keyBoardShown=YES;
    
}
-(void)keyboardWasHide:(NSNotification *)notification{
    keyBoardShown=NO;
    [UIView animateWithDuration:0.33 animations:^{
        [self.view setFrame:CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height)];
    }];
}
-(void)animationMove{
    UIView * cr=[self curResponder];
    CGPoint p=  [self.navigationController.view.superview convertPoint:[self curResponder].superview.frame.origin fromView:[self curResponder]];
    
    NSLog(@"%f",p.y);
    
  //flo  deltaMove=keyBoardSize.height-p.y;
    
    
 

    float keyY=[UIScreen mainScreen].bounds.size.height- keyBoardSize.height;
    if (p.y+cr.frame.size.height>keyY) {
        [UIView animateWithDuration:0.33 animations:^{
            [self.view setCenter:CGPointMake(self.view.center.x,self.view.center.y- ((cr.frame.size.height+p.y)-keyY)-40)];
        }];
    }else {
        [UIView animateWithDuration:0.33 animations:^{
            [self.view setFrame:CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height)];
        }];
    }
    
    
    
}


#pragma mark - actions
-(void)curentLocation{
    [JMMLocationEngine getBallParkLocationOnSuccess:^(CLLocation *loc) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self getPlacemarksPressed:Nil];
            curLocation=loc;
        });
    } onFailure:^(NSInteger failCode) {
    }];
}

- (IBAction)getPlacemarksPressed:(id)sender {

    [JMMLocationEngine getPlacemarkLocationOnSuccess:^(CLPlacemark *place) {
        dispatch_async(dispatch_get_main_queue(), ^{
            for (BSTextField * tf in fieldsArr) {
              
                    if ([tf.group isEqualToString:locLocation]) {
                        
                        NSLog(@"%@",[place addressDictionary]);
              
             
                             NSArray * arr=[[place addressDictionary] objectForKey:@"FormattedAddressLines"];
                                  NSString * totaStr=[arr firstObject];
                        for(int a=1 ;a<arr.count;a++){
                            totaStr=[NSString stringWithFormat:@"%@, %@",totaStr,[arr objectAtIndex:a]];
                        }
                   
                        [tf setText:totaStr];
                        
                        break;
                    }
                
            
            }
 
        });
    } onFailure:^(NSInteger failCode) {
        
    }];
}

-(void)addPhoto{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"SNPicker" bundle:nil];
    self.imagePickerNavigationController = [storyboard instantiateViewControllerWithIdentifier:@"ImagePickerNC"];
    [self.imagePickerNavigationController setModalPresentationStyle:UIModalPresentationFullScreen];
    self.imagePickerNavigationController.imagePickerDelegate = self;
    self.imagePickerNavigationController.pickerType = kPickerTypePhoto;
    [self presentViewController:self.imagePickerNavigationController animated:YES completion:^{ }];

}
- (void)imagePicker:(SNImagePickerNC *)imagePicker didFinishPickingWithMediaInfo:(NSMutableArray *)info
{
   
    photosArr=[[NSMutableArray alloc]init];
     for (int i = 0; i < info.count; i++) {
     ALAssetsLibrary *assetLibrary=[[ALAssetsLibrary alloc] init];
     [assetLibrary assetForURL:info[i] resultBlock:^(ALAsset *asset) {
     UIImage *image = [UIImage imageWithCGImage:[asset aspectRatioThumbnail]];
         
         [photosArr addObject:image];
         
     } failureBlock:^(NSError *error) {     }];
     }
     
   
}

- (void)imagePickerDidCancel:(SNImagePickerNC *)imagePicker
{
    
}
-(void)submit{
    

    for (BSTextField * tf in fieldsArr) {
        if ([tf.group isEqualToString:locTitle]&&tf.text.length==0) {
            UIAlertView * av=[[UIAlertView alloc]initWithTitle:@"" message:@"Enter a Tititle" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [av show];
            
            return;
        }
        
        if ([tf.group isEqualToString:locLocation]&&tf.text.length==0) {
            UIAlertView * av=[[UIAlertView alloc]initWithTitle:@"" message:@"Enter a Location" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [av show];
            return;
        }
   
    }
    
 loadView=  [LoadVIew  create];
    if (!curLocation) {
        NSString * findStr;
        for (BSTextField * tf in fieldsArr) {
            if ([tf.group isEqualToString:locLocation]) {
                findStr=tf.text;
            }
            
        }
        
        [JMMLocationEngine getGooglePlacesWithString:findStr onSuccess:^(NSArray * arr){
            NSLog(@"%@",arr);
              [self saveLocation];
        } onFailure:^(NSError * err){
      
        
        
        }];
    }else{
        [self saveLocation];
    }

    
    
    
    
    
    
}
static int counter=0;
-(void)saveLocation{
    
    counter++;
    
    
//    float a1=((arc4random()%100 ));
//    
//    float a2=(arc4random()%1000);
//    
//    float b1=a1/a2;
    
  double a=  curLocation.coordinate.latitude+ (arc4random()%500 )/(arc4random()%1000);
    double b=curLocation.coordinate.longitude + (arc4random()%500 )/(arc4random()%1000);

    PFGeoPoint * point=[PFGeoPoint geoPointWithLocation:curLocation];
    Place * pl=[[Place alloc]init];
    pl.userId=[PFUser currentUser];
    pl.location=point;
    
    NSMutableDictionary * values=[[NSMutableDictionary alloc]init];
    
    for (BSTextField * tf in fieldsArr) {
        [values setObject:tf.text forKey:tf.group];
    }
    
    pl.descriptionText=[values valueForKey:locDescr];
    pl.warrnings=[values valueForKey:locWarrinig];
    pl.title=[NSString stringWithFormat:@"%@",[values valueForKey:locTitle]];
    pl.height=[NSNumber numberWithFloat:[[values valueForKey:locHeight] floatValue]];
    
    [pl saveInBackgroundWithBlock:^(BOOL suc,NSError * err){  [loadView removeFromSuperview];
    
        if (!err) {
            [self back];
            
           
            
        }
    }];

}
@end
