//
//  main.m
//  BigSwings
//
//  Created by Oleksandr Borovok on 03.06.14.
//  Copyright (c) 2014 Oleksandr Padalko. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ELKAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ELKAppDelegate class]));
    }
}
