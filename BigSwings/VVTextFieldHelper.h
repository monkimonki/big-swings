//
//  TextFieldHelper.h
//  TutoringOnDemand
//
//  Created by Aleksandr Padalko on 3/20/14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol VVTextFieldHelperDelegate <NSObject>

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
-(void)textFieldDidBeginEditing:(UITextField *)textFiel;
-(void) textFieldDidEndEditing:(UITextField *)textField;
-(BOOL)textFieldShouldReturn:(UITextField *)textField;
-(BOOL)textFieldShouldClear:(UITextField *)textField;
@end
@interface VVTextFieldHelper : NSObject<UITextFieldDelegate>

@property (assign,nonatomic) id<VVTextFieldHelperDelegate> delegate;

@end

@protocol VVTextFieldProtocol <NSObject>

-(void) editingBegin:(UIView*)v;
-(void) editingEndWithView:(UIView*)v;
-(void) editingEnd;
-(void) editingEndWithText:(NSString*)str;

-(void)clear;
-(void)clearWithObj:(id)obj;


-(void)editingWithText:(NSString*)text;



-(void)textFieldShouldTabBack:(UITextField*)tf;
-(void)textFieldShouldTabForward:(UITextField*)tf;



@end