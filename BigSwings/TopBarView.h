//
//  TopBarView.h
//  BigSwings
//
//  Created by Aleksandr Padalko on 6/19/14.
//  Copyright (c) 2014 Oleksandr Padalko. All rights reserved.
//

#import <UIKit/UIKit.h>
enum kMapViewType {
    
    kMapViewTypeList=1,
    
   kMapViewTypeMap=2,
};

@class TopBarView;
@protocol TopBarViewDelegate<NSObject>

-(void)topBarView:(TopBarView*)topBarView didSwitchViewType:(enum kMapViewType)type;
-(void)topBarView:(TopBarView*)topBarView startEdidtingTextField:(UITextField*)tf;
-(void)topBarView:(TopBarView*)topBarView endEdidtingTextField:(UITextField*)tf;
-(void)topBarView:(TopBarView *)topBarView searchWithText:(NSString*)text;
@end
@interface TopBarView : UIView


@property (assign,nonatomic)id<TopBarViewDelegate>delegate;

@end
