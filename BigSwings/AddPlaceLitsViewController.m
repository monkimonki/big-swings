//
//  AddPlaceLitsViewController.m
//  testswings
//
//  Created by Aleksandr Padalko on 6/24/14.
//  Copyright (c) 2014 Oleksandr Padalko. All rights reserved.
//

#import "AddPlaceLitsViewController.h"
#import "BSTextField.h"
#import "LineView.h"
#import "BSButton.h"
#import "AlertTextView.h"

#import "JMMLocationEngine.h"
#import "SNImagePickerNC.h"
#import "LoadVIew.h"

#import "Media.h"
#import "SettingsViewController.h"
#import "Place.h"
#import "SelectPlaceViewController.h"
@interface AddPlaceLitsViewController ()<AlertTextViewDelegate,SNImagePickerDelegate,UITextViewDelegate ,SlectPlaceDelegate>{
    NSMutableArray * fieldsArr;
    BOOL keyBoardShown;
    CGSize keyBoardSize;
    
    NSMutableArray * photosArr;
    CLLocation * curLocation;
    
    LoadVIew * loadView;
    
    AlertTextView * alertTextView;
    BOOL textViewEditing;
}
@property (strong, nonatomic) SNImagePickerNC *imagePickerNavigationController;
@end

@implementation AddPlaceLitsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setTitle:[locAddPlaceTitle localized]];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    textViewEditing=NO;
    self.listTableView.pullToRefreshEnabled=NO;
    fieldsArr=[[NSMutableArray alloc]init];
    keyBoardShown=NO;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
//    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
//    // [backButton setTitle:@"back" forState:UIControlStateNormal];
//    [backButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//    [backButton setImage:[UIImage imageNamed:@"Back_ico.png"] forState:UIControlStateNormal];
//    [backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]
                                             initWithImage:[UIImage imageNamed:@"Back_ico.png"]
                                             style:UIBarButtonItemStylePlain
                                             target:self
                                             action:@selector(back)];
 
    self.navigationController.interactivePopGestureRecognizer.delegate = (id<UIGestureRecognizerDelegate>)self;
    
    UIButton *backButton1 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
    //   [backButton setTitle:@"" forState:UIControlStateNormal];
    [backButton1 setImage:[UIImage imageNamed:@"Settings_ico.png"] forState:UIControlStateNormal];
    [backButton1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [backButton1 addTarget:self action:@selector(openSettings) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton1];
    
    [self addViewToCell:[self createTextFieldWithName:locTitle] toSection:0];
        [self addViewToCell:[self createTextFieldWithName:locDescr] toSection:0];
        [self addViewToCell:[self createTextFieldWithName:locLocation] toSection:0];
        [self addViewToCell:[self createTextFieldWithName:locHeight] toSection:0];

    
    {
        
        UIView * v=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.height, 62)];
        alertTextView=[[AlertTextView alloc]initWithFrame:CGRectMake(22+57*0.5+12,0,self.view.frame.size.width -(22+57*0.5)-57*0.5, 62)];
        [alertTextView setDelegate:self];
        [alertTextView setAlertDelegate:self];
        [alertTextView setGroup:locWarrinig];
        [alertTextView setPlaceholder:@"Text box where you can write any warrnings."];
   
        [alertTextView setTextColor:[UIColor BSDarkBlueColor]];
        [v addSubview:alertTextView];
        [fieldsArr addObject:alertTextView];
        UIImageView * imgv=[[UIImageView alloc]initWithFrame:CGRectMake(22, alertTextView.frame.origin.y  , 57*0.5, 57)];
        // [imgv setBackgroundColor:[UIColor purpleColor]];
        [imgv setContentMode:UIViewContentModeCenter];
        [imgv setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png",locWarrinig]]];
        [v addSubview:imgv];
        LineView * lv=[[LineView alloc]initWithFrame:CGRectMake(0, alertTextView.frame.origin.y+alertTextView.frame.size.height, self.view.frame.size.width, 1)];
        
        [v addSubview:lv];
        
        [self addViewToCell:v toSection:0];
        
    }
    
    
    {
        
        
        UIView * v=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 52)];
        
        
        UIButton * butCurrentLocation=[UIButton buttonWithType:UIButtonTypeCustom];
              [butCurrentLocation setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        [butCurrentLocation setFrame:CGRectMake(0, 0, v.frame.size.width/2, v.frame.size.height)];
        [butCurrentLocation addTarget:self action:@selector(curentLocation) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:butCurrentLocation];
        
        UILabel * labCur=[[UILabel alloc]initWithFrame:CGRectMake(10, butCurrentLocation.frame.size.height-12-4, butCurrentLocation.frame.size.width-20, 12)];
        [labCur setFont:[UIFont BSFontSmallLight]];
        [labCur setTextColor:[UIColor BSDarkBlueColor]];
        [labCur setBackgroundColor:[UIColor clearColor]];
        [labCur setTextAlignment:NSTextAlignmentCenter];
        [labCur setText:@"use my current location"];
        
        [butCurrentLocation addSubview:labCur];
        UIImageView * locIco=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"flag_ico"]];
        [butCurrentLocation addSubview:locIco];
        [locIco setCenter:CGPointMake(butCurrentLocation.frame.size.width/2, butCurrentLocation.frame.size.height/2-5)];
        
        
        
        UIButton * butAddPhoto=[UIButton buttonWithType:UIButtonTypeCustom];
     //  [butAddPhoto setTitle:@"addPhoto" forState:UIControlStateNormal];
        [butAddPhoto setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [butAddPhoto addTarget:self action:@selector(addPhoto) forControlEvents:UIControlEventTouchUpInside];
        [butAddPhoto setFrame:CGRectMake( v.frame.size.width/2  , 0,v.frame.size.width/2,v.frame.size.height)];
        
        
        
        
        UILabel * labAddPhoto=[[UILabel alloc]initWithFrame:CGRectMake(10, butCurrentLocation.frame.size.height-12-4, butCurrentLocation.frame.size.width-20, 12)];
        [labAddPhoto setFont:[UIFont BSFontSmallLight]];
        [labAddPhoto setTextColor:[UIColor BSDarkBlueColor]];
        [labAddPhoto setBackgroundColor:[UIColor clearColor]];
        [labAddPhoto setTextAlignment:NSTextAlignmentCenter];
        [labAddPhoto setText:[locAddPhoto localized]];
        
        [butAddPhoto addSubview:labAddPhoto];
        UIImageView * photoIco=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"add_photo"]];
        [butAddPhoto addSubview:photoIco];
        [photoIco setCenter:CGPointMake(butAddPhoto.frame.size.width/2, butAddPhoto.frame.size.height/2-5)];
        
        
        
        [v addSubview:butAddPhoto];
        [v addSubview:butCurrentLocation];
        

        LineView * lv2=[[LineView alloc]initWithFrame:CGRectMake(0, v.frame.size.height-0.5, v.frame.size.width, 0.5)];
        
        [v addSubview:lv2];
        
        UIView * clv=[[UIView alloc]initWithFrame:CGRectMake(v.frame.size.width/2, 0, 0.5, v.frame.size.height)];
        
        [clv setBackgroundColor:[UIColor blackColor]];
        [v addSubview:clv];
        
        [self addViewToCell:v toSection:0];
     
        
    }
    {
        
       CGRect r= [UIScreen mainScreen].bounds;
        UIView*v;
        if (r.size.height==480) {
                   v=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 80)];
        }else
        v=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 134)];
        
        BSButton * b=[UIButton buttonWithType:UIButtonTypeCustom];
 
        
        [b setFrame:CGRectMake(0,0, 165, 35)];
        [b setCenter:CGPointMake(v.frame.size.width/2,v.frame.size.height/2)];
        [b setTitle:[[locSubmit localized] uppercaseString] forState: UIControlStateNormal];
        [b setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [b setBackgroundColor:[UIColor BSDarkBlueColor]];
        [b.titleLabel setFont:[UIFont BSFontLargeLight]];
        b.layer.cornerRadius=4;
        [b addTarget:self action:@selector(addPlace) forControlEvents:UIControlEventTouchUpInside];
        [v addSubview:b];
               [v setBackgroundColor:[UIColor clearColor]];

        
        
        [self addViewToCell:v toSection:0];
        
        
    
    }

    
	[self.listTableView addStaticCellsWithData:[NSArray arrayWithObjects:locTitle,locDescr,locLocation,locHeight,locWarrinig,@"buttons",@"submit", nil]];
    
    
    // Do any additional setup after loading the view.
}
-(void)openSettings{
    SettingsViewController * s=[[SettingsViewController alloc]init];
    
    
    [UIView  beginAnimations:nil context:NULL];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:0.75];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:self.navigationController.view cache:NO];
    [UIView commitAnimations];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelay:0.375];
    [self.navigationController pushViewController:s animated:NO];
    [UIView commitAnimations];
}
-(void)back{
    [UIView  beginAnimations:nil context:NULL];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:0.75];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:self.navigationController.view cache:NO];
    [UIView commitAnimations];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelay:0.375];
    [self.navigationController popViewControllerAnimated:YES];
    [UIView commitAnimations];
}
-(BSTextField*)createTextFieldWithName:(NSString*)name{
 BSTextField*   tf=[[BSTextField alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 57)];
    
    [tf setFont:[UIFont BSFontMediumLightOblique]];
    [tf addImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png",name]]];
    [tf setPlaceholder:[name localized]];
    [tf setBackgroundColor:[UIColor whiteColor]];
    [tf setTextColor:[UIColor BSDarkBlueColor]];
    tf.botBorder=YES;
    tf.group=name;
    [tf setClipsToBounds:NO];
    [tf setVvTextFieldDelegate:self];
    [fieldsArr addObject:tf];
    if ([name isEqualToString:locHeight]) {
        [tf setKeyboardType:UIKeyboardTypeDecimalPad];
    }
    
    
    return tf;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(CGFloat)listTableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UIView * v=[self getViewOfCellInIndexPath:indexPath];
    return v.frame.size.height;
    
}
-(UITableViewCell *)listTableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath objectForRow:(id)object{
    NSString *   CellIdentifer = [NSString stringWithFormat:@"c2%@",object];
      UITableViewCell  * cell=[tableView dequeueReusableCellWithIdentifier:CellIdentifer];
       if (!cell) {
  cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifer];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    UIView * v=[self getViewOfCellInSection:indexPath.section atIndex:indexPath.row];
    [cell addSubview:[self getViewOfCellInSection:indexPath.section atIndex:indexPath.row]];
    [cell setClipsToBounds:NO];
    [cell setBackgroundColor:[UIColor clearColor]];
    
    
     }
    return  cell;
    
}

#pragma mark - text field delegates
-(void)textFieldShouldTabBack:(UITextField *)tf{
    
    int idx=[fieldsArr indexOfObject:tf];
    
    if (idx>0) {
        [[fieldsArr objectAtIndex:idx-1] becomeFirstResponder];
    }
    
}
-(void)textViewDidBeginEditing:(UITextView *)textView{
    textViewEditing=YES;
      [self animationMove];
}
-(void)textViewDidEndEditing:(UITextView *)textView{
    textViewEditing=NO;
}
-(void)editingBegin:(UIView *)v{
    
    BSTextField * tf=(BSTextField*)v;
    
    if ([tf.group isEqualToString:locLocation]) {
        [tf resignFirstResponder];
        
        
        SelectPlaceViewController * s=[[SelectPlaceViewController alloc] init];
        [s setDelegate:self];
        CATransition *transition = [CATransition animation];
        transition.duration = 0.45;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault];
        transition.type = kCATransitionFromTop;
        [transition setType:kCATransitionPush];
        transition.subtype = kCATransitionFromTop;
        transition.delegate = self;
        [self.navigationController.view.layer addAnimation:transition forKey:nil];
        
        self.navigationController.navigationBarHidden = NO;
        [self.navigationController pushViewController:s animated:NO];
        
        return;
    }
    
    
    [super editingBegin:v];
    if (keyBoardShown) {
        [self animationMove];
    }
    
    
    
}
-(void)textFieldShouldTabForward:(UITextField *)tf{
    int idx=[fieldsArr indexOfObject:tf];
    
    if (idx<fieldsArr.count-1) {
        [[fieldsArr objectAtIndex:idx+1] becomeFirstResponder];
    }
}

-(void)keyboardWillChangeFrame:(NSNotification *)notification{
    keyBoardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
}

-(void)keyboardWasShown:(NSNotification *)notification{
    keyBoardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    if (!keyBoardShown) {
        [self animationMove];
    }
    
    keyBoardShown=YES;
    
}
-(void)keyboardWasHide:(NSNotification *)notification{
    keyBoardShown=NO;
    [UIView animateWithDuration:0.33 animations:^{
        [self.view setFrame:CGRectMake(0,64, self.view.frame.size.width, self.view.frame.size.height)];
    }];
}
-(void)animationMove{
    UIView * cr;
    
    if (textViewEditing) {
        cr=alertTextView;
    }else
    cr=[self curResponder];
    CGPoint p=  [self.navigationController.view.superview convertPoint:cr.superview.frame.origin fromView:cr];
    
    NSLog(@"%f",p.y);
    
    //flo  deltaMove=keyBoardSize.height-p.y;
    
    
    
    
    float keyY=[UIScreen mainScreen].bounds.size.height- keyBoardSize.height;
    if (p.y+cr.frame.size.height>keyY) {
        [UIView animateWithDuration:0.33 animations:^{
            [self.view setCenter:CGPointMake(self.view.center.x,self.view.center.y- ((cr.frame.size.height+p.y)-keyY)-40)];
        }];
    }else {
        [UIView animateWithDuration:0.33 animations:^{
            [self.view setFrame:CGRectMake(0,64, self.view.frame.size.width, self.view.frame.size.height)];
        }];
    }
    
    
    
}

#pragma mark - actions
-(void)curentLocation{
//    [JMMLocationEngine getBallParkLocationOnSuccess:^(CLLocation *loc) {
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [self getPlacemarksPressed:Nil];
//            curLocation=loc;
//        });
//    } onFailure:^(NSInteger failCode) {
//    }];
    
    
    [[DataSingleton sharedInstance]updateCurLocation:^(CLLocation *loc) {
        
        if (loc) {
                 [self getPlacemarksPressed:Nil];
            curLocation=loc;
            
        }else{
            
           UIAlertView* av= [[UIAlertView alloc]initWithTitle:@"" message:@"Please enable all geolocation services in your settings" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [av show];
        }
        
    }];
    
}

- (IBAction)getPlacemarksPressed:(id)sender {
    [JMMLocationEngine getPlacemarkLocationOnSuccess:^(CLPlacemark *place) {
        dispatch_async(dispatch_get_main_queue(), ^{
            for (BSTextField * tf in fieldsArr) {
                
                if ([tf.group isEqualToString:locLocation]) {
                    
                    NSLog(@"%@",[place addressDictionary]);
                    
                    
                    NSArray * arr=[[place addressDictionary] objectForKey:@"FormattedAddressLines"];
                    NSString * totaStr=[arr firstObject];
                    for(int a=1 ;a<arr.count;a++){
                        totaStr=[NSString stringWithFormat:@"%@, %@",totaStr,[arr objectAtIndex:a]];
                    }
                    
                    [tf setText:totaStr];
                    
                    break;
                }
                
                
            }
            
        });
    } onFailure:^(NSInteger failCode) {
        
    }];
}

-(void)addPhoto{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"SNPicker" bundle:nil];
    self.imagePickerNavigationController = [storyboard instantiateViewControllerWithIdentifier:@"ImagePickerNC"];
    [self.imagePickerNavigationController setModalPresentationStyle:UIModalPresentationFullScreen];
    self.imagePickerNavigationController.imagePickerDelegate = self;
    self.imagePickerNavigationController.pickerType = kPickerTypePhoto;
    [self presentViewController:self.imagePickerNavigationController animated:YES completion:^{ }];
    
}
- (void)imagePicker:(SNImagePickerNC *)imagePicker didFinishPickingWithMediaInfo:(NSMutableArray *)info
{
    
    photosArr=[[NSMutableArray alloc]init];
    for (int i = 0; i < info.count; i++) {
        ALAssetsLibrary *assetLibrary=[[ALAssetsLibrary alloc] init];
        [assetLibrary assetForURL:info[i] resultBlock:^(ALAsset *asset) {
            UIImage *image = [UIImage imageWithCGImage:[asset aspectRatioThumbnail]];
            
            [photosArr addObject:image];
            
        } failureBlock:^(NSError *error) {     }];
    }
    
    
}

- (void)imagePickerDidCancel:(SNImagePickerNC *)imagePicker
{
    
}
-(void)addPlace{
    
    
    for (BSTextField * tf in fieldsArr) {
        if ([tf.group isEqualToString:locTitle]&&tf.text.length==0) {
            UIAlertView * av=[[UIAlertView alloc]initWithTitle:@"" message:@"Enter a Tititle" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [av show];
            
            return;
        }
        
        if ([tf.group isEqualToString:locLocation]&&tf.text.length==0) {
            UIAlertView * av=[[UIAlertView alloc]initWithTitle:@"" message:@"Enter a Location" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [av show];
            return;
        }
        
    }
    
    loadView=  [LoadVIew  create];
    if (!curLocation) {
        NSString * findStr;
        for (BSTextField * tf in fieldsArr) {
            if ([tf.group isEqualToString:locLocation]) {
                findStr=tf.text;
            }
            
        }
        
        [JMMLocationEngine getGooglePlacesWithString:findStr onSuccess:^(NSArray * arr){
            NSLog(@"%@",arr);
            [self saveLocation];
        } onFailure:^(NSError * err){
            
            
            
        }];
    }else{
        [self saveLocation];
    }
    
    
    
    
    
    
    
}
-(void)placeSelectedWithAddress:(NSString *)adr andLocation:(CLLocation *)loc{
    for (BSTextField * tf in fieldsArr) {
        
        if ([tf.group isEqualToString:locLocation]) {
            
            [tf setText:adr];
            curLocation=loc;
        }
        
    }
}
static int counter=0;
-(void)saveLocation{
    
    counter++;
    
    
    //    float a1=((arc4random()%100 ));
    //
    //    float a2=(arc4random()%1000);
    //
    //    float b1=a1/a2;
    
    double a=  curLocation.coordinate.latitude+ (arc4random()%500 )/(arc4random()%1000);
    double b=curLocation.coordinate.longitude + (arc4random()%500 )/(arc4random()%1000);
    
    PFGeoPoint * point=[PFGeoPoint geoPointWithLocation:curLocation];
    Place * pl=[[Place alloc]init];
    pl.userId=[PFUser currentUser];
    pl.location=point;
    
    NSMutableDictionary * values=[[NSMutableDictionary alloc]init];
    
    for (BSTextField * tf in fieldsArr) {
        [values setObject:tf.text forKey:tf.group];
    }
    
    pl.descriptionText=[values valueForKey:locDescr];
    pl.warrnings=[values valueForKey:locWarrinig];
    pl.title=[NSString stringWithFormat:@"%@",[values valueForKey:locTitle]];
    pl.height=[NSNumber numberWithFloat:[[values valueForKey:locHeight] floatValue]];
    pl.textAddres=[values valueForKey:locLocation];
    [pl saveInBackgroundWithBlock:^(BOOL suc,NSError * err){  [loadView removeFromSuperview];
        
        if (!err) {
            
            for (UIImage * img in photosArr) {
                Media * m=[[Media alloc]init];
                m.userId=[PFUser currentUser];
                m.placeId=pl;
                m.image=[PFFile fileWithData:UIImageJPEGRepresentation(img, 0.5)];
                [m saveInBackground];
            }
            [[DataSingleton sharedInstance]loadMapObjects:^(BOOL suc) {
                    [self back];
            }];
        
            
            
            
        }
    }];
    
}



@end
