//
//  AlertTextView.m
//  testswings
//
//  Created by Aleksandr Padalko on 6/20/14.
//  Copyright (c) 2014 Oleksandr Padalko. All rights reserved.
//

#import "AlertTextView.h"

@implementation AlertTextView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        
        UIBarButtonItem *doneItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButtonDidPressed:)];

        UIButton * but=[UIButton buttonWithType:UIButtonTypeCustom];
        [but setFrame:CGRectMake(0, 0, 44, 44)];
        [but setImage:[UIImage imageNamed:@"ico-back"] forState:UIControlStateNormal];
        [but addTarget:self action:@selector(tabBack) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *perv=[[UIBarButtonItem alloc]initWithCustomView:but];
        UIButton * but2=[UIButton buttonWithType:UIButtonTypeCustom];
        [but2 setFrame:CGRectMake(0, 0, 44, 44)];
        [but2 setImage:[UIImage imageNamed:@"ico-forward"] forState:UIControlStateNormal];
        [but2 addTarget:self action:@selector(tabForward) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *next= [[UIBarButtonItem alloc] initWithCustomView:but2];
        
        UIBarButtonItem *flexableItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:NULL];
        UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 44)];
        [toolbar setItems:[NSArray arrayWithObjects:perv,next,flexableItem,doneItem, nil]];
        self.inputAccessoryView = toolbar;
    }
    return self;
}
- (void)doneButtonDidPressed:(id)sender {
    [self resignFirstResponder];
}
-(void)tabBack{
    [self.alertDelegate  textFieldShouldTabBack:self];
}
-(void)tabForward{
    [self.alertDelegate textFieldShouldTabForward:self];
}
 
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.


@end
