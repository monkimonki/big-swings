//
//  VVCollectionView.h
//  Chooos
//
//  Created by mm7 on 10.05.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VVListTableView.h"


#import "VVColumnView.h"
@interface VVCollectionViewButton:UIButton{
    
}
@property (retain,nonatomic)id Object;
@property (nonatomic)NSUInteger colum;
@property (retain,nonatomic)NSIndexPath*indexPath;
@end



@class VVCollectionView;
@protocol VVCollectionViewDelegate<NSObject>
-(VVColumnView*)collectionView:(VVCollectionView*)collectionView viewForColumn:(NSUInteger)column atIndexPath:(NSIndexPath *)indexPath andObject:(id)object;

-(NSUInteger )numberOfColumnsInSection:(NSUInteger)section;

//

-(CGFloat)collectionView:(VVCollectionView *)collectionView heightForLoadCellAtIndexPath:(NSIndexPath *)indexPath;
-(CGFloat)collectionView:(VVCollectionView *)collectionView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
-(CGFloat)collectionView:(VVCollectionView *)collectionView heightForHeaderInSection:(NSInteger)section;
-(UIView *)collectionView:(VVCollectionView *)collectionView viewForHeaderInSection:(NSInteger)section;
-(UITableViewCell *)collectionView:(VVCollectionView *)collectionView loadingCellViewForRowAtIndexPath:(NSIndexPath *)indexPath;
-(void)collectionView:(VVCollectionView*)collectionView didSelectItemInColum:(NSUInteger)colum atIndexPath:(NSIndexPath*)indexPath withObject:(id)object;
-(void)collectionViewDidLoadData:(id)data forHeader:(NSUInteger)section;



@end
@interface VVCollectionView : VVListTableView
@property (retain,nonatomic)NSMutableArray * columnsData;
;
@property (assign,nonatomic)id<VVCollectionViewDelegate> vvCollectionViewDelegate;
-(VVListTableViewSection*)getSection:(NSInteger)section;


-(id)getColumnView:(NSString*)name;
@end
