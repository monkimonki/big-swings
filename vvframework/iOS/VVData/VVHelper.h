//
//  VVHelper.h
//  ChoooseData
//
//  Created by Aleksandr Padalko on 5/1/14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import <Foundation/Foundation.h>
#define CASE(str)                       if ([__s__ isEqualToString:(str)])
#define SWITCH(s)                       for (NSString *__s__ = (s); ; )
#define DEFAULT


#import "VVObject.h"
@class VVObject;
@interface VVObject (Dictionary)
+(VVObject*)objectFromDictionary:(NSDictionary*)dict;
+(VVObject*)objectFromPFObject:(PFObject*)obj;
@end



@interface VVHelper : NSObject

+(NSDictionary*)convertDictionaryToVV:(NSDictionary*)dict;
+(NSDictionary*)convertVVDictToDictionary:(NSDictionary*)dict;
+(NSDictionary*)convertDictionaryToPF:(NSDictionary*)dict;

+(NSArray*)convertArrayToVV:(NSArray*)array;
+(NSArray*)convertVVArrayToDictionary:(NSArray*)array;
+(NSArray*)convertArrayToPF:(NSArray*)array;


//
+ (NSData *)formEncodedDataDictionary:(NSDictionary*)dict;
+ (NSString *)URLEncodedString:(NSString *)string;
//

+(NSString*)getObjectEntity:(NSObject*)obj;
+(NSString*)getEntityFromName:(NSString*)str;
+(NSString *)getClassNameFromEntity:(NSString*)str;

+(NSString*)propNameToKey:(NSString*)propName;
+(NSString*)keyToPropName:(NSString*)key;


//local BD HELPER

+(NSMutableArray*)findCollection:(NSString*)colName andSearchKey:(NSString*)searchKey andSearchId:(NSString*)objId andSearchLim:(int)lim;
+(void)saveDataToDerectory:(NSString*)derec andFileName:(NSString*)fn andFile:(NSData*)data;
+(BOOL)isExsistFileInDerectory:(NSString*)derec andFileName:(NSString*)fn;
+(NSData*)loadDataFromDerectory:(NSString*)derec andFileName:(NSString*)fn;
+(void)deleteAllFilesInDerectory:(NSString*)derec;

+(NSMutableDictionary*)dictFromPFObject:(PFObject*)obj;
@end
