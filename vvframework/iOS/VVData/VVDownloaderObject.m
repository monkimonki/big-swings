//
//  VVDownloaderObject.m
//  FGApp
//
//  Created by macmini9 on 4/30/14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "VVDownloaderObject.h"

@implementation VVDownloaderObject
+(VVDownloaderObject *)createWithInnerObject:(id)object{
	return [[self alloc] initWithInnerObject:object];
}

-(id)initWithInnerObject:(id)object {
	if (self = [super init]) {

		_innerObject = object;
	}
	return self;
}
@end
