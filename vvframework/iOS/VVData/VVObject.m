//
//  CObject.m
//  ChoooseData
//
//  Created by mm7 on 30.04.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "VVObject.h"
#import "VVHelper.h"
#import "VVStorage.h"

@implementation VVObject
-(id)init{
    
    if (self=[super init]) {
        if ([VVServer inst].dataType==kVVServerDataTypeLocal) {
            _objectId=[NSString stringWithFormat:@"%d%d%d%d",arc4random()%500+1,arc4random()%4+1,arc4random()%100,arc4random()%1998];
            
        }
    
        _entityName=[self entityName];
    }
    return self;
}


-(VVEntity*)getEmptyObjectEntity{
    if (self.objectId) {
         return  [[[VVEntity create] setEName:self.entityName] addObject:self.objectId forKey:@"objectId"];
    }else{
        return [[VVEntity create] setEName:self.entityName];
    }
    
}
+(VVEntity *)getCollectionEntity{
    return [[VVEntity create] setEName:[self getClassEntity]];
}
+(VVEntity *)getCollectionEntity:(VVObject*)obj{
    return [obj.getEmptyObjectEntity addObject:[self getCollectionEntity] forKey:[self getClassEntity]];
}



-(VVParametrsObject*)getLoadObjectParametrs{
    VVParametrsObject * parObj= [VVParametrsObject createWithEntity:self.getEmptyObjectEntity];
    
//    NSMutableDictionary * dict=[self logProp];
//    for (NSString * key in dict) {
//        if (![key isEqualToString:[VVHelper keyToPropName:@"objectId"]]) {
//            if ([[dict valueForKey:key] isKindOfClass:[NSDictionary class]]||[[dict valueForKey:key] isKindOfClass:[NSMutableDictionary class]]) {
//                
//                NSString * objId=[[dict valueForKey:key] valueForKey:[VVHelper keyToPropName:@"objectId"]];
//                if (objId) {
//                    [parObj.body setValue:objId forKey:key];
//
//                }
//          }else
//            [parObj.body setValue:[dict valueForKey:key] forKey:key];
//            
//        }
//    }
    //[parObj.body addEntriesFromDictionary:dict];
    
    
    
    return parObj;
}


-(id)valueForUndefinedKey:(NSString *)key{
    NSLog(@"UNDIFINED KEY : %@",key);
    
    return nil;
    
}
+(VVParametrsObject*)getLoadCollectinParametrs{
    
    return [VVParametrsObject createWithEntity:[self getCollectionEntity]];
}
+(VVParametrsObject *)getLoadCollectinParametrs:(VVObject*)obj{
    return [VVParametrsObject createWithEntity:[self getCollectionEntity:obj]];
}
+(VVParametrsObject*)getLoadCollectinParametrs:(int)origin andLenght:(int)lenght{
    
    VVParametrsObject*parObj=[self getLoadCollectinParametrs];
    
    parObj.pageOrigin=origin;
    parObj.pageSize=lenght;
    return parObj;
    
    
}
+(VVParametrsObject*)getLoadCollectinParametrs:(VVObject*)obj andOrigin:(int)origin andLenght:(int)lenght{
    VVParametrsObject * parObj=[self getLoadCollectinParametrs:obj];
    parObj.pageOrigin=origin;
    parObj.pageSize=lenght;
    //    parObj.methodType=kMethodTypeGet;
    return parObj;
}


-(id)initWithDictionary:(NSDictionary*)dict{
       VVObject * result= [[VVStorage inst] getObjectByID:[dict valueForKey:[VVHelper propNameToKey:@"objectId"]]];
    
    if (result) {
     
        
        self=result;
        
        [self setWithDictionary:dict];
        
        return self;
    }else{
       
    if (self=[super init]) {
        
        [self setWithDictionary:dict];
        if ([self objectId]) {
               [[VVStorage inst].objectsData addObject:self];
        }
     
     
        
    }
    return self;
    }
}
-(void)setWithDictionary:(NSDictionary*)dict{
    
 
    for (NSString * key in dict) {
      
        
        if ([[dict valueForKey:key]isKindOfClass:[NSDictionary class]]) {
            
            if ([[dict valueForKey:key] valueForKey:[VVHelper propNameToKey:@"entityName"]]) {
                VVObject * obj= [[NSClassFromString([VVHelper getClassNameFromEntity:[[dict valueForKey:key] valueForKey:[VVHelper propNameToKey:@"entityName"]]]) alloc] initWithDictionary:[dict valueForKey:key]];
                  [super setValue:obj forKey:[VVHelper keyToPropName:key]];
            }else{
            
            VVObject * obj= [[NSClassFromString([VVHelper getClassNameFromEntity:[VVHelper keyToPropName:key]]) alloc] initWithDictionary:[dict valueForKey:key]];
            
            if (obj) {
                    [super setValue:obj forKey:[VVHelper keyToPropName:key]];
            }else{
                [self setValue:[VVHelper convertDictionaryToVV:[dict valueForKey:key]] forKey:[VVHelper keyToPropName:key]];
            }
                
            }
        
        }else if ([[dict valueForKey:key]isKindOfClass:[NSArray class]]){
            NSMutableArray * realArr=[[NSMutableArray alloc]init];
            for (NSDictionary* dict2 in [dict valueForKey:key]) {
                if ([dict2 isKindOfClass:[NSDictionary class]]) {
                    
             
                if ([dict2 valueForKey:[VVHelper propNameToKey:@"entityName"]]) {
                    VVObject * obj= [[NSClassFromString([VVHelper getClassNameFromEntity:[dict2 valueForKey:[VVHelper propNameToKey:@"entityName"]]]) alloc] initWithDictionary:dict2];
                    [realArr addObject:obj];
                }else{
                   VVObject * obj= [[NSClassFromString([VVHelper getClassNameFromEntity:[VVHelper keyToPropName:key]]) alloc] initWithDictionary:dict2];
                    if (obj) {
                           [realArr addObject:obj];
                    }else
                    [realArr addObject:dict2];
                }
                    
                }else{
                    [realArr addObject:dict2];
                }
                
            }
            
         [super setValue:[VVHelper convertArrayToVV:realArr] forKey:[VVHelper keyToPropName:key]];
            
            
        }else{
            
            if ([[dict valueForKey:key] isKindOfClass:[NSString class]]) {
                
            
            if ([[dict valueForKey:key] isEqualToString:@"true22"]) {
                       [super setValue:@YES forKey:[VVHelper keyToPropName:key]];
            }else if ([[dict valueForKey:key] isEqualToString:@"false22"]) {
                   [super setValue:@NO forKey:[VVHelper keyToPropName:key]];
            }
            else
            [super setValue:[dict valueForKey:key] forKey:[VVHelper keyToPropName:key]];
                
            }else{
                if ([[dict valueForKey:key] isKindOfClass:[NSNumber class]]) {
                    NSNumber * n=[dict valueForKey:key];
                    if ([n boolValue]) {
                        NSLog(@"this is bolean");
                    }
                    float a=[n floatValue];
                    [super setValue:n forKey:[VVHelper keyToPropName:key]];
                }
                else
                    [super setValue:[dict valueForKey:key] forKey:[VVHelper keyToPropName:key]];
                
            }
            
        }
        
    }
    
 
}
-(void)setValue:(id)value forUndefinedKey:(NSString *)key{
    
 
}
///
+(NSString *)getClassEntity{
    
    NSString * str=NSStringFromClass(self);
    
    return [VVHelper getEntityFromName:str];
    
}
-(NSString *)entityName{
    
    return [VVHelper getObjectEntity:self];
}
//

-(VVParametrsObject*)deleteVVObject{
    VVEntity * en = [[VVEntity create] setEName:self.entityName];
    VVParametrsObject * parObj= [VVParametrsObject createWithEntity:en];
    parObj.methodType=KMethodTypePost;
    parObj.objectToDelete=self;
    return parObj;
}
-(VVParametrsObject *)saveVVObject{
    VVEntity * en = [[VVEntity create] setEName:self.entityName];
    VVParametrsObject * parObj= [VVParametrsObject createWithEntity:en];
    parObj.methodType=KMethodTypePost;
    parObj.objectToSave=self;
    return parObj;
}


-(NSMutableDictionary*)logProp{
  // NSLog(@"%@",[self dictionaryRepresentation]);

  //  return;
    NSMutableDictionary * dict=[[NSMutableDictionary alloc] init];
    NSMutableArray * arr=[self getProp];
    
    for (int a=0; a<arr.count; a++) {
        id val=[self valueForKey:[arr objectAtIndex:a]];
        
        NSString * realKey=[VVHelper propNameToKey:[arr objectAtIndex:a]];
     
        if ([val isKindOfClass:[VVObject class]]) {
            VVObject  *obj=val;
            [dict setValue:[obj logProp] forKey:realKey];

        }else if([val isKindOfClass:[VVFile class]]){
            
            VVFile * vvFile=val;
            
            
            [dict setValue:[vvFile formDictionary] forKey:realKey];
    
            
            
            
        }else if([val isKindOfClass:[NSDate  class]]){
            
            
            
        }else if([val isKindOfClass:[NSArray class]]){
            
            NSArray * valArr=val;
            
            if (valArr.count>0) {
                if ([[valArr firstObject]isKindOfClass:[VVObject class]]) {
                    NSMutableArray* arr=[[NSMutableArray alloc]init];
                    for (VVObject * obj in valArr) {
                                            [arr addObject:[obj logProp]];
                    }
                    
                    [dict setValue:arr forKey:realKey];
                 
                    
                }else{
                    
                        [dict setValue:[VVHelper convertVVArrayToDictionary:val] forKey:realKey];
                    
                }
            }else if([val isKindOfClass:[NSDictionary class]]){
                

                [dict setValue:[VVHelper convertVVDictToDictionary:val] forKey:realKey];

            
            }else{
                
                [dict setValue:[VVHelper convertVVArrayToDictionary:val] forKey:realKey];
            }
            
            
            
        }else{
            
           // if (![[arr objectAtIndex:a]isEqualToString:@"entityName"]) {
            [self addValForKey:[arr objectAtIndex:a] forKey:realKey toCDict:dict];
         
         //   [dict setValue:[self valueForKey:[arr objectAtIndex:a]] forKey:realKey];
                
             //      }
         //     VVLog(@"%@=%@",[arr objectAtIndex:a],[self valueForKey:[arr objectAtIndex:a]]);
        }
            
            
     
    }
    
    return dict;
}
-(void)addValForKey:(NSString*)key forKey:(NSString*)realKey toCDict:(NSMutableDictionary*)dict{
    [dict setValue:[self valueForKey:key] forKey:realKey];
}
- (NSMutableArray*)getProp {
    
    NSMutableArray * arr=[[NSMutableArray alloc]init];
    
    id base=self;
    
    
    while (![NSStringFromClass([base class])isEqualToString:@"NSObject"]) {
        
  
    
    unsigned int outCount, i;
    objc_property_t *properties = class_copyPropertyList([base class], &outCount);
    for(i = 0; i < outCount; i++) {
        objc_property_t property = properties[i];
        const char *propName = property_getName(property);

        
        if(propName) {
             NSString *propertyName = [NSString stringWithCString:propName
                                                        encoding:[NSString defaultCStringEncoding]];

           
            [arr addObject:propertyName];
           
        }
    }
    free(properties);
        base=[base superclass];
    }
    
    return arr;
}
static const char *getPropertyType(objc_property_t property) {
    const char *attributes = property_getAttributes(property);
    char buffer[1 + strlen(attributes)];
    strcpy(buffer, attributes);
    char *state = buffer, *attribute;
    while ((attribute = strsep(&state, ",")) != NULL) {
        if (attribute[0] == 'T') {
            return (const char *)[[NSData dataWithBytes:(attribute + 3) length:strlen(attribute) - 4] bytes];
        }
    }
    return "@";
}
@end
