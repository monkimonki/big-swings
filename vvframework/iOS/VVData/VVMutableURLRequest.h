//
//  VVMutableURLRequest.h
//  ChoooseData
//
//  Created by mm7 on 03.05.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import <Foundation/Foundation.h>
enum  kVVReturningType{
	
	kVVReturningTypeList,
	kVVReturningTypeObject
};


@interface VVMutableURLRequest : NSMutableURLRequest
@property (nonatomic)enum kVVReturningType returnigType;

@property (nonatomic,retain) NSString * endPointName;
-(NSString *)getDataType;
@end
