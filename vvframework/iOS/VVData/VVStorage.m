//
//  VVStorage.m
//  FGApp
//
//  Created by mm7 on 15.05.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "VVStorage.h"

@implementation VVStorage

static VVStorage * vvStorage;
static NSString * path=@"VVFramework/VVStorage";


-(id)init{
    
    
    if (self=[super init]) {
        [VVHelper deleteAllFilesInDerectory:path];
        _objectsData=[[NSMutableArray alloc]init];
        _parseObjectsData=[[NSMutableArray alloc]init];
    }
    return self;
}

+(VVStorage *)inst{
    if (vvStorage==NULL) {
        vvStorage=[[VVStorage alloc]init];
    }
    
    
    return vvStorage;
}

#pragma mark - objects

-(VVObject*)getObjectByID:(NSString*)objectId{
    if (!objectId) {
        return nil;
    }
    
    
    
    __block VVObject *searchingObject=nil;
    
    // enumerating through the first array (containing arrays)
    [_objectsData enumerateObjectsUsingBlock:^(VVObject *object, NSUInteger indx, BOOL *firstStop) {
        
        // enumerating through the nested array
      
            
            if ([object.objectId isEqualToString:objectId]) {
                searchingObject = object; // or do whatever you wanted to do.
                // you found your object now kill both the blocks
                *firstStop = YES;
            }
             }];
    
    
    return searchingObject;
    
    
}
-(id)getParseObjectById:(NSString*)objectId{
    if (!objectId) {
        return nil;
    }
    
    
    
    __block PFObject *searchingObject=nil;
    
    // enumerating through the first array (containing arrays)
    [_parseObjectsData enumerateObjectsUsingBlock:^(PFObject * object, NSUInteger indx, BOOL *firstStop) {
        
        // enumerating through the nested array
        
        
        if ([object.objectId isEqualToString:objectId]) {
            searchingObject = object; // or do whatever you wanted to do.
            // you found your object now kill both the blocks
            *firstStop = YES;
        }
    }];
    
    
    return searchingObject;
}
#pragma mark -()

-(void)saveFile:(NSData *)fileData andFileName:(NSString*)fn{
    
    [VVHelper saveDataToDerectory:path andFileName:fn andFile:fileData];
    
}

-(NSData *)loadFileWithFileName:(NSString *)fileName{
    
    if ([VVHelper isExsistFileInDerectory:path andFileName:fileName]) {
        return [VVHelper loadDataFromDerectory:path andFileName:fileName ];
    }
    
    return nil;
    
}

-(NSData *)loadVVFile:(VVFile *)file{
    if (file.url) {
     return    [self loadFileWithFileName:[self formStringFromURL:file.url]];
    }else{
#ifdef  PARSE
        
        return    [self loadFileWithFileName:file.file.name];

#endif
        
    }
    return nil;
    
}

-(void)saveVVFile:(VVFile *)file{
    if (file.url) {
       [self saveFile:file.data andFileName:[self formStringFromURL:file.url]];
    }else{
#ifdef  PARSE
        
        [self saveFile:file.data andFileName:file.file.name];
        
#endif
        
    }
}

-(NSString*)formStringFromURL:(NSURL*)url{
    
    NSString *myString = [url absoluteString];
    myString= [myString stringByReplacingOccurrencesOfString:@"/" withString:@""];
    
    return [myString stringByReplacingOccurrencesOfString:@":" withString:@""];
    
    
    
    
}
-(void)getAllItemsByClass:(Class)cl andValueParams:(NSDictionary*)dict mustHaveValForKey:(NSString*)reqKey andBlock:(void (^)(NSMutableArray *arr))block{
    
    NSMutableArray * itemsArr=[[NSMutableArray alloc]init];
    
    [_objectsData enumerateObjectsUsingBlock:^(VVObject *object, NSUInteger indx, BOOL *firstStop) {
       if ([object isKindOfClass:cl]) {
        
        if (dict.allKeys.count>0) {
            
   
        
        for (NSString * key in dict) {
            
            id  val=[dict valueForKey:key];
            
            
            if ([val isKindOfClass:[NSNumber class]]) {
                
                
                if ([val isEqualToNumber:[dict valueForKey:key]]) {
                    if ([object valueForKey:reqKey]) {
                        
                        [itemsArr addObject:object];
                    }
                }
                
            }
            
            
        }
        }else{
            
            if ([object valueForKey:reqKey]) {
                
                [itemsArr addObject:object];
            }

        }
        
         
        }
    
    }];
    
    
    block(itemsArr);
    
    
}
-(void)addParseObject:(PFObject*)po{
    
   id o= [self getParseObjectById:po.objectId];
    
    [_parseObjectsData removeObject:o];
    
    [_parseObjectsData addObject:po];
    
}
@end
