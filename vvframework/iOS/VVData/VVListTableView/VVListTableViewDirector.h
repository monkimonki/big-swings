//
//  VVListTableViewDirector.h
//  Chooos
//
//  Created by Aleksandr Padalko on 5/8/14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VVListTableViewSection.h"

@protocol VVListTableViewDelegate
-(VVParametrsObject *)paramsForLoad:(VVParametrsObject*)params forIndexPath:(NSIndexPath*)indexPath;

-(UITableViewCell *)listTableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath objectForRow:(id)object;

-(UITableViewCell *) listTableView:(UITableView *) tableView loadingCellViewForRowAtIndexPath:(NSIndexPath*)indexPath;

-(void)listTableViewDidLoadEmtyListForSection:(NSUInteger)section;

-(void)listTableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath withObject:(VVObject*)object;

-(void)listTableView:(UITableView *)tableView didSelectEditebleRowAtIndexPath:(NSIndexPath *)indexPath withObject:(VVObject*)object;

-(void)listTableView:(UITableView *)tableView configurateEditableCell:(UITableViewCell*)cell forIndexPath:(NSIndexPath*)indexPath;


-(CGFloat)listTableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
-(CGFloat)listTableView:(UITableView *)tableView heightForLoadCellAtIndexPath:(NSIndexPath *)indexPath;

-(CGFloat)listTableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section;
-(UIView *)listTableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section withObject:(id)object;

-(UIView *)listTableView:(UITableView*)tableView viewForFooterInSection:(NSInteger)section withObject:(id)object;
-(CGFloat)listTableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section;


-(void)listTableViewDidLoadData:(id)data forHeader:(NSUInteger)section;

-(void)listTableView:(UITableView*)listTableView refresh:(void (^)( BOOL success ) )cb;



@end


@interface VVListTableViewDirector : NSObject
{
    
    NSMutableArray * sections;
    
}
-(NSArray*)allIndexPathes;
@property (retain,nonatomic) UITableView * baseTableView;
-(UITableViewCell*)controlCell:(UITableView*)tableView andIndexPath:(NSIndexPath*)indexPath;
-(CGFloat)controlHeight:(UITableView*)tableView andIndexPath:(NSIndexPath*)indexPath;

-(UIView*)controlHeader:(UITableView *)tableView andSection:(NSInteger)section;
-(UIView*)controlFooter:(UITableView *)tableView andSection:(NSInteger)section;
//
@property (assign,nonatomic)id<VVListTableViewDelegate>delegate;
//

-(void)addEmptySection;
-(void)insertEmptySectionAtSection:(NSUInteger)section;



-(void)addDynamicHeader:(VVParametrsObject*)loadParametrs andClass:(Class)cl;
-(void)setDynamicHeader:(VVParametrsObject *)loadParametrs andClass:(Class)cl forSection:(NSUInteger)section;



-(void)addDynamicCells:(VVParametrsObject*)loadParametrs andClass:(Class)cl;
-(void)insertDynamicCells:(VVParametrsObject *)loadParametrs andClass:(Class)cl atSection:(NSUInteger)section;
-(void)addStaticCells;
-(void)addStaticCellsWithData:(NSArray*)arr;
-(void)insertData:(NSArray *)arr toStaticCellAtSection:(NSUInteger)section;
-(void)setData:(NSArray *)arr toStaticCellAtSection:(NSUInteger)section;


-(void)refresh:(void (^)(BOOL success))cb;
-(BOOL)isNeedToLoad:(NSIndexPath*)indexPath;

-(NSInteger)getRowsCountInSection:(NSInteger)section;
-(VVObject *)getObjectAtIndexPath:(NSIndexPath *)indexPath;
    -(VVObject*)getObject:(VVListTableViewSection*)section andRow:(NSInteger)row;
-(NSInteger)getNumberOfSections;


-(VVListTableViewSection*)getSection:(NSInteger)section;

@end
