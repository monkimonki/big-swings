//
//  VVListTableViewDirector.m
//  Chooos
//
//  Created by Aleksandr Padalko on 5/8/14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "VVListTableViewDirector.h"

@implementation VVListTableViewDirector
-(id)init{
    if (self=[super init ]) {
        sections=[[NSMutableArray alloc]init];
    }
    return self;
}
-(NSArray *)allIndexPathes{
    
    NSMutableArray * arr=[[NSMutableArray alloc]init];
    
    int i=0;
    for (VVListTableViewSection * sec in sections) {
        for(int a=0;a<sec.cells.objects.count;a++){
            
            [arr addObject:[NSIndexPath indexPathForRow:a inSection:i]];
        }
        
        i++;
    }
    
    
    return arr;
    
    
    
    
    
    
}
-(void)loadCellsInSection:(NSIndexPath*)indexPath {
    
	VVParametrsObject * paramsObj;
  __block  VVListTableViewSection *section=[self getSection:indexPath.section];
    
    
    
    paramsObj=section.cells.loadParametrs;
    
    paramsObj = [self.delegate paramsForLoad:paramsObj forIndexPath:indexPath];
    paramsObj.pageOrigin = section.pageOrigin;
	paramsObj.pageSize = section.pageSize;
    
    
    
	[[VVServer inst] loadObjects:paramsObj callback:^(BOOL success, NSArray *array) {
		if (success ) {

            
          
			[section.cells.objects addObjectsFromArray:array];
            if (array.count<section.pageSize) {
                section.fullLoad=YES;
            }
			[_baseTableView reloadData];
			section.pageOrigin += section.pageSize;
		}
	}];
}
-(void)refresh:(void (^)(BOOL success))cb{
    
//    VVParametrsObject * paramsObj;
//    __block  VVListTableViewSection *section=[self getSection:0];
//    
//
//    paramsObj=section.cells.loadParametrs;
//    
//    paramsObj = [self.delegate paramsForLoad:paramsObj forIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
//    paramsObj.pageOrigin = 0;
//	paramsObj.pageSize = section.pageSize;
//    
//    
//    
//	[[VVServer inst] loadObjects:paramsObj callback:^(BOOL success, NSArray *array) {
//		if (success ) {
//            
//            
//			section.cells.objects=[[NSMutableArray alloc] initWithArray:array];
//            if (array.count<section.pageSize) {
//                section.fullLoad=YES;
//            }
//			[_baseTableView reloadData];
//			section.pageOrigin += section.pageSize;
//		}
//	}];
//    
//    return;
    int a=0;
    int firstSect;
    VVListTableViewSection * firsDynamic;
    for (VVListTableViewSection  *sec in sections) {
        if (sec.cells.type==kVVDirectorObjectTypeDynamic) {
            [sec.cells.objects removeAllObjects ];
            [sec setPageOrigin:0];
            sec.fullLoad=NO;
            NSIndexPath * ip=[[NSIndexPath alloc]init];
            if (!firsDynamic) {
                  firsDynamic=sec;
                firstSect=a;
            }
        
            
        }
        
        a++;
    }
    
    
    [sections removeObjectsAtIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(firstSect+1, sections.count-firstSect-1)]];
    
    [self.delegate listTableView:self.delegate refresh:^(BOOL success) {
        
        if (success) {
            
      
    
    if (firsDynamic) {
        
        VVParametrsObject * paramsObj;
        __block  VVListTableViewSection *section=firsDynamic;
        
        
        
        paramsObj=section.cells.loadParametrs;
        
        paramsObj = [self.delegate paramsForLoad:paramsObj forIndexPath:[NSIndexPath indexPathForRow:0 inSection:firstSect]];
        paramsObj.pageOrigin = section.pageOrigin;
        paramsObj.pageSize = section.pageSize;
        
        
        
        [[VVServer inst] loadObjects:paramsObj callback:^(BOOL success, NSArray *array) {
            if (success ) {
                
                if (array.count==0 && section.cells.objects.count==0) {
                    [self.delegate listTableViewDidLoadEmtyListForSection:firstSect];
                }
                
                [section.cells.objects addObjectsFromArray:array];
                if (array.count<section.pageSize) {
                    section.fullLoad=YES;
                }
                [_baseTableView reloadData];
                section.pageOrigin += section.pageSize;
                  cb(YES);
            }else{
                  cb(NO);
            }
            
            
        }];

    }else{
        
        cb(YES);
    }
            
        }else{
            
            cb(NO);
        }
            
            
      }];
}

#pragma mark - control
-(UITableViewCell *)controlCell:(UITableView *)tableView andIndexPath:(NSIndexPath *)indexPath{
    VVListTableViewSection * section=[self getSection:indexPath.section];
    if ([self isNeedToLoad:indexPath]) {
        [self loadCellsInSection:indexPath];
        //[self performSelector:@selector(loadCellsInSection:) withObject:indexPath afterDelay:0.01];
        return [self.delegate listTableView:tableView loadingCellViewForRowAtIndexPath:indexPath];
    }
    
    
    //if (section.cells.type==kVVDirectorObjectTypeDynamic ) {
    
            
       
        return  [self.delegate listTableView:tableView cellForRowAtIndexPath:indexPath objectForRow:[self getObject:section andRow:indexPath.row]];
        
    
    //}else{
      //  return  [self.delegate listTableView:tableView cellForRowAtIndexPath:indexPath];
    //}
    
    
}
-(UIView*)controlHeader:(UITableView *)tableView andSection:(NSInteger)section{
   
    VVListTableViewSection * sec=[self getSection:section];
    
    if (sec.header ) {
        
       return  [ self.delegate listTableView:tableView viewForHeaderInSection:section withObject:[self getObjectForHeader:section]];
        
    }else{
   return   [self.delegate listTableView:tableView viewForHeaderInSection:section withObject:nil];
    }
    
    
}
-(UIView*)controlFooter:(UITableView *)tableView andSection:(NSInteger)section{
    VVListTableViewSection * sec=[self getSection:section];
    
    if (sec.header ) {
        
        return  [ self.delegate listTableView:tableView viewForFooterInSection:section withObject:[self getObjectForHeader:section]];
        
    }else{
        return   [self.delegate listTableView:tableView viewForFooterInSection:section withObject:nil];
    }
    
}
-(CGFloat)controlHeight:(UITableView *)tableView andIndexPath:(NSIndexPath *)indexPath{
    if ([self isNeedToLoad:indexPath ]) {
	
		return [self.delegate listTableView:tableView heightForLoadCellAtIndexPath:indexPath];
	}else{
        return [self.delegate listTableView:tableView heightForRowAtIndexPath:indexPath];
    }
}
#pragma mark - ()


-(void)addEmptySection{
    VVListTableViewSection * section=[[VVListTableViewSection alloc]init];
    [sections addObject:section];
}
-(void)insertEmptySectionAtSection:(NSUInteger)section{
    

}

-(void)addDynamicHeader:(VVParametrsObject *)loadParametrs andClass:(Class)cl {
    VVListTableViewSection * section=[[VVListTableViewSection alloc]init];
    [section createHeader:loadParametrs andClassName:[VVHelper getClassNameFromEntity:NSStringFromClass(cl)]];
    [sections addObject:section];
    
}
-(void)setDynamicHeader:(VVParametrsObject *)loadParametrs andClass:(Class)cl forSection:(NSUInteger)section{
    if (section<sections.count) {
        VVListTableViewSection * sec=[self getSection:section];
  
        if (!sec.header.object) {
            
        
    
                  [sec createHeader:loadParametrs andClassName:[VVHelper getClassNameFromEntity:NSStringFromClass(cl)]];
        
        }
        
        
        
    }else{
        
        [self addDynamicHeader:loadParametrs andClass:cl];
    }
    
   
}

-(void)addDynamicCells:(VVParametrsObject*)loadParametrs andClass:(__unsafe_unretained Class)cl{
    VVListTableViewSection * section=[[VVListTableViewSection alloc]init];
    [section createCell:loadParametrs andClassName:[VVHelper getClassNameFromEntity:NSStringFromClass(cl)]];
    [sections addObject:section];
    
}

-(void)insertDynamicCells:(VVParametrsObject *)loadParametrs andClass:(Class)cl atSection:(NSUInteger)section{
    if (section<sections.count) {
        
        VVListTableViewSection * sec= [self getSection:section];
        sec.fullLoad=NO;
     
            [sec createCell:loadParametrs andClassName:[VVHelper getClassNameFromEntity:NSStringFromClass(cl)]];
      
        
        
    }else{
        
        
        [self addDynamicCells:loadParametrs andClass:cl];
    }

}

-(void)addStaticCells{
    VVListTableViewSection * section=[[VVListTableViewSection alloc]init];
    [section createCell:nil andClassName:nil];
    [sections addObject:section];
}
-(void)addStaticCellsWithData:(NSArray*)arr{
    VVListTableViewSection * section=[[VVListTableViewSection alloc]init];
    [section createCell:nil andClassName:nil];
    [sections addObject:section];
    
    [section.cells.objects addObjectsFromArray:arr];
}
-(void)insertData:(NSArray *)arr toStaticCellAtSection:(NSUInteger)section{
      if (section<sections.count) {
          
   VVListTableViewSection * sec= [self getSection:section];
          [sec createCell:nil andClassName:nil];
          sec.cells.objects=[[NSMutableArray alloc]initWithArray:arr];
          sec.cells.type=kVVDirectorObjectTypeStatic;
        
     
    }else{
        
        [self addStaticCellsWithData:arr];
    }
    
}

-(void)setData:(NSArray *)arr toStaticCellAtSection:(NSUInteger)section{
	if (section<sections.count) {
		
		VVListTableViewSection * sec= [self getSection:section];
		//if (sec.cells.type==kVVDirectorObjectTypeStatic) {
        if (!sec.cells) {
            [sec createCell:nil andClassName:nil];
        }
			sec.cells.objects = [[NSMutableArray alloc]initWithArray:arr];
		//}
	}else{
        [self addStaticCellsWithData:arr];
    }
}

#pragma mark - additionals
-(BOOL)isNeedToLoad:(NSIndexPath *)indexPath{
    return [[self getSection:indexPath.section] isCountGraterThanCellsCount:indexPath.row];

}
#pragma mark - getters
-(VVListTableViewSection*)getSection:(NSInteger)section{
    
    if (sections.count<= section) {
            return nil;
    }else
    return [sections objectAtIndex:section];
}
-(NSInteger)getRowsCountInSection:(NSInteger)section{
    
    VVListTableViewSection * sec=[sections objectAtIndex:section];
    return sec.rowsCount;
    
}
-(NSInteger)getNumberOfSections{
    return sections.count;
}


-(VVObject *)getObjectAtIndexPath:(NSIndexPath *)indexPath{
    
    
    VVListTableViewSection * section=[self getSection:indexPath.section];
    NSString* className= section.cells.className;
    return  [self getObjectWithDict:[section getObjectAtRow:indexPath.row] andClassName:className];
}
-(VVObject*)getObject:(VVListTableViewSection*)section andRow:(NSInteger)row{
    
    NSString* className= section.cells.className;
    return  [self getObjectWithDict:[section getObjectAtRow:row] andClassName:className];
    
}
-(id)getObjectForHeader:(NSInteger)sect{
    
   __block VVListTableViewSection * section=[self getSection:sect];
    __block int bSect=sect;
    NSString* className= section.header.className;
    if(section.header.object){
    return  [self getObjectWithDict:section.header.object andClassName:className];
        
    }else{
        
    
        [[VVServer inst]loadObject:section.header.loadParametrs callback:^(BOOL suc,id obj) {
            if (suc ) {
             
                section.header.object=obj;
          //      if ([self.delegate respondsToSelector:@selector(didLoadData:forHeader:)]) {
                
          
                    [self.delegate listTableViewDidLoadData:[self getObjectWithDict:obj andClassName:section.header.className] forHeader:bSect ];
              //  [_baseTableView reloadData];
               // }
                
               
              //  [_baseTableView beginUpdates];
              //  [_baseTableView reloadSections:[NSIndexSet indexSetWithIndex:bSect-1] withRowAnimation:UITableViewRowAnimationMiddle];
              //  [_baseTableView endUpdates];
             //   [_baseTableView reloadData];
               
            }
        }];
        
        return nil;
    }
    
}
-(id)getObjectForFooter:(NSInteger)sect{
    
    return nil;

}
-(id)getObjectWithDict:(NSDictionary*)dict andClassName:(NSString*)className{
    if ([dict isKindOfClass:[VVObject class]]) {
        return dict;
    }
    
    if (!dict) {
        return nil;
    }
    if (className) {
//#ifdef PARSE
//		if ([VVServer inst].dataType == kVVServerDataTypeParse) {
//			return [[NSClassFromString(className) alloc]initWithDictionary:dict];
//		}
//#endif
        if ([dict isKindOfClass:[NSDictionary class]]) {
            return [[NSClassFromString(className) alloc]initWithDictionary:dict];
        }else
        return dict;
    }else{
        return dict;
    }
}
@end
