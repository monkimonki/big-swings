//
//  RequestTableView.h
//  FGApp
//
//  Created by macmini9 on 4/30/14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VVBaseTableView.h"
#import "VVParametrsObject.h"
#import "VVListTableViewDirector.h"
@class VVObject;

@interface VVListTableView : VVBaseTableView <VVListTableViewDelegate>
{
	NSMutableArray * dataArr;
}
@property (nonatomic)BOOL editingEnabled;
@property (nonatomic)BOOL pullToRefreshEnabled;
@property (nonatomic,retain)Class  objectsClass;
-(void)loadCellsInSection:(NSIndexPath*)indexPath;

@property (nonatomic, retain) id<VVListTableViewDelegate> vvListTableViewDelegate;

@property (nonatomic) int pageOrigin;
@property (nonatomic) int pageSize;

- (void)refreshTable;
//
@property (retain,nonatomic)VVListTableViewDirector * director;
//
-(NSArray*)allIndexPathes;
//headers
-(VVListTableView *)addEmptySection;
-(VVListTableView *)insertEmptySectionAtSection:(NSUInteger)section;

-(VVListTableView*)addDynamicHeader:(VVParametrsObject*)loadParametrs andClass:(Class)cl;
-(VVListTableView*)addDynamicHeader:(VVParametrsObject*)loadParametrs;
-(VVListTableView*)setDynamicHeader:(VVParametrsObject *)loadParametrs andClass:(Class)cl forSection:(NSUInteger)section;
-(VVListTableView*)setDynamicHeader:(VVParametrsObject *)loadParametrs forSection:(NSUInteger)section;



//dynamic cells
-(VVListTableView*)addDynamicCells:(VVParametrsObject*)loadParametrs andClass:(Class)cl;
-(VVListTableView*)addDynamicCells:(VVParametrsObject*)loadParametrs;
-(VVListTableView*)insertDynamicCells:(VVParametrsObject *)loadParametrs andClass:(Class)cl atSection:(NSUInteger)section;
-(VVListTableView*)insertDynamicCells:(VVParametrsObject *)loadParametrs atSection:(NSUInteger)section;

//static cells
-(VVListTableView*)addStaticCells;
-(VVListTableView*)addStaticCellsWithData:(NSArray*)arr;
-(VVListTableView*)insertData:(NSArray *)arr toStaticCellAtSection:(NSUInteger)section;
-(VVListTableView*)setData:(NSArray *)arr toStaticCellAtSection:(NSUInteger)section;


-(void)insertDynamicCells:(Class)objectClass inSection:(int)section;
#pragma mark - vvListTableViewDelegate
-(VVParametrsObject *)paramsForLoad:(VVParametrsObject*)params forIndexPath:(NSIndexPath*)indexPath;

-(UITableViewCell *)listTableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath objectForRow:(id)object;

-(UITableViewCell *) listTableView:(UITableView *) tableView loadingCellViewForRowAtIndexPath:(NSIndexPath*)indexPath;

-(void)listTableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath withObject:(VVObject*)object;
-(CGFloat)listTableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
-(CGFloat)listTableView:(UITableView *)tableView heightForLoadCellAtIndexPath:(NSIndexPath *)indexPath;

-(CGFloat)listTableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section;
-(UIView *)listTableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section withObject:(id)object;



-(void)listTableViewDidLoadData:(id)data forHeader:(NSUInteger)section;


-(void)setPageSize:(int)pageSize InSection:(NSUInteger)section;
-(void)setPageOrigin:(int)pageOrigin InSection:(NSUInteger)section;
@end
