//
//  VVListTableViewSection.m
//  Chooos
//
//  Created by Aleksandr Padalko on 5/8/14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "VVListTableViewSection.h"
@implementation VVTableDirectorObject
@end
@implementation VVTableDirectorCell


@end
@implementation VVTableDirectorHeader
@end

@implementation VVListTableViewSection
- (id)init
{
    self = [super init];
    if (self) {
        _pageOrigin=0;
        _pageSize=10;
        _columnsCount=0;
        _fullLoad=NO;
    }
    return self;
}
-(void)createCell:(VVParametrsObject *)loadParametrs andClassName:(NSString*)className{
    
    _cells=[[VVTableDirectorCell alloc]init];
    _cells.loadParametrs=loadParametrs;
    _cells.className=className;
    _cells.objects=[[NSMutableArray alloc]init];
    if (loadParametrs) {
        _cells.type=kVVDirectorObjectTypeDynamic;
    }else{
        _cells.type=kVVDirectorObjectTypeStatic;
    }
    
}
-(void)createHeader:(VVParametrsObject *)loadParametrs andClassName:(NSString *)className{
    _header=[[VVTableDirectorHeader alloc]init];
    _header.className=className;
    _header.type=kVVDirectorObjectTypeDynamic;
    _header.loadParametrs=loadParametrs;
    
}
-(id)getObjectAtRow:(NSInteger)row{
    if (row>=self.cells.objects.count) {
        return nil;
    }
    return [_cells.objects objectAtIndex:row];
}
-(BOOL)isCountGraterThanCellsCount:(NSInteger)count{
    if(_cells.type==kVVDirectorObjectTypeDynamic){
        if (_columnsCount>0) {
            int rows=truncf(((float)_cells.objects.count)/_columnsCount);
            int rest=_cells.objects.count%_columnsCount;
            if (rest>0) {
                rows++;
                
            }
            if (count>=rows) {
                return YES;
            }
            
        }else{
            if (count>=_cells.objects.count) {
                return YES;
            }
        }
    }
    
    return NO;
    
}
-(NSInteger)rowsCount{
    
    if(_cells.type==kVVDirectorObjectTypeDynamic && _fullLoad==NO )
        return _cells.objects.count+1;
    else
        return _cells.objects.count;
    
}
@end
