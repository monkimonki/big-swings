//
//  RequestTableView.m
//  FGApp
//
//  Created by macmini9 on 4/30/14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "VVListTableView.h"
#import "VVViewController.h"
#import "VVViewController+VVTextField.h"
@interface VVListTableView(){
    UIRefreshControl *refreshControl;
    BOOL onVVControl;
    VVViewController * v;
}
@end
@implementation VVListTableView
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.editingEnabled=NO;
		dataArr = [NSMutableArray array];
		self.pageSize = 10;
		self.pageOrigin = 0;
        _director=[[VVListTableViewDirector alloc]init];
        _director.baseTableView=self;
        self.pullToRefreshEnabled=YES;
        
        	[self setVvListTableViewDelegate: self];
        
        dataArr=[[NSMutableArray alloc]init];

        [self setBackgroundColor:[UIColor clearColor]];
        [self setSeparatorStyle:UITableViewCellSeparatorStyleNone];
      //  [dataArr addObject:[[NSMutableDictionary alloc] init]];
        
        
    }
    return self;
}


-(void)setPullToRefreshEnabled:(BOOL)pullToRefreshEnabled{
    
    _pullToRefreshEnabled=pullToRefreshEnabled;
    
    if (_pullToRefreshEnabled) {
        refreshControl = [[UIRefreshControl alloc]init];
        [self addSubview:refreshControl];
        [refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];

    }else{
        [refreshControl removeFromSuperview];
        refreshControl=nil;
    }
}
- (void)refreshTable {
    //TODO: refresh your data
    
   [self sendSubviewToBack: refreshControl];
    [self.director refresh:^(BOOL success) {
        
        [refreshControl endRefreshing];
        if (success) {
            [self reloadData];
        }else{
            NSLog(@"ERORR TO RELOAD");
        }
        
        
        
    }];
  
 
}
-(NSArray *)allIndexPathes{
    
return     [self.director allIndexPathes];
    
}
#pragma mark - ()

-(VVListTableView *)addEmptySection{
    [_director addEmptySection];
    return self;
}
-(VVListTableView *)insertEmptySectionAtSection:(NSUInteger)section{
        [_director addEmptySection];
    return self;
}

-(VVListTableView *)addDynamicHeader:(VVParametrsObject *)loadParametrs{
    
    [_director addDynamicHeader:loadParametrs andClass:nil];
    return self;
}

-(VVListTableView *)addDynamicHeader:(VVParametrsObject *)loadParametrs andClass:(__unsafe_unretained Class)cl{
    
    [_director addDynamicHeader:loadParametrs andClass:cl];
    return self;
}
-(void)listTableViewDidLoadEmtyListForSection:(NSUInteger)section{
    
}
-(VVListTableView*)setDynamicHeader:(VVParametrsObject *)loadParametrs andClass:(Class)cl forSection:(NSUInteger)section{
    [_director setDynamicHeader:loadParametrs andClass:cl forSection:section];
    return self;
    
}
-(VVListTableView*)setDynamicHeader:(VVParametrsObject *)loadParametrs forSection:(NSUInteger)section{
        [_director setDynamicHeader:loadParametrs andClass:nil forSection:section];
    return self;
}

-(VVListTableView*)addDynamicCells:(VVParametrsObject*)loadParametrs andClass:(Class)cl{
    
    [_director addDynamicCells:loadParametrs andClass:cl];
    return self;
}
-(VVListTableView*)addDynamicCells:(VVParametrsObject*)loadParametrs{
    [_director addDynamicCells:loadParametrs andClass:nil];
    
    return self;
}
-(VVListTableView*)insertDynamicCells:(VVParametrsObject *)loadParametrs andClass:(Class)cl atSection:(NSUInteger)section{
    
    [_director insertDynamicCells:loadParametrs andClass:cl atSection:section];
    
    return self;
}
-(VVListTableView*)insertDynamicCells:(VVParametrsObject *)loadParametrs atSection:(NSUInteger)section{
        [_director insertDynamicCells:loadParametrs andClass:nil atSection:section];
    return self;
}

-(VVListTableView*)addStaticCells{
    
    [_director addStaticCells];
    
    return self;
    
}
-(VVListTableView *)addStaticCellsWithData:(NSArray *)arr{
    [_director addStaticCellsWithData:arr];
    return self;
}
-(VVListTableView *)insertData:(NSArray *)arr toStaticCellAtSection:(NSUInteger)section{
    [_director insertData:arr toStaticCellAtSection:section];
    return self;
}

-(VVListTableView *)setData:(NSArray *)arr toStaticCellAtSection:(NSUInteger)section{
    [_director setData:arr toStaticCellAtSection:section];
    return self;
}

#pragma mark - ()


-(VVObject*)getObjectOfCell:(NSIndexPath*)indexPath{
      
    return [_director getObjectAtIndexPath:indexPath];
}
#pragma mark - tableViewDelegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return  [_director getRowsCountInSection:section];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [_director getNumberOfSections];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell * cell= [_director controlCell:tableView andIndexPath:indexPath];
    
    if (self.editingEnabled) {
        [[self vvListTableViewDelegate ] listTableView:tableView configurateEditableCell:cell forIndexPath:indexPath];
        
    }
    
    return cell;
   
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
 
    
    if (self.editingEnabled) {
        [[self vvListTableViewDelegate ] listTableView:tableView didSelectEditebleRowAtIndexPath:indexPath withObject:[_director getObjectAtIndexPath:indexPath]];
    }
    else
    [[self vvListTableViewDelegate ] listTableView:tableView didSelectRowAtIndexPath:indexPath withObject:[_director getObjectAtIndexPath:indexPath]];
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [_director controlHeight:tableView andIndexPath:indexPath];
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
   return  [self.vvListTableViewDelegate listTableView:tableView heightForHeaderInSection:section];
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return [_director controlHeader:tableView andSection:section];
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return  [self.vvListTableViewDelegate listTableView:tableView heightForFooterInSection:section];
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return [_director controlFooter:tableView andSection:section];
}


#pragma mark - ()

#pragma mark - listTableViewDelegate
-(CGFloat)listTableView:(UITableView *)tableView heightForLoadCellAtIndexPath:(NSIndexPath *)indexPath{
    
    
    return 20;
}
-(CGFloat)listTableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 64;
}
-(CGFloat)listTableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0;
}
-(void)listTableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath withObject:(VVObject *)object{
    
}
-(void)listTableView:(UITableView *)tableView didSelectEditebleRowAtIndexPath:(NSIndexPath *)indexPath withObject:(VVObject *)object{
    
}

-(void)listTableView:(UITableView *)tableView configurateEditableCell:(UITableViewCell *)cell forIndexPath:(NSIndexPath *)indexPath{
    
}
-(void)listTableViewDidLoadData:(id)data forHeader:(NSUInteger)section{
    
    NSLog(@"%@",data);
    
}
-(UIView *)listTableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
   
    return nil;
}
-(CGFloat)listTableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0;
}
-(UIView *)listTableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section withObject:(id)object{
    return nil;
}
-(UITableViewCell *)listTableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath objectForRow:(VVObject *)object {
	NSLog(@"delegate open");
    NSLog(@"%@",object.objectId);
    static NSString *CellIdentifer = @"simpleCell";
	
	UITableViewCell  * cell=[self dequeueReusableCellWithIdentifier:CellIdentifer];
	
	if (!cell) {
		cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifer];
	}
    [cell setTag:0];
	cell.textLabel.text=object.objectId;
	return  cell;
	
}
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    
 //  [super scrollViewWillBeginDragging:scrollView];
    
    if (onVVControl) {
        [[v  curResponder] resignFirstResponder];
    }
}
-(void)listTableView:(UITableView*)listTableView refresh:(void (^)( BOOL success ) )cb{
    
    cb(YES);
}

-(UITableViewCell *)listTableView:(UITableView *)tableView loadingCellViewForRowAtIndexPath:(NSIndexPath *)indexPath{
	static NSString *CellIdentifer = @"loadingCell";
	
	UITableViewCell  * cell=[self dequeueReusableCellWithIdentifier:CellIdentifer];
	
	if (!cell) {
		cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifer];
        
        [cell.textLabel setTextAlignment:NSTextAlignmentCenter];
	}
    cell.textLabel.text=@"loading";
	return  cell;
}
-(VVParametrsObject *)paramsForLoad:(VVParametrsObject*)params forIndexPath:(NSIndexPath*)indexPath {
	
	return params;
}

//
-(id<VVListTableViewDelegate>)vvListTableViewDelegate{
    return _director.delegate;
}
-(void)setVvListTableViewDelegate:(id)vvListTableViewDelegate{
    if ([vvListTableViewDelegate isKindOfClass:[VVViewController class]]) {
        onVVControl=YES;
        v=vvListTableViewDelegate;
    }
    _director.delegate=vvListTableViewDelegate;
}


-(void)dealloc{
    self.director=nil;
}
-(void)setPageSize:(int)pageSize InSection:(NSUInteger)section{
    
VVListTableViewSection *sec=[self.director getSection:section];
    sec.pageSize=pageSize;

}
-(void)setPageOrigin:(int)pageOrigin InSection:(NSUInteger)section{
    VVListTableViewSection *sec=[self.director getSection:section];
    sec.pageOrigin=pageOrigin;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
	if ([otherGestureRecognizer isKindOfClass:[UISwipeGestureRecognizer class]]) {
		return YES;
	}
	return NO;
}

@end
