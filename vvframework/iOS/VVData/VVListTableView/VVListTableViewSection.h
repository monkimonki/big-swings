//
//  VVListTableViewSection.h
//  Chooos
//
//  Created by Aleksandr Padalko on 5/8/14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import <Foundation/Foundation.h>
enum kVVDirectorObjectType{
	
	kVVDirectorObjectTypeDynamic=1,
	kVVDirectorObjectTypeStatic
};

@interface VVTableDirectorObject:NSObject{
    
}
@property (nonatomic)enum kVVDirectorObjectType type;
@property(nonatomic,retain)VVParametrsObject * loadParametrs;
@property(nonatomic,retain)NSString * className;
@end

@interface VVTableDirectorCell : VVTableDirectorObject{
    
}
@property(nonatomic,retain)NSMutableArray* objects;


@end
@interface VVTableDirectorHeader : VVTableDirectorObject{
    
}

@property(nonatomic,retain)NSDictionary * object;
@end
@interface VVListTableViewSection : NSObject{
   
}
@property (nonatomic)BOOL fullLoad;

@property (retain,nonatomic)   VVTableDirectorCell * cells;
@property (retain,nonatomic)   VVTableDirectorHeader * header;

@property (nonatomic) NSUInteger columnsCount;

-(void)createCell:(VVParametrsObject *)loadParametrs andClassName:(NSString*)className;
-(void)createHeader:(VVParametrsObject*)loadParametrs andClassName:(NSString*)className;

-(BOOL)isCountGraterThanCellsCount:(NSInteger)count;

-(id)getObjectAtRow:(NSInteger)row;

-(NSInteger)rowsCount;

@property(nonatomic)int pageOrigin;
@property(nonatomic)int pageSize;

@end
