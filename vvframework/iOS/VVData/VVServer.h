//
//  VVServer.h
//  FGApp
//
//  Created by macmini9 on 4/30/14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VVDownloader.h"
#import "VVHelper.h"
#import "VVLog.h"
#import "VVEntity.h"
#import "VVParametrsObject.h"
#import "NSMutableURLRequest+VVRequestCategory.h"
#import "VVMutableURLRequest.h"
#import "VVObject.h"
#import "VVListTableView.h"
#import "VVBaseTableView.h"
#import "VVFile.h"

#import "VVImageView.h"
#define kEntityName @"entityName"
@interface VVServer : NSObject
enum kVVServerDataTypes{
	
	kVVServerDataTypeLocal,
	kVVServerDataTypeParse,
	kVVServerDataTypeCUrl,
};




+(VVServer *)inst;
+(VVServer *)create;

-(void)getObject:(VVObject*)obj callback:(VVObjectCallback)cb;
-(void)loadObjects:(VVParametrsObject *)par callback:(VVArrayCallback)cb;
-(void)loadObject:(VVParametrsObject *)par callback:(VVObjectCallback)cb;
-(void)doAction:(VVParametrsObject*)actionParameters  callback:(VVObjectCallback)cb;


-(void)loadFile:(VVFile*)file callback:(VVDataCallback)cb;



@property (nonatomic) enum kVVServerDataTypes dataType;
@property(retain,nonatomic)NSString * serverRootAdress;


@property(retain,nonatomic)NSMutableDictionary * localServerData;

-(void)addDataToLocalServer:(id)obj forKey:(NSString*)key;
-(void)addDataToLocalServer:(id)obj atIndex:(int)index forKey:(NSString*)key;

-(void)addResponseDataModel:(id)dm;

@property (retain,nonatomic)NSMutableArray * responseDataModel;


-(void)searchInStorage:(NSString*)searchString andKey:(NSString*)searchKey andClass:(Class)searchClass valueParams:(NSDictionary *)dict andBlock:(void(^)(NSArray * arr))block;

@end
