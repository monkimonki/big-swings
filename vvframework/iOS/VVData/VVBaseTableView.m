//
//  CVParseTableView.m
//  FGApp
//
//  Created by macmini9 on 4/18/14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "VVBaseTableView.h"

@implementation VVBaseTableView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
		[self setDelegate:self];
		[self setDataSource:self];
        
        self.allowsMultipleSelectionDuringEditing = NO;
	}
    return self;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	return nil;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	return 1;
}


-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return YES - we will be able to delete all rows
    return NO;
}
@end
