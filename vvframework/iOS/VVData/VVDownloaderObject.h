//
//  VVDownloaderObject.h
//  FGApp
//
//  Created by macmini9 on 4/30/14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import <Foundation/Foundation.h>

enum kVVDownloaderObjectTypes{
	
	kVVDOTypeList,
    kVVDOTypeObject
	
};

@interface VVDownloaderObject : NSObject
+(VVDownloaderObject*)createWithInnerObject:(id) object;
-(id)initWithInnerObject:(id) object;

@property (nonatomic) enum kVVDownloaderObjectTypes type;
@property (nonatomic, retain) id innerObject;
@end
