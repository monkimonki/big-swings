//
//  VVListTableViewController.m
//  Chooos
//
//  Created by mm7 on 08.05.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "VVListTableViewController.h"

@interface VVListTableViewController ()

@end

@implementation VVListTableViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self setup];
    }
    return self;
}
-(id)init{
    if (self=[super init ]) {
        [self setup];
    }
    return self;
}
-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    [self.listTableView setFrame:self.view.bounds];
}
-(void)setup{
    
    listTableViewViews=[[NSMutableArray alloc]init];
}
-(void)addViewToCell:(UIView *)v toSection:(NSUInteger)section{
    NSMutableDictionary * dict;
    if (section>=listTableViewViews.count) {
        
       dict=[self createSectionDict];
     
        
    }else{
        
     dict=   [listTableViewViews objectAtIndex:section];
        
    }
       [[dict valueForKey:@"cells" ] addObject:v];
}

-(void)setViewtoCell:(UIView *)v toSection:(NSUInteger)section atIndex:(NSUInteger)index{
      NSMutableDictionary * dict;
    if (section>=listTableViewViews.count){
         [ self addViewToCell:v toSection:section];
    }else{
        
        dict=   [listTableViewViews objectAtIndex:section];
        NSMutableArray * cells=[dict valueForKey:@"cells"];
        if (index>=cells.count) {
            [cells addObject:v];
        }else{
            [cells replaceObjectAtIndex:index withObject:v];
        }
        
    }
    
}
-(void)setViewToHeaderView:(UIView *)v toSection:(NSUInteger)section{
          NSMutableDictionary * dict;
    if (section>=listTableViewViews.count) {
        
        dict=[self createSectionDict];
        
        
    }else{
        
        dict=   [listTableViewViews objectAtIndex:section];
        
    }
    
    
    [dict setValue:v forKey:@"header"];
    
}

-(NSMutableDictionary*)createSectionDict{
    
    NSMutableDictionary * dict=[[NSMutableDictionary alloc]init];
    [dict setValue:[[NSMutableArray alloc] init] forKey:@"cells"];
    [listTableViewViews addObject:dict];
    
    return dict;
    
    
}

-(UIView *)getViewOfCellInSection:(NSUInteger)section atIndex:(NSUInteger)index{
    
    if (section<listTableViewViews.count) {
        
        NSArray * arr=[[listTableViewViews objectAtIndex:section] valueForKey:@"cells"];
        
        if (index<arr.count) {
            return [[[listTableViewViews objectAtIndex:section] valueForKey:@"cells"] objectAtIndex:index];
        }
    }
    return nil;
}

-(UIView *)getViewOfCellInIndexPath:(NSIndexPath*)indexPath{
    
    return [self getViewOfCellInSection:indexPath.section atIndex:indexPath.row];
}

-(UIView *)getViewOfHeaderInSection:(NSUInteger)section{
    
    
    if (section<listTableViewViews.count) {
        
        
        return [[listTableViewViews objectAtIndex:section] valueForKey:@"header"];
        
    }
    return nil;
    
}


#pragma mark -()
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.listTableView=[[VVListTableView alloc]initWithFrame:self.view.bounds];
    [self.listTableView setVvListTableViewDelegate:self];
    [self.view addSubview:self.listTableView];
    


    [self.view setBackgroundColor:[UIColor whiteColor]];
    
	// Do any additional setup after loading the view.
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(VVParametrsObject *)paramsForLoad:(VVParametrsObject*)params forIndexPath:(NSIndexPath*)indexPath{
	return  params;
}

-(UITableViewCell *)listTableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath objectForRow:(id)object{
    static NSString *   CellIdentifer = @"simpleCell";
	
	UITableViewCell  * cell=[tableView dequeueReusableCellWithIdentifier:CellIdentifer];
	if (!cell) {
		cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifer];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
	}
	return  cell;
    
    
}
-(UITableViewCell *)listTableView:(UITableView *)tableView loadingCellViewForRowAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *CellIdentifer = @"loadCell";
	
	UITableViewCell  * cell=[tableView dequeueReusableCellWithIdentifier:CellIdentifer];
	if (!cell) {
		cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifer];
	}
	return  cell;
}
-(void)listTableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath withObject:(VVObject*)object{
}
-(void)listTableView:(UITableView *)tableView didSelectEditebleRowAtIndexPath:(NSIndexPath *)indexPath withObject:(VVObject *)object{
    
}
-(void)listTableView:(UITableView *)tableView configurateEditableCell:(UITableViewCell *)cell forIndexPath:(NSIndexPath *)indexPath{
    
}
-(CGFloat)listTableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 22;
}
-(CGFloat)listTableView:(UITableView *)tableView heightForLoadCellAtIndexPath:(NSIndexPath *)indexPath
{
	return 20.f;
}
-(CGFloat)listTableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
	return 0.f;
}
-(CGFloat)listTableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0;
}
-(UIView *)listTableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section withObject:(id)object{
    return nil;
}
-(UIView *)listTableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section withObject:(id)object
{
	
    return nil;
}
-(void)listTableViewDidLoadData:(id)data forHeader:(NSUInteger)section
{
	
}
-(void)cleanCellsViewInSection:(NSUInteger)section{
    if (listTableViewViews.count>section) {
        NSMutableDictionary * dict=[listTableViewViews objectAtIndex:section];
        [dict setValue:[[NSMutableArray alloc]init] forKey:@"cells"];
    }
    
}
-(void)cleanViews{
    
    listTableViewViews=[[NSMutableArray alloc]init];
}
-(void)listTableView:(UITableView*)listTableView refresh:(void (^)( BOOL success ) )cb{
    
    cb(YES);
}
-(void)listTableViewDidLoadEmtyListForSection:(NSUInteger)section{
    
}

@end
