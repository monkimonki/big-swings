//
//  NSMutableURLRequest+VVRequestCategory.h
//  ChoooseData
//
//  Created by Aleksandr Padalko on 5/1/14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableURLRequest (VVRequestCategory)
-(void)setHTTPBodyAsDictionary:(NSDictionary*)dict;
-(NSString*)getStringFromHTTPBody;
-(NSDictionary*)getDictionaryFromHTTPBody;

@end
