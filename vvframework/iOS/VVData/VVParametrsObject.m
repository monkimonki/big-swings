//
//  VVParametrsObject.m
//  FGApp
//
//  Created by macmini9 on 4/30/14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "VVParametrsObject.h"
#import "VVServer.h"

@implementation VVParametrsObject {
	NSArray * responseKeyPath;

}
static NSString * resNameCollection=@"Collection";

NSString * getMethodNameByType(int t){
    NSString* mt;
    switch (t) {
        case kMethodTypeGet:
            mt=@"GET";
            break;
            
        case KMethodTypePost:
            mt=@"POST";
            break;
        case kMethodTypePut:
            mt=@"PUT";
            break;
        case kMethodTypeDelete:
            mt=@"DELETE";
            break;
    }
    
    
    return mt;
    
}



+(VVParametrsObject *)createWithEntity:(VVEntity *)entity{
    return [[self alloc]initWithEntity:entity];
}
+(VVParametrsObject *)create {
	return [[self alloc] init];
}
-(id)initWithEntity:(VVEntity*)entity{
	if (self=[super init]) {
	
		_entity = entity;
        [self baseSetup];
	
	}
	return self;
}
-(id)init{
	if (self=[super init]) {
	
	
		_entity = [VVEntity create];
        [self baseSetup];
	
	}
	return self;
}
-(void)baseSetup{
    _includeKeys=[[NSMutableArray alloc]init];
    _pageOrigin = 0;
    _subQuerry=[[NSMutableArray alloc]init];
	_pageSize = -1;
	responseKeyPath = [NSArray array];
    _methodType=kMethodTypeGet;
    _totalIncludesKeys=[[NSMutableArray alloc]init];
    _body=[[NSMutableDictionary alloc]init];
    _orQuerrys=[[NSMutableArray alloc]init];
    
    
    _lessNumberParams=[[NSMutableArray alloc]init];
    _highNumberParams=[[NSMutableArray alloc]init];
}
-(void)addHideKey:(NSString *)key{
    
    if (!debugHideKey) {
        debugHideKey=[[NSMutableArray alloc]init];
    }
    [debugHideKey addObject:[VVHelper propNameToKey:key]];
}
-(void)addSearchString:(NSString *)str forKey:(NSString*)key{
    
    if (!_searchKeys) {
        _searchKeys=[[NSMutableArray alloc]init];
    }
    [_searchKeys addObject:[NSDictionary dictionaryWithObject:str forKey:key]];
}
-(void)addShowKey:(NSString *)key{
    
    if (!debugShowKeys) {
        debugShowKeys=[[NSMutableArray alloc]init];
    }
    [debugShowKeys addObject:[VVHelper propNameToKey:key]];
}

-(VVMutableURLRequest*)urlRequest{
    
    NSString * urlString=[NSString stringWithFormat:@"%@%@",[VVServer inst].serverRootAdress,[self stringFromEntity:_entity]];
  
    
    VVMutableURLRequest * req=[[VVMutableURLRequest alloc]init];
 
    [req setURL:[NSURL URLWithString:urlString]];
    [req setHTTPMethod:getMethodNameByType(_methodType)];
    
  //  NSLog(@"%@",req.HTTPMethod);
  
    
    
    if ([[_endPoint componentsSeparatedByString:@"_"]count]==2) {
        [req setReturnigType:kVVReturningTypeList];
    }else{
        [req setReturnigType:kVVReturningTypeObject];
    }
    
    
    
    
    if (_pageSize>0) {
        [_body setValue:[NSString stringWithFormat:@"%d",_pageSize] forKey:@"limit"];
    }
    
    if (_pageOrigin>=0) {
        [_body setValue:[NSString stringWithFormat:@"%d",_pageOrigin] forKey:@"offset"];

    }
 
    for (NSString * key in _entity.headerFieldDict) {
           [req setValue:[_entity.headerFieldDict valueForKey:key]  forHTTPHeaderField:key];
    }
    
       [_body setValuesForKeysWithDictionary:_entity.body];
    if (debugHideKey.count>0) {
        [_body setValue:debugHideKey forKey:@"zdebug_hide_key"];
    }
    if (debugSearhcKey.count>0) {
        [_body setValue:debugSearhcKey forKey:@"zdebug_search_key"];
    }
    if (debugShowKeys) {
        [_body setValue:debugShowKeys forKey:@"zdebug_show_key"];
    }
    
    for (NSDictionary* dic in self.additionalParams) {
        [_body addEntriesFromDictionary:dic];
    }
    [req setHTTPBodyAsDictionary:_body];
    
    
    
    
    //NSLog(@"\n-------%@ %@------%@\n------------------------------------",getMethodNameByType(_methodType),_endPoint,NSStringFromRequest(req));
    req.endPointName=_endPoint;
    VVLog(@"\n-------%@ %@------%@\n",getMethodNameByType(_methodType),_endPoint,NSStringFromRequest(req));
  //  NSLog(@"%@",NSStringFromRequest(req));
    //setBody
    
    //
    
    return req;
}


-(NSString*)stringFromEntity:(VVEntity*)entity{
    

    
    NSMutableString * str = [[NSMutableString alloc]init];
    if (!entity|| !entity.name    ) {
        return @"";
    }
    [str appendString:entity.name];
    _endPoint=[NSString stringWithFormat:@"%@_%@",entity.name,resNameCollection];
	for (NSString * key in entity.properties) {
		id value = [entity.properties objectForKey:key];
		if ([value isKindOfClass:[VVEntity class]]) {
			//PFQuery * inQuery = [self queryFromEntity:value];
			//[query whereKey:key equalTo:inQuery];
            VVEntity * en=(VVEntity*)value;
          [_entity setValuesForKeysWithDictionary:en.body];
              [str appendString:[NSString stringWithFormat:@"/%@",[self stringFromEntity:en]]];
            
		} else if ([value isKindOfClass:[NSArray class]]) {
			for (NSString *val in value) {
                [str appendString:value];
                
            }
            
		} else {
			//[query whereKey:key equalTo:value];
            
           
              [str appendString:[NSString stringWithFormat:@"/%@",value]];
            _endPoint=[[_endPoint componentsSeparatedByString:@"_"] firstObject];
            if ([key isEqualToString:@"action"]) {
                _endPoint=[NSString stringWithFormat:@"%@_%@_%@",_endPoint,key,value];
            }
            
		}
	}
	return [NSString stringWithFormat:@"%@",str];

}
-(NSString *)endPoint{
    
    return [[_endPoint componentsSeparatedByString:@"_"] firstObject];
    
}


#ifdef PARSE
-(PFQuery *)queryFromEntity:(VVEntity *) entity
{
    PFQuery * query=[self getMainQuery:entity];
    
    [self updateQuery:entity andQuery:query];
    return query;
    

}

-(PFQuery*)getMainQuery:(VVEntity*)entity{
    PFQuery * query=[PFQuery queryWithClassName:entity.name] ;
    _entity=entity;
    
    for (NSString * key in entity.properties) {
        id value = [entity.properties objectForKey:key];
        if ([value isKindOfClass:[VVEntity class]]) {
            
            query=[self getMainQuery:value];
            
            
        }
    }
    
    return query;
}

-(PFQuery *)updateQuery:(VVEntity*)entity andQuery:(PFQuery*)query{
    for (NSString * key in entity.properties) {
		id value = [entity.properties objectForKey:key];
		if ([value isKindOfClass:[VVEntity class]]) {
			//PFQuery * inQuery = [self queryFromEntity:value];
            
		} else if ([value isKindOfClass:[NSArray class]]) {
			[query whereKey:key containsAllObjectsInArray:value];
		} else {
            
            if ([_entity.name isEqualToString:entity.name]) {
                		[query whereKey:@"objectId" equalTo:value];
   

            }else{
           
                PFObject * obj=[PFObject objectWithClassName:entity.name];
                obj.objectId=value;
                
            [query whereKey:[NSString stringWithFormat:@"%@",key] equalTo:obj];
                [_totalIncludesKeys addObject:[NSString stringWithFormat:@"%@",key]];
            }
			
		}
	}
	return query;
}

-(void)addAditionalQueryParam:(VVParametrsObject*)obj withKey:(NSString*)key andKeyInMainQuery:(NSString*)mainQueryKey{
    
    NSMutableDictionary * dict=[[NSMutableDictionary alloc]init];
    
    [dict setValue:obj forKey:@"par"];
    [dict setValue:key forKey:@"parKey"];
    [dict setValue:mainQueryKey forKey:@"mainKey"];
    
    if (!_additionalQueryParms) {
        _additionalQueryParms=[[NSMutableArray alloc]init];
        
        
    }
    
    [_additionalQueryParms addObject:dict];
}


-(void)addMathQueryParam:(VVParametrsObject*)obj forKeyInMainQuery:(NSString*)mainQueryKey{
    
    NSMutableDictionary * dict=[[NSMutableDictionary alloc]init];
    
    [dict setValue:obj forKey:@"par"];
    [dict setValue:mainQueryKey forKey:@"mainKey"];
    
    if (!_additionalQueryParms) {
        _additionalQueryParms=[[NSMutableArray alloc]init];
        
        
    }
    
    [_additionalQueryParms addObject:dict];
}
-(PFQuery *)query
{
    
    

	PFQuery * query = [self queryFromEntity:_entity];
    if (self.additionalParams.count>0) {
        for (NSDictionary * aParm in self.additionalParams) {
            id val=[aParm valueForKey:[[aParm allKeys]firstObject ]];
            if ([val isKindOfClass:[VVObject class]]) {
                PFObject * obj=[self pfObject:(VVObject*)val];
                 [query whereKey:[[aParm allKeys]firstObject ] equalTo:obj];
                [_totalIncludesKeys addObject:[[aParm allKeys]firstObject ]];
                
                
            }else
            [query whereKey:[[aParm allKeys]firstObject ] equalTo:[aParm valueForKey:[[aParm allKeys]firstObject ]]];
            
             
            
        }

        
    }
    for (NSDictionary * aParm in self.lessNumberParams) {
        [query whereKey:[[aParm allKeys]firstObject ] lessThanOrEqualTo:[aParm valueForKey:[[aParm allKeys]firstObject ]]];
        
        
    }
    
    for (NSDictionary * aParm in self.highNumberParams) {
        [query whereKey:[[aParm allKeys]firstObject ] greaterThan:[aParm valueForKey:[[aParm allKeys]firstObject ]]];
        
        
    }
    
    for (NSDictionary * aParm in self.matchesArrParam) {
        [query whereKey:[[aParm allKeys]firstObject ] containedIn:[aParm valueForKey:[[aParm allKeys]firstObject ]]];
      //  [query whereKey:[[aParm allKeys]firstObject ] greaterThan:[aParm valueForKey:[[aParm allKeys]firstObject ]]];
        
        
    }
    if (_searchKeys.count>0) {
        
        
        for (NSDictionary * aParm in self.searchKeys) {
            
            for (NSString * key in aParm) {
            //    [query whereKey:key containsString:[aParm valueForKey:key]];
                [query whereKey:key matchesRegex:[aParm valueForKey:key] modifiers:@"i"];
            }
            
            
        }
            
        
    }

    
	query.skip = _pageOrigin;
    if (_pageSize<1) {
        query.limit=1;
    }else
	query.limit = _pageSize;
    
    
    if (self.subQuerry.count>0) {
        NSMutableArray * subQuerryArr=[[NSMutableArray alloc]init];
        for (VVParametrsObject* sPar in self.subQuerry) {
            PFQuery * qq=[sPar query];
            qq.limit=-1;
            qq.skip=0;
            [qq setValue:nil forKey:@"include"];
            [_totalIncludesKeys addObjectsFromArray:sPar.totalIncludesKeys];
            [_totalIncludesKeys addObjectsFromArray:self.includeKeys];
           [subQuerryArr addObject:qq];
         
        }
        int tempLim=query.limit;
        int tempOffset=query.skip;
        query.limit=-1;
         query.skip=0;
       
        [subQuerryArr insertObject:query atIndex:0];
        
       PFQuery  *nQ= [PFQuery orQueryWithSubqueries:subQuerryArr];
        nQ.limit=tempLim;
        nQ.skip=tempOffset;
      
        for (NSString * n in _totalIncludesKeys) {
            [nQ includeKey:n];
        }
        
        return nQ;
    }else{
        for (NSString * n in _totalIncludesKeys) {
            [query includeKey:n];
        }
        
        if (_additionalQueryParms.count>0) {
            for (NSDictionary *qDict in _additionalQueryParms) {
                
                if ([qDict valueForKey:@"parKey"]) {
                    VVParametrsObject * tPar=[qDict valueForKey:@"par"];
                    PFQuery *q= [tPar query];
                    q.limit=-1;
                    q.skip=0;
                    //   [query whereKey:[qDict valueForKey:@"mainKey"] matchesKey:[qDict valueForKey:@"parKey"] inQuery:[tPar query]];
                    [query whereKey:[qDict valueForKey:@"mainKey"] matchesKey:[qDict valueForKey:@"parKey"] inQuery:q];
                }else{
                    VVParametrsObject * tPar=[qDict valueForKey:@"par"];
                    PFQuery *q= [tPar query];
                    q.limit=-1;
                    q.skip=0;
                    
                    [query whereKey:[qDict valueForKey:@"mainKey"] matchesQuery:q];

                }
            
                
            }
        }
        
        
        if (_orQuerrys.count>0) {
            NSMutableArray * allQ=[[NSMutableArray alloc]init];
            query.limit=-1;
            query.skip=0;
            [allQ addObject:query];
            for (VVParametrsObject * obj in _orQuerrys) {
                obj.pageOrigin=0;
                obj.pageSize=0;
                PFQuery * q= [obj query];
                q.limit=-1;
                q.skip=0;
                
                [allQ addObject:q];
                
            }
            
         
            PFQuery *q= [PFQuery orQueryWithSubqueries:allQ];
            
            q.skip=self.pageOrigin;
            q.limit=self.pageSize;
            for (NSString * key in _includeKeys) {
                [q includeKey:key];
            }
            return q;
        }
        
        for (NSString * key in _includeKeys) {
            [query includeKey:key];
        }
          return query;
    }
  
	
}
-(PFObject*)pfObject:(VVObject*)obj{
    
    
    NSMutableDictionary * dict=[obj logProp];
    NSLog(@"%@",dict);

    PFObject * pfObj;
    
    if ([obj.entityName isEqualToString:@"_User"]) {
        
       pfObj =[[PFUser alloc]init];
    }else{
        pfObj=[[PFObject alloc]initWithClassName:obj.entityName];
    }
  
    
    for (NSString * key in dict) {
        
        if ([[dict valueForKey:key] isKindOfClass:[NSDictionary class]]) {
            NSLog(@"%@",[dict valueForKey:[VVHelper keyToPropName:key]]);
            
            id obj2=[VVObject objectFromDictionary:[dict valueForKey:[VVHelper keyToPropName:key]]];
            if (obj2) {
                       [pfObj setValue:[self pfObject:[obj valueForKey:[VVHelper keyToPropName:key]]] forKey:key];
            }else{
                
                id val=[VVHelper convertDictionaryToPF:[dict valueForKey:[VVHelper keyToPropName:key]]];
                if (val) {
                     [pfObj setValue:val forKeyPath:[VVHelper keyToPropName:key]];
                }
             
            }
     
            
        }else if([[dict valueForKey:key] isKindOfClass:[NSArray class]]){
            
            [pfObj setValue:[VVHelper convertArrayToPF:[dict valueForKey:key]] forKeyPath:[VVHelper keyToPropName:key]];
            
        }else if([[dict valueForKey:key] isKindOfClass:[UIImage class]]){
            
            PFFile * pf=[PFFile fileWithData:UIImageJPEGRepresentation([dict valueForKey:key], 0.9)];
            [pfObj setValue:pf forKey:[VVHelper keyToPropName: key ]];
        }else if ([[dict valueForKey:key] isKindOfClass:[VVFile class]]){
            VVFile * vf=[dict valueForKey:key];
            PFFile * pf=vf.file;
           [pfObj setValue:pf forKey:[VVHelper keyToPropName: key ]];
        }else if([[dict valueForKey:key] isKindOfClass:[NSNumber class]]){
            
            NSNumber * n=[dict valueForKey:key];
            if ([n boolValue]) {
                NSLog(@"this is bool");
            }
           
                         [pfObj setValue:[dict valueForKey:key]  forKey:[VVHelper keyToPropName: key ]];
           // [pfObj setValue:[[dict valueForKey:key] boolValue]  forKey:[VVHelper keyToPropName: key ]];
        }else {
              [pfObj setValue:[dict valueForKey:key]  forKey:[VVHelper keyToPropName: key ]];
//            BOOL s=[[dict valueForKey:key] boolValue];
//              [pfObj setValue:[NSString stringWithFormat:@"%hhd",s]  forKey:[VVHelper keyToPropName: key ]];
        }

        
        
        
    }
    [pfObj removeObjectForKey:[VVHelper keyToPropName:@"entityName"]];
    return pfObj;
    
    
}


#endif



-(void)addAditionalParam:(id)par forKey:(NSString*)key{
    
    if (!_additionalParams) {
        _additionalParams=[[NSMutableArray alloc]init];
    }
    if (par && key) {
        [_additionalParams addObject:[NSDictionary dictionaryWithObjectsAndKeys:par,key,nil]];
        
   
    }
 }

-(void)addLessParam:(NSNumber *)par forKey:(NSString *)key{
    
    [self.lessNumberParams addObject:[NSDictionary dictionaryWithObject:par forKey:key]];
    
}
-(void)addHightParam:(NSNumber *)par forKey:(NSString *)key{
     [self.highNumberParams addObject:[NSDictionary dictionaryWithObject:par forKey:key]];
}
-(void)addArrayMatchesParam:(NSMutableArray *)arr forKey:(NSString *)key{
    
    if (!_matchesArrParam) {
        _matchesArrParam=[[NSMutableArray alloc]init];
    }
    [_matchesArrParam addObject:[NSDictionary dictionaryWithObject:arr forKey:key]];
       
    
}
@end
