//
//  VVCollectionView.m
//  Chooos
//
//  Created by mm7 on 10.05.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "VVCollectionView.h"
@implementation VVCollectionViewButton



@end

@interface VVCollectionView(){
    NSUInteger columnsCount;
    NSUInteger lastIndex;
    
}

@end

@implementation VVCollectionView
-(id)initWithFrame:(CGRect)frame{
    if (self=[super initWithFrame:frame]) {
        
        lastIndex=0;
    }
    return self;
}
#pragma mark - tableViewDelegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    NSUInteger curRowsCount=[super tableView:tableView numberOfRowsInSection:section];
    columnsCount  = [self.vvCollectionViewDelegate numberOfColumnsInSection:section];
              VVListTableViewSection * sect=[self.director getSection:section];
   int realRowsCount= sect.cells.objects.count;
    

    
    int rest=(realRowsCount)%columnsCount;
    float totalRows=truncf(((float)realRowsCount)/columnsCount);
    [[self.director getSection:section] setColumnsCount:columnsCount];
    if (rest>0) {
      totalRows++;
             
        
    }
    
    
    if (realRowsCount<curRowsCount) {
        return totalRows+1;
    }else{
        return totalRows;
    }
    
    
    
}
-(void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    for (int a=0; a<100; a++) {
        
        VVColumnView *v=(VVColumnView*)[cell viewWithTag:9999+a ];
        if (v) {
            v.reused=YES;
        }else break;
    }
    
}
-(void)listTableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath withObject:(VVObject *)object{
    
    
    
    
}
-(void)didSelectItem:(VVCollectionViewButton*)but{
    
    [self.vvCollectionViewDelegate collectionView:self didSelectItemInColum:but.colum atIndexPath:but.indexPath withObject: but.Object];
}
#pragma mark -listTableViewDelegate
-(UITableViewCell *)listTableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath objectForRow:(id)object{
           VVListTableViewSection * sect=[self.director getSection:indexPath.section] ;

     NSString *CellIdentifer = [NSString stringWithFormat:@"colCell"];
	UITableViewCell  * cell=[tableView dequeueReusableCellWithIdentifier:CellIdentifer];
	if (!cell) {
        
		cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifer];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
       
        for (int a=0; a<columnsCount; a++) {
            
            id obj=  [self.director getObject:sect andRow:indexPath.row*columnsCount+a ];
     
            VVColumnView * columnView=[self.vvCollectionViewDelegate collectionView:self viewForColumn:a atIndexPath:indexPath andObject:obj];
            
            [columnView setFrame:CGRectMake(0+a*(self.frame.size.width/columnsCount), 0, (self.frame.size.width/columnsCount) ,[self listTableView:self heightForRowAtIndexPath:indexPath])];
            VVCollectionViewButton *b=[VVCollectionViewButton buttonWithType:UIButtonTypeCustom];
            b.indexPath=indexPath;
            b.Object=obj;
            b.colum=a;
            
            
            [b setFrame:columnView.bounds];
            
            
            [b addTarget:self action:@selector(didSelectItem:) forControlEvents:UIControlEventTouchUpInside];
            
            [columnView insertSubview:b atIndex:0];
            [cell addSubview:columnView];
        
            [b setTag:88888+a];
            [columnView setTag:9999+a];
            if (!self.columnsData) {
                self.columnsData=[[NSMutableArray alloc] init];
            }
            [self.columnsData addObject:columnView];
       
            [cell setBackgroundColor:[UIColor clearColor]];
        }
        
        
	}else{
        for (int a=0; a<columnsCount; a++) {
            id obj=  [self.director getObject:sect andRow:indexPath.row*columnsCount+a ];
           
            
            
            [self.vvCollectionViewDelegate collectionView:self viewForColumn:a atIndexPath:indexPath andObject:obj];
            VVCollectionViewButton  * b=(VVCollectionViewButton*)[[cell viewWithTag:9999+a]viewWithTag:88888+a];
            [b setObject:obj];
            [b setIndexPath:indexPath];
            [b setColum:a];
            
            
            
        }
      
        
    }
       //  [cell.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    lastIndex+=columnsCount+indexPath.row;
    
	return  cell;

    
    
    
}

-(CGFloat)listTableView:(UITableView *)tableView heightForLoadCellAtIndexPath:(NSIndexPath *)indexPath{
    
    
    return [self.vvCollectionViewDelegate collectionView:self heightForLoadCellAtIndexPath:indexPath ];
}
-(CGFloat)listTableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [self.vvCollectionViewDelegate collectionView:self heightForRowAtIndexPath:indexPath ];
}
-(CGFloat)listTableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return [self.vvCollectionViewDelegate collectionView:self heightForHeaderInSection:section];
}
-(UIView *)listTableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    
    return [self.vvCollectionViewDelegate collectionView:self viewForHeaderInSection:section];
}
-(UITableViewCell *)listTableView:(UITableView *)tableView loadingCellViewForRowAtIndexPath:(NSIndexPath *)indexPath{
  return   [self.vvCollectionViewDelegate collectionView:self loadingCellViewForRowAtIndexPath:indexPath];
}
-(void)listTableViewDidLoadData:(id)data forHeader:(NSUInteger)section{
    [self.vvCollectionViewDelegate collectionViewDidLoadData:data forHeader:section];
}
#pragma  mark -()

-(id)getColumnView:(NSString*)name{
      
    for (VVColumnView * cv in self.columnsData) {
        
        if ([cv.columnIdentifier isEqualToString:name]&& cv.reused==YES) {
            cv.reused=NO;
       
            return cv;
        }
    }
    
    return nil;
    
}

#pragma mark - vvCollectionViewDelegate


-(VVColumnView *)collectionView:(VVCollectionView *)collectionView viewForColumn:(NSUInteger)column atIndexPath:(NSIndexPath *)indexPath andObject:(id)object{
    NSString *CellIdentifer = [NSString stringWithFormat:@"colitem"];
	VVColumnView  * cell=[collectionView getColumnView:CellIdentifer];
	if (!cell) {
		cell=[[VVColumnView alloc]initWithFrame:CGRectMake(0, 0, 320, 64)];
        cell.columnIdentifier=CellIdentifer;
    }
	return  cell;
    
    
    
}
-(NSUInteger)numberOfColumnsInSection:(NSUInteger)section{
    
    
    return 2;
    
}
-(CGFloat)collectionView:(VVCollectionView *)collectionView heightForLoadCellAtIndexPath:(NSIndexPath *)indexPath{
    
    
    return 20;
}
-(CGFloat)collectionView:(VVCollectionView *)collectionView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 64;
}
-(CGFloat)collectionView:(VVCollectionView *)collectionView heightForHeaderInSection:(NSInteger)section{
    return 0;
}
-(void)collectionView:(VVCollectionView *)collectionView didSelectRowAtIndexPath:(NSIndexPath *)indexPath withObject:(VVObject *)object{
    
}
-(UIView *)collectionView:(VVCollectionView *)collectionView viewForHeaderInSection:(NSInteger)section{
    
    
    return nil;
}

-(UITableViewCell *)collectionView:(VVCollectionView *)collectionView loadingCellViewForRowAtIndexPath:(NSIndexPath *)indexPath{
	static NSString *CellIdentifer = @"loadingCell";
	
	UITableViewCell  * cell=[self dequeueReusableCellWithIdentifier:CellIdentifer];
	
	if (!cell) {
		cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifer];
        
        [cell.textLabel setTextAlignment:NSTextAlignmentCenter];
	}
    cell.textLabel.text=@"loading";
	return  cell;
}
-(void)collectionView:(VVCollectionView*)collectionView didSelectItemInColum:(NSUInteger)colum atIndexPath:(NSIndexPath*)indexPath withObject:(id)object{
    
}
-(void)collectionViewDidLoadData:(id)data forHeader:(NSUInteger)section{
    
     
    
}
@end
