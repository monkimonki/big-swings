//
//  VVStorage.h
//  FGApp
//
//  Created by mm7 on 15.05.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import <Foundation/Foundation.h>
@class VVFile;
@class PFObject;
@interface VVStorage : NSObject
+(VVStorage*)inst;


-(void)saveFile:(NSData *)fileData andFileName:(NSString*)fn;

-(NSData*)loadFileWithFileName:(NSString*)fileName;

-(NSData *)loadVVFile:(VVFile*)file;

-(void)saveVVFile:(VVFile*)file;
-(NSString*)getLocalName;


@property (retain,nonatomic)NSMutableArray * objectsData;

-(VVObject*)getObjectByID:(NSString*)objectId;


@property (retain,nonatomic)NSMutableArray * parseObjectsData;
-(id)getParseObjectById:(NSString*)objectId;
-(void)addParseObject:(PFObject*)po;
-(void)getAllItemsByClass:(Class)cl andValueParams:(NSDictionary*)dict mustHaveValForKey:(NSString*)reqKey andBlock:(void (^)(NSMutableArray *arr))block;
@end
