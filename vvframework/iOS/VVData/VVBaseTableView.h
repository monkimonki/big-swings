//
//  CVParseTableView.h
//  FGApp
//
//  Created by macmini9 on 4/18/14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VVBaseTableView : UITableView<UITableViewDataSource, UITableViewDelegate>


@end
