//
//  VVDataHost.m
//  FGApp
//
//  Created by macmini9 on 5/5/14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "VVDataHost.h"

@implementation VVDataHost

static VVDataHost * singleton;

+(VVDataHost *)inst{
	if (!singleton) {
		singleton = [self create];
	}
	return singleton;
}

+(VVDataHost *)create{
	return [[self alloc] init];
}

@end
