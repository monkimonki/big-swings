//
//  VVEntity.m
//  FGApp
//
//  Created by macmini9 on 4/30/14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "VVEntity.h"

@implementation VVEntity

+(VVEntity *)create
{
	return [[self alloc] init];
}

-(id)init{
	if (self=[super init]) {
		self.properties = [NSMutableDictionary dictionary];
        self.body=[[NSMutableDictionary alloc]init];
        self.headerFieldDict=[[NSMutableDictionary alloc]init];
	}
	return self;
}

-(VVEntity *)addObject:(id)object forKey:(id<NSCopying>)key
{
    

	[self.properties setObject:object forKey:key];
	return self;
}
-(VVEntity *)setEName:(NSString*)n{
    _name=n;
    return self;
}
-(VVEntity *)addEntity:(VVEntity *)entity
{
	[self.properties setObject:entity forKey:keyEntity];
	return self;
}
@end
