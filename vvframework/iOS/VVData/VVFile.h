//
//  VVFile.h
//  FGApp
//
//  Created by mm7 on 15.05.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VVFile : NSObject
-(NSDictionary*)formDictionary;
+(VVFile*)createWithDictionary:(NSDictionary*)dict;
+(PFFile*)createPFFile:(NSDictionary*)dict;
@property(retain,nonatomic)NSURL *  url;

+(id)createWithUrl:(NSURL*)url;
-(id)initWithUrl:(NSURL*)url;

@property (retain,nonatomic) NSData * data;
-(void)loadWithCallback:(VVDataCallback)cb;
#ifdef PARSE
+(id)createWithFile:(PFFile*)file;
-(id)initWithPFFile:(PFFile*)file;
@property (retain,nonatomic) PFFile * file;

#endif
-(NSString*)getLocalName;
@end
