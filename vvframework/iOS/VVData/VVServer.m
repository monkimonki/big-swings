//
//  VVServer.m
//  FGApp
//
//  Created by macmini9 on 4/30/14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "VVServer.h"
#import "VVStorage.h"

@implementation VVServer

static VVServer * singleton = nil;

+(VVServer *)inst{
	if (!singleton) {
		singleton = [self create];
	}
	return singleton;
}

+(VVServer *)create{
	return [[self alloc] init];
}
-(id)init{
    if (self=[super init]) {
        _serverRootAdress=@"http://Server_Root/";
        _localServerData=[[NSMutableDictionary alloc]init];
        
        [VVStorage inst];
        
    }
    return self;
}
-(void)loadObjects:(VVParametrsObject *)par callback:(VVArrayCallback)cb
{
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
	VVDownloader * dw = [VVDownloader createWithParObj:par];
	[dw load:^(BOOL success, VVDownloaderObject *obj, VVError *error) {
           dispatch_async(dispatch_get_main_queue(), ^{
		if (success) {
			
				cb(success,obj.innerObject);
			
		}
               
           });
	}];
        });
}
-(void)loadObject:(VVParametrsObject *)par callback:(VVObjectCallback)cb{
      dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    VVDownloader * downloader= [VVDownloader createWithParObj:par];
    
    [downloader load:^(BOOL suc,VVDownloaderObject*obj,VVError* err){
        dispatch_async(dispatch_get_main_queue(), ^{
        if (suc) {
            if ([obj.innerObject isKindOfClass:[NSMutableArray class]]) {
                	cb(suc,[obj.innerObject firstObject]);
            }else
    		cb(suc,obj.innerObject);
            
            
            
        }
        });
    }];
          
      });

}
-(void)doAction:(VVParametrsObject*)actionParameters  callback:(VVObjectCallback)cb{
      dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    VVDownloader * dw = [VVDownloader createWithParObj:actionParameters];
          
	[dw load:^(BOOL success, VVDownloaderObject *obj, VVError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{

		if (success) {
			
            cb(success,obj.innerObject);
			
		}else {
            cb(success,nil);
        }
        });
	}];
      });
}


-(void)loadFile:(VVFile *)file callback:(VVDataCallback)cb{
         dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    VVDownloader * downloader=[VVDownloader createWithVVFile:file];
    
    
    [downloader load:^(BOOL success, VVDownloaderObject *obj, VVError *error){
        dispatch_async(dispatch_get_main_queue(), ^{

        if (success) {
            cb(YES,nil,obj.innerObject);
        }else{
              cb(NO,nil,nil);
        }
        });
        
    }];
         });
}
-(void)getObject:(VVObject*)obj callback:(VVObjectCallback)cb{
    
    VVParametrsObject * parObj=[VVParametrsObject createWithEntity:obj.getEmptyObjectEntity];
 
    VVDownloader * downloader= [VVDownloader createWithParObj:parObj];
    
    [downloader load:^(BOOL suc,VVDownloaderObject*obj,VVError* err){
        
    }];
    
    
}
-(void)addDataToLocalServer:(id)obj forKey:(NSString*)key{
    
    if (![_localServerData valueForKey:key]) {
        [_localServerData setValue:[[NSMutableArray alloc]init] forKey:key];
    }
    
    
    [[_localServerData valueForKey:key]addObject:obj];
    
    
    
    
    
}
-(void)addDataToLocalServer:(id)obj atIndex:(int)index forKey:(NSString*)key{
    if (![_localServerData valueForKey:key]) {
        [_localServerData setValue:[[NSMutableArray alloc]init] forKey:key];
    }
    
     [[_localServerData valueForKey:key]insertObject:obj  atIndex:index];
}

-(void)addResponseDataModel:(id)dm{
    if(!_responseDataModel){
        _responseDataModel=[[NSMutableArray alloc]init];
    }
    [_responseDataModel addObject:dm];
    

}

-(void)searchInStorage:(NSString*)searchString andKey:(NSString*)searchKey andClass:(Class)searchClass valueParams:(NSDictionary *)dict andBlock:(void(^)(NSArray * arr))block{
    
    
    [[VVStorage inst]getAllItemsByClass:searchClass andValueParams:dict mustHaveValForKey:searchKey  andBlock:^(NSMutableArray *arr) {
       
        if (arr.count>0) {
            
            
            if (searchString.length==0) {
                block(arr);
            }else{
                
             
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.%@ contains[c] %@" ,searchKey, searchString]; // if you need case sensitive search avoid '[c]' in the predicate
            
            NSArray *results = [arr filteredArrayUsingPredicate:predicate];
                
                block(results);
            }

            
            
        }else{
            block(nil);
        }
        
        
    }];
    
}
@end
