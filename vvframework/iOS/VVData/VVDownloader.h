//
//  VVDownloader.h
//  FGApp
//
//  Created by macmini9 on 4/30/14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "VVDownloaderObject.h"
#import "VVError.h"
#import "VVCallbacks.h"
#import "VVServer.h"
@class VVFile;
@class VVParametrsObject;
@class VVObject;
@interface VVDownloader : NSObject

+(VVDownloader *)createWithParObj:(VVParametrsObject *)obj;

+(VVDownloader *)createWithVVFile:(VVFile *)file;

-(id)initWithParObj: (VVParametrsObject *)obj;
-(id)initWithVVFile:(VVFile*)file;

-(void)load:(VVDownloaderCallback) cb;

@property (nonatomic, retain) VVParametrsObject * parObj;
@property (nonatomic, retain) VVFile * fileToLoad;

-(id)doLocalAction:(NSArray*)paramsArr;
-(void)doLocalDelete:(NSArray *)paramsArr;


//-(VVObject*)getTempObject:(NSArray* )paramsArr;


//


@end
