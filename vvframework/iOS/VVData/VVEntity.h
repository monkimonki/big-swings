//
//  VVEntity.h
//  FGApp
//
//  Created by macmini9 on 4/30/14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VVEntity : NSObject
{
	NSString * keyEntity;
    
      
    

}

+(VVEntity*) create; 
@property (retain,nonatomic)    NSString * name;
@property (nonatomic, retain)   NSMutableDictionary * properties;
@property (nonatomic,retain)    NSMutableDictionary * body;
@property (nonatomic,retain) NSMutableDictionary * headerFieldDict;
-(VVEntity *)setEName:(NSString*)n;
-(VVEntity*) addObject:(id)object forKey:(id<NSCopying>)key;
-(VVEntity*) addEntity:(VVEntity *)entity;


@end
