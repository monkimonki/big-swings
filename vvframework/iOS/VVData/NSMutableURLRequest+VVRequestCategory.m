//
//  NSMutableURLRequest+VVRequestCategory.m
//  ChoooseData
//
//  Created by Aleksandr Padalko on 5/1/14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "NSMutableURLRequest+VVRequestCategory.h"
#import "VVHelper.h"

@implementation NSMutableURLRequest (VVRequestCategory)
-(void)setHTTPBodyAsDictionary:(NSDictionary*)dict{

    [self setHTTPBody:[VVHelper formEncodedDataDictionary:dict]];
}
-(NSString *)getStringFromHTTPBody{

  
NSString *myString = [[NSString alloc] initWithData:self.HTTPBody encoding:NSUTF8StringEncoding];
       return  myString;
    
}

-(NSDictionary *)getDictionaryFromHTTPBody{

    NSArray * arr=[[self getStringFromHTTPBody] componentsSeparatedByString:@"&"];
   
    NSMutableDictionary * dict=[[NSMutableDictionary alloc]init];
    NSMutableArray * tempArr=[[NSMutableArray alloc]init];
    for (int a=0; a<arr.count; a++) {
      
        NSArray * str=[[arr objectAtIndex:a] componentsSeparatedByString:@"="];
        if (str.count>1) {
            
    
        [tempArr addObject:[str objectAtIndex:0]];
                [tempArr addObject:[str objectAtIndex:1]];
        
            [dict setValue:[tempArr objectAtIndex:1] forKey:[tempArr objectAtIndex:0]];
            [tempArr removeAllObjects];
       
            }
        
        
    }
    
    return dict;
    
    
    
}
@end
