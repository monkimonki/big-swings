//
//  CObject.h
//  ChoooseData
//
//  Created by mm7 on 30.04.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VVEntity.h"
#import "VVParametrsObject.h"
#import <objc/runtime.h>
@interface VVObject : NSObject
@property (retain,nonatomic)NSString * objectId;
@property(retain,nonatomic)NSString * entityName;
//+(NSString*)getEntity;
-(VVEntity*)getEmptyObjectEntity;
+(VVEntity*)getCollectionEntity;
+(VVEntity *)getCollectionEntity:(VVObject*)obj;

-(VVParametrsObject*)getLoadObjectParametrs;

+(VVParametrsObject*)getLoadCollectinParametrs;
+(VVParametrsObject *)getLoadCollectinParametrs:(VVObject*)obj;
+(VVParametrsObject*)getLoadCollectinParametrs:(int)origin andLenght:(int)lenght;
+(VVParametrsObject*)getLoadCollectinParametrs:(VVObject*)obj andOrigin:(int)origin andLenght:(int)lenght;

+(NSString *)getClassEntity;

-(id)initWithDictionary:(NSDictionary*)dict;
-(void)setWithDictionary:(NSDictionary*)dict;

-(NSMutableDictionary*)logProp;
-(VVParametrsObject *)saveVVObject;

-(VVParametrsObject*)deleteVVObject;
-(void)addValForKey:(NSString*)key forKey:(NSString*)realKey toCDict:(NSMutableDictionary*)dict;
@end
