//
//  VVFile.m
//  FGApp
//
//  Created by mm7 on 15.05.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "VVFile.h"

@implementation VVFile
-(NSDictionary*)formDictionary{
    
    NSMutableDictionary * dict=[[NSMutableDictionary alloc]init];
    if (self.file&&self.file.url) {
     
        [dict setValue:self.file.url forKeyPath:@"url"];
        [dict setValue:self.file.name forKeyPath:@"name"];
    
     
            [dict setValue:@"File" forKey:@"__type"];
    
    }else if(self.url){
        [dict setValue:[self.url absoluteString] forKey:@"url"];
        [dict setValue:@"url" forKeyPath:@"type"];
    }
    if (dict.allKeys.count==0){
        if (self.data) {
            [dict setValue:self.data forKeyPath:@"data"];
        }else{
            return nil;
        }
       
    }
    return dict;
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict
                                                       options:0
                                                         error:nil];
    
 
        
        NSString *JSONString = [[NSString alloc] initWithBytes:[jsonData bytes] length:[jsonData length] encoding:NSUTF8StringEncoding];
    NSLog(@"%@",JSONString);
    return JSONString;
    
    
    return @"{\"__type\":\"File\",\"name\":\"tfss-941b7309-2f6b-4af7-b8cd-be345ab4f945-photo.jpg\",\"url\":\"http://files.parsetfss.com/a6751d32-8dae-47a1-b10e-96b4a0e251c2/tfss-941b7309-2f6b-4af7-b8cd-be345ab4f945-photo.jpg\"}";
    
}
+(VVFile*)createWithDictionary:(NSDictionary*)dict{
    VVFile * file;
    if ([[dict valueForKey:@"__type"] isEqualToString:@"File"]) {
    file=[VVFile createWithFile: [self createPFFile:dict]];
        
    }
    return file;
 
}
+(PFFile*)createPFFile:(NSDictionary*)dict{
    PFFile * f=[[PFFile alloc]init];
    f.url=[dict valueForKey:@"url"];
    f.name=[dict valueForKey:@"name"];
    
    return f;
}
+(id)createWithUrl:(NSURL*)url{
    
    return  [[self alloc]initWithUrl:url];
    
}
-(id)initWithUrl:(NSURL*)url{
    
    if (self=[super init]) {
        _url=url;
    }
    return self;
    
}
-(NSString*)getLocalName{
    
    NSString *myString = [_url absoluteString];
    myString= [myString stringByReplacingOccurrencesOfString:@"/" withString:@""];
    
    return [myString stringByReplacingOccurrencesOfString:@":" withString:@""];
    
}
#ifdef PARSE
+(id)createWithFile:(PFFile*)file{
    
    
    return  [[self alloc]initWithPFFile:file];
    
}
-(id)initWithPFFile:(PFFile*)file{
    if (self=[super init]) {
        _file=file;
    }
    return self;
}
#endif


-(void)loadWithCallback:(VVDataCallback)cb{
    
    if(_url){
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:_url];

        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response, NSData *body, NSError *requestError) {
                              
                                   if (body) {
                                       _data=body;
                                       cb (YES,nil,body);
                                   }else{
                                       cb (NO,nil,nil);
                                   }
                                   
                                   
                               }];

    }else{
#ifdef PARSE
        
        
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:_file.url]];
        
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response, NSData *body, NSError *requestError) {
                                   
                                   if (body) {
                                       _data=body;
                                       cb (YES,nil,body);
                                   }else{
                                       cb (NO,nil,nil);
                                   }
                                   
                                   
                               }];
        return;
        
        [_file getDataInBackgroundWithBlock:^(NSData *data, NSError *err) {
            if (!err) {
                _data=data;
                return cb(YES,nil,data);
                
            }else {
                return cb(NO,nil,data);
            }
        }];
#endif
    }
    
    
}


@end
