//
//  VVDownloader.m
//  FGApp
//
//  Created by macmini9 on 4/30/14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "VVDownloader.h"
#import "VVStorage.h"

@implementation VVDownloader

+(VVDownloader *)createWithParObj:(VVParametrsObject *)obj{
	return [[self alloc] initWithParObj:obj];
}

-(id)initWithParObj:(VVParametrsObject *)obj {
	if (self=[super init]) {
		_parObj = obj;
	}
	return self;
}


+(VVDownloader *)createWithVVFile:(VVFile *)file{
    	return [[self alloc] initWithVVFile:file];
}
-(id)initWithVVFile:(VVFile*)file{
    if (self=[super init]) {
        _fileToLoad=file;
	}
	return self;
}


-(void)load:(VVDownloaderCallback) cb{
    int k=0;
//    while (k<100000000) {
//        k++;
//    }
//   
   
    
    if (_fileToLoad) {
 
        
     _fileToLoad.data=    [[VVStorage inst]loadVVFile:_fileToLoad];
        
        if (_fileToLoad.data) {
            VVDownloaderObject * dObj=[VVDownloaderObject createWithInnerObject:_fileToLoad.data];
            cb(YES,dObj,nil);
            return;
        }
        
        if ([VVServer inst].dataType==kVVServerDataTypeLocal) {
//             [NSThread sleepForTimeInterval:1];
            NSData *data=[[NSData alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:[_fileToLoad getLocalName] ofType:@""]];
           
            if (data) {
                 _fileToLoad.data=data;
      
            [[VVStorage inst] saveVVFile:_fileToLoad];
                           VVDownloaderObject * dObj=[VVDownloaderObject createWithInnerObject:data];
            cb(YES,dObj,nil);
            }else{
              cb(NO,nil,nil);
            }
            
            
        }else{
        
        [_fileToLoad loadWithCallback:^(BOOL suc,VVError * err,NSData * data){
            
            
            if (suc&&data) {
                VVDownloaderObject * dObj=[VVDownloaderObject createWithInnerObject:data];
                [[VVStorage inst] saveVVFile:_fileToLoad];
                    cb(YES,dObj,nil);
                
            }else{
                
                cb(NO,nil,nil);
            }
            
            
        }];
        }
      
    }else{
//        if ([VVServer inst].dataType==kVVServerDataTypeLocal) {
//            [NSThread sleepForTimeInterval:1];
//        }
	if ([VVServer inst].dataType == kVVServerDataTypeParse) {
#ifdef PARSE
	
        if (_parObj.objectToSave) {
        [self saveViaParse:[_parObj pfObject:_parObj.objectToSave] callback:cb];
        }else if (_parObj.objectToDelete){
            [self deleteViaParse:[_parObj pfObject:_parObj.objectToDelete] callback:cb];
        }
        else{
           	[self loadViaParse:[_parObj query] callback:^(BOOL success,id result,VVError * error){
                if (success) {
                    
                    id newResult=  [self scanResult:result];
                    
                    cb(YES,[VVDownloaderObject createWithInnerObject:newResult],nil);
                    
                }else{
                    cb(NO,nil,nil);
                }
            }];
        }
#endif
	}else if([VVServer inst].dataType == kVVServerDataTypeCUrl||[VVServer inst].dataType == kVVServerDataTypeLocal){
        
        [self loadViaUrl:[_parObj urlRequest] callback:^(BOOL success,id result,VVError * error){
        
        
            if (success) {

              id newResult=  [self scanResult:result];
           
                cb(YES,[VVDownloaderObject createWithInnerObject:newResult],nil);
                
            }else{
                cb(NO,nil,nil);
            }
        
        } ] ;
        
    }
        
    }
}
-(id)scanResult:(id)result{
    
    if ([result isKindOfClass:[NSDictionary class]]) {
        if ([result valueForKey:[VVHelper propNameToKey:@"entityName"]]) {
            id r=   [self configurateResult:result];
            
            return r;
   
        }else{
            id r=   [self configurateResult:[self scanResultWithDataModel:result]];
            
            return r;
        }
        
    }else{
        if ([[result firstObject] valueForKey:[VVHelper propNameToKey:@"entityName"]]) {
            
            id r=   [self configurateResult:result];
            
            return r;

        }else {
            
         id r=   [self configurateResult:[self scanResultWithDataModel:result]];
            
            return r;
  
        }
        
    }
    return  nil;
}
-(id)configurateResult:(id)result{
    id realResult=result;
    if ([realResult isKindOfClass:[NSArray class]]) {
        NSMutableArray * returnArray=[[NSMutableArray alloc]init];
        for (NSDictionary * singleObject in realResult) {
            VVObject * object=[VVObject objectFromDictionary:singleObject];
            [returnArray addObject:object];
        }
        
        return returnArray;
    }else{
        
        VVObject * object=[VVObject objectFromDictionary:result];
      
        return object;

    }
    
    
    return nil;
}
-(id)scanResultWithDataModel:(id)result{
    
    for (id obj in [VVServer inst].responseDataModel) {
        
       id retObj= [self scanResult:result withSingleDataModel:obj];
        if (retObj) {
            return retObj;
        }
        
    }
    return nil;
    
}
-(id)scanResult:(id)result withSingleDataModel:(id)dm{
    
    if ([result isKindOfClass:[dm class]]) {
        
    
        if ([dm isKindOfClass:[NSDictionary class]]) {
                         if ([[dm allKeys] count]==0) {
                return result;
            }else
          return   [self scanResult:[result valueForKey:[[dm allKeys] firstObject]] withSingleDataModel:[dm valueForKey:[[dm allKeys] firstObject]]];
            
        }else{
            
            return result;
            
        }

        
    }else{
        return nil;
    }
    
}

-(void)loadViaUrl:(VVMutableURLRequest*)req callback:(void (^)(BOOL success,NSMutableDictionary* result,VVError*error))cb{
    
    if([VVServer inst].dataType == kVVServerDataTypeLocal){
        
      
        
        NSString * str=[NSString stringWithFormat:@"%@",req.URL];
        
        NSString * str2=[str stringByReplacingOccurrencesOfString:[VVServer inst].serverRootAdress withString:@""];
   
        
        NSMutableArray * paramsArr=[[NSMutableArray alloc]initWithArray:[str2 componentsSeparatedByString:@"/"]];
        
        NSDictionary* bodyDic=[req getDictionaryFromHTTPBody];
        NSString * strHideKeys=[bodyDic valueForKey:@"zdebug_hide_key"];
        NSArray * hideKeysArr;
        if (strHideKeys) {
            
               strHideKeys=[strHideKeys stringByReplacingOccurrencesOfString:@" " withString:@""];
               strHideKeys=[strHideKeys stringByReplacingOccurrencesOfString:@"\n" withString:@""];
               strHideKeys=[strHideKeys stringByReplacingOccurrencesOfString:@"(" withString:@""];
               strHideKeys=[strHideKeys stringByReplacingOccurrencesOfString:@")" withString:@""];
               hideKeysArr=[strHideKeys componentsSeparatedByString:@","];
        }
        NSString * strSearchKeys=[bodyDic valueForKey:@"zdebug_search_key"];
        NSArray * searchKeysArr;
        if (strSearchKeys) {
            
            strSearchKeys=[strSearchKeys stringByReplacingOccurrencesOfString:@" " withString:@""];
            strSearchKeys=[strSearchKeys stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            strSearchKeys=[strSearchKeys stringByReplacingOccurrencesOfString:@"(" withString:@""];
            strSearchKeys=[strSearchKeys stringByReplacingOccurrencesOfString:@")" withString:@""];
            searchKeysArr=[strSearchKeys componentsSeparatedByString:@","];
        }
        NSString * strShowKeys=[bodyDic valueForKey:@"zdebug_show_key"];
        NSArray * showhKeysArr;
        if (strShowKeys) {
            
            strShowKeys=[strShowKeys stringByReplacingOccurrencesOfString:@" " withString:@""];
            strShowKeys=[strShowKeys stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            strShowKeys=[strShowKeys stringByReplacingOccurrencesOfString:@"(" withString:@""];
            strShowKeys=[strShowKeys stringByReplacingOccurrencesOfString:@")" withString:@""];
            showhKeysArr=[strShowKeys componentsSeparatedByString:@","];
        }
        
       
        NSInteger limit=1000;
        NSInteger offset=0;
        if ([bodyDic valueForKey:@"limit"]) {
            limit=[[bodyDic valueForKey:@"limit"] integerValue];
            
        }
        
        if ([bodyDic valueForKey:@"offset"]) {
            offset=[[bodyDic valueForKey:@"offset"]
            integerValue];
            
        }
        
        
  
        
        NSMutableDictionary * resultDic=[[NSMutableDictionary alloc]init];
        
        NSMutableDictionary * dataDict=[[NSMutableDictionary alloc]init];
        
        
        //[dataDict setValue:[req getDataType] forKey:@"type"];
        
        if ([req.HTTPMethod isEqualToString:@"GET"]) {
            


            if (req.returnigType==kVVReturningTypeList) {
            
                NSMutableArray * resultFromBd=  [self getCollectionFromLocalBD:paramsArr parBody:bodyDic andSKeys:searchKeysArr andShowKeysArr:showhKeysArr andHideKeysArr:hideKeysArr andOffset:offset andLimit:limit];
           
                
                
                NSMutableArray * reslArr=[self doAditionalSetupToList : resultFromBd andName:req.endPointName];
            
                [dataDict setValue:reslArr forKey:[req getDataType]];
           //     [dataDict setValue:listArr forKey:[req getDataType]];
                
            
          
            }else{
            
            
                NSMutableDictionary* objDict= [self getObjectFromLocalBD:paramsArr andHideKeys:hideKeysArr andEndpointName:req.endPointName];

                objDict=[self doAditionalSetupToObject:objDict andName:req.endPointName];
                
                
                [dataDict setValue:objDict forKey:[req getDataType]];
            
        //    }
   
            
            }
            
            
        }else{
              if ([req.HTTPMethod isEqualToString:@"POST"]) {
               
                  
                  
          [dataDict setValue:[self doLocalAction:paramsArr] forKey:@"object"];
              }else if ([req.HTTPMethod isEqualToString:@"DELETE"]){
                  
                  [self doLocalDelete:paramsArr];
                  
                                         }
            
            [dataDict setValue:@"YES" forKey:@"success"];


            
        }
    
        [resultDic setValue:dataDict forKey:@"data"];
        

        
        
        VVLog(@"-------Result------\n\n%@",resultDic);
        
        
//        VVDownloaderObject * downObj=[VVDownloaderObject createWithInnerObject:resultDic];
//        if (req.returnigType==kVVReturningTypeList) {
//            downObj.type=kVVDOTypeList;
//        }else{
//            
//            if (_parObj.returningObjectClass) {
//                
//                NSMutableDictionary  *dict= [dataDict valueForKey:@"object"];
//                
//              downObj.innerObject=[[_parObj.returningObjectClass alloc]initWithDictionary:dict];
//                
//                
//            }
//            downObj.type=kVVDOTypeObject;
//        }
//        
        
        cb(YES,resultDic,nil);
        
        
    }else {
        
        
        
        [NSURLConnection sendAsynchronousRequest:req queue:[NSOperationQueue currentQueue ] completionHandler:^(NSURLResponse * resp,NSData * data,NSError * erorr){
            
     
            if (data) {
                NSMutableDictionary * resultDic=[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
//                VVDownloaderObject * downObj=[VVDownloaderObject createWithInnerObject:resultDic];
//                if (req.returnigType==kVVReturningTypeList) {
//                    downObj.type=kVVDOTypeList;
//                }else{
//                    downObj.type=kVVDOTypeObject;
//                }
                
                cb(YES,resultDic,nil);
            }else{
                cb(NO,nil,nil);
            }
        
        
        }];
        
    }

    
}


-(void)doLocalDelete:(NSArray *)paramsArr{
    
    
}
-(NSMutableDictionary*)doAditionalSetupToObject:(NSMutableDictionary*)objDict andName:(NSString*)objName{
    
    
    return objDict;
}

-(NSMutableArray*)doAditionalSetupToList:(NSMutableArray*)list andName:(NSString*)objName{
    
    
    return list;
}

-(NSMutableDictionary*)getObjectFromLocalBD:(NSMutableArray *)paramsArr andHideKeys:(NSArray*)hideKeysArr andEndpointName:(NSString*)name{
    if (paramsArr.count>1) {
        
        
        NSString * last=[paramsArr objectAtIndex:paramsArr.count-1];
        NSString * prelast=[paramsArr objectAtIndex:paramsArr.count-2];
        for (VVObject * obj in [[VVServer inst].localServerData valueForKey:prelast]) {
            if ([obj.objectId isEqualToString:last]){
                
                NSMutableDictionary * dict=obj.logProp;
                for (NSMutableDictionary *dd in dict) {
                    for (NSString * s in hideKeysArr) {
                        [dd removeObjectForKey:s];
                    }
                }
                
                return dict;
                break;
            }
            
        }
    }
    
    return [[NSMutableDictionary alloc]init];
}
-(NSMutableArray*)getCollectionFromLocalBD:(NSMutableArray *)paramsArr parBody:(NSDictionary*)body andSKeys:(NSArray*)searchKeysArr andShowKeysArr:(NSArray*)showhKeysArr andHideKeysArr:(NSArray*)hideKeysArr andOffset:(float)offset andLimit:(float)limit {
    VVObject * tempObject;
    NSMutableArray * listArr=[[NSMutableArray alloc]init];
    if (paramsArr.count>1) {
        
        NSArray * tempArr;
        
        for (int z=0; z<paramsArr.count; z++) {
            if (z%2==0) {
                tempArr=[[VVServer inst].localServerData valueForKey:[paramsArr objectAtIndex:z]];
            }else{
                for (VVObject * obj in tempArr) {
                    if ([obj.objectId isEqualToString:[paramsArr objectAtIndex:z]]) {
                        tempObject=obj;
                        break;
                    }
                    
                }
            }
            
            
        }
        
    }
    NSMutableArray * searchedArray=[[NSMutableArray alloc]init];
    NSArray * firstEntityArr=[[VVServer inst].localServerData valueForKey:[paramsArr lastObject]];
    
  
    if (tempObject) {
        
        
        for (int a=0 ;a<firstEntityArr.count;a++) {
           
            
            NSMutableDictionary * d=[[firstEntityArr objectAtIndex:a] logProp];
            
            BOOL isAdd=NO;
            if (searchKeysArr.count>0 && [d valueForKey:[searchKeysArr firstObject]]) {
                if([[[d valueForKey:[searchKeysArr firstObject]] valueForKey:@"id"] isEqualToString:tempObject.objectId]){
                    [d removeObjectForKey:[searchKeysArr firstObject]];
                    isAdd=YES;
                }
                
            }
            else{
                for (NSString* key in d) {
                    
                    if ([[d valueForKey:key]isKindOfClass:[NSDictionary class]]) {
                        
                        NSDictionary *d2=[d valueForKey:key ];
                        if ([[d2 valueForKey: @"id"]isEqualToString:tempObject.objectId]) {
                            
                            
                            [d removeObjectForKey:key];
                            isAdd=YES;
                            break;
                        }
                    }else if([key isEqualToString:@"id"]&&[[d valueForKey:key] isEqualToString:tempObject.objectId])
                        isAdd=YES;
                    
                }
                
            }
            if (isAdd) {
                
                if (showhKeysArr.count>0&&[d valueForKey:[showhKeysArr firstObject]]) {
                    
                    [searchedArray addObject:[d valueForKey:[showhKeysArr firstObject]]];
                }else
                    [searchedArray addObject:d];
            }
            
        }
        NSMutableArray * copyListArray=[[NSMutableArray alloc]initWithArray:searchedArray];

        if ([body valueForKey:@"objects"]) {
                      NSMutableArray * tempDataArr=[[NSMutableArray alloc]init];
            
            
            for (NSDictionary* dic in searchedArray) {
                
                
                BOOL isOk=NO;
                for (VVObject * obj in [[VVServer inst].localServerData objectForKey:[body valueForKey:@"objects"]]) {
                    // [tempDataArr addObject:[obj logProp]];
                    
                    if ([obj.objectId isEqualToString:[dic valueForKey:[VVHelper propNameToKey:@"objectId"]]]) {
                        isOk=YES;
                        break;
                    }
                    
                }
                
                if (!isOk) {
                    [copyListArray removeObject:dic];
                }
                
            }
            
            
            
        }
        
        
        offset=MIN(copyListArray.count, offset);
        limit=MIN(offset+limit, copyListArray.count);
        //  [paramsArr removeObjectAtIndex:0];
        
        
        for (NSInteger a=offset; a<limit; a++) {
            //  VVLog(@"%@",[[firstEntityArr objectAtIndex:a] logProp]);
            [listArr addObject:[copyListArray objectAtIndex:a]];
            
        }
        
        
    }else{
        
        
        offset=MIN(firstEntityArr.count, offset);
        limit=MIN(offset+limit, firstEntityArr.count);
        
        
        for (NSInteger a=offset; a<limit; a++) {
            [listArr addObject:[[firstEntityArr objectAtIndex:a] logProp]];
        }
        

        
  
    }
    
    for (NSMutableDictionary *dd in listArr) {
        for (NSString * s in hideKeysArr) {
            [dd removeObjectForKey:[s stringByReplacingOccurrencesOfString:@"\"" withString:@""]];
        }
    }
    
  
  
    return listArr;
}

-(VVObject*)getTempObject:(NSArray* )paramsArr{
    
    return nil;
}

#ifdef PARSE
-(void) loadViaParse:(PFQuery*)query callback:(VVDownloaderCallback) cb {
   [query orderByDescending:@"createdAt"];

	[query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
		BOOL success = !error;
		NSMutableDictionary * resultDict=[NSMutableDictionary dictionary];
        NSMutableDictionary * dataDict=[NSMutableDictionary dictionary];
        NSMutableArray * realArray=[[NSMutableArray alloc]init];
        
      
        for (PFObject * obj in objects) {
        
            NSMutableDictionary* dict=[self dictFromPFObject:obj];
         //  VVObject* vvObject= [VVObject objectFromDictionary:dict];
            [realArray addObject:dict];
            
        }
        
//		[dataDict setValue: (success?@"YES":@"NO") forKey:@"success"];
//		[dataDict setValue:realArray forKey:@"list"];
//		[resultDict setValue:dataDict forKey:@"data"];
//        NSLog(@"%@",resultDict);
		VVDownloaderObject * obj = [VVDownloaderObject createWithInnerObject:resultDict];
		cb(success, realArray, nil);
	}];
}

-(void)checkObjects:(PFObject*)obj{
    
     for (NSString* k in obj.allKeys) {
             if ([[obj valueForKey:k] isKindOfClass:[PFUser class]]) {
                 PFUser * u=[obj valueForKey:k];

                 if (![u.objectId isEqualToString:[[PFUser currentUser] objectId]]) {
                     
                    PFUser * u2=[[VVStorage inst] getParseObjectById:u.objectId];
                     
                     [obj setObject:u2 forKey:k];
                     
                 }
             }else if ([[obj valueForKey:k] isKindOfClass:[PFObject class]]){
                 
                 [self checkObjects:[obj valueForKey:k]];
             }
     }
}
-(void) saveViaParse:(PFObject*)obj callback:(VVDownloaderCallback) cb {
    
//    NSLog(@"%@",obj);
//    NSLog(@"%@",[[obj valueForKey:@"product"] valueForKey:@"shop"]);
//    PFUser * u=[[obj valueForKey:@"product"] valueForKey:@"shop"];
//    PFUser * u2=[PFUser user];
//    u2.objectId=u.objectId;
//    [u2 fetch];
//    NSLog(@"%@",u2);
//
//    NSLog(@"%@",obj.allKeys);
//    
//    for (NSString* k in obj.allKeys) {
//        
//        if ([[obj valueForKey:k] isKindOfClass:[PFUser class]]) {
//            PFUser * u=[obj valueForKey:k];
//            if (![u.objectId isEqualToString:[PFUser currentUser].objectId]) {
//                PFUser * newU=[PFUser user];
//                newU.objectId=u.objectId;
//                [newU fetch];
//                NSLog( @"%@",newU);
//                    NSLog(@"%@",[obj valueForKey:@"shop"]);
//                [obj setValue:newU forKey:k];
//            }
//        }
//        
//    }
    
  
    [self checkObjects:obj];
    NSLog(@"%@",[[obj valueForKey:@"product"] valueForKey:@"shop"]);
	[obj saveInBackgroundWithBlock:^(BOOL success,NSError * err) {
		BOOL suc = !err;
		      
        VVObject * nObj=[VVObject objectFromDictionary:[self dictFromPFObject:obj]];
        NSLog(@"%@",[nObj logProp]);
		VVDownloaderObject * dObj = [VVDownloaderObject createWithInnerObject:nObj];
		cb(success, dObj, nil);
	}];
}
-(void) deleteViaParse:(PFObject*)obj callback:(VVDownloaderCallback) cb {
    
    NSLog(@"%@",obj);
    PFObject * obj2=[PFObject objectWithoutDataWithClassName:obj.parseClassName objectId:obj.objectId];
    obj2.objectId=obj.objectId;
      NSLog(@"%@",obj2);
	[obj2 deleteInBackgroundWithBlock:^(BOOL success,NSError * err) {
//		BOOL suc = !err;
//        
//        VVObject * nObj=[VVObject objectFromDictionary:[self dictFromPFObject:obj]];
//        NSLog(@"%@",[nObj logProp]);
//		VVDownloaderObject * dObj = [VVDownloaderObject createWithInnerObject:nObj];
		cb(success, nil, nil);
	}];
}
-(NSMutableDictionary*)dictFromPFObject:(PFObject*)obj{
    NSMutableDictionary* dict=[[NSMutableDictionary alloc]init];
    [[VVStorage inst]addParseObject:obj];
    

    
    
    NSLog(@"%@",obj.allKeys);
    
    for (NSString* key in obj.allKeys) {
        id val=[obj valueForKey:key];
        if ([val isKindOfClass:[PFObject class]]) {
         val=   [self dictFromPFObject:[obj valueForKey:key]];
        }else if ([val isKindOfClass:[PFFile class]]){
            
            val=[[VVFile alloc]initWithPFFile:val];
           
        }else if ([val isKindOfClass:[NSArray class]]){
          val=  [self convertArray:val];
        }else if([val isKindOfClass:[NSDictionary class]]){
            
           val= [self convertDictionary:val];
            
            }
        
        [dict setValue:val forKey:key];
    }
      [dict setValue:obj.parseClassName forKey:[VVHelper propNameToKey:@"entityName"]];
    [dict setValue:obj.createdAt forKey:@"createdAt"];
    [dict setValue:obj.objectId forKey:[VVHelper propNameToKey:@"objectId"]];
    return dict;

}
-(NSArray*)convertArray:(NSArray*)arr{
    NSMutableArray * newArr=[[NSMutableArray alloc]init];
    for (id obj in arr) {
        if ([obj isKindOfClass:[NSDictionary class]]) {
            [newArr addObject:  [self convertDictionary:obj]];
        }else if([obj isKindOfClass:[NSArray class]]){
            [newArr addObject:[self convertArray:obj]];
        }else if([obj isKindOfClass:[PFFile class]]){
         [newArr addObject:[[VVFile createWithFile:obj] formDictionary]];
        }else{
            [newArr addObject:obj];
        }
        
    }
    
    return newArr;
}
-(NSDictionary*)convertDictionary:(NSDictionary*)arr{
    NSMutableDictionary * newDict=[[NSMutableDictionary alloc] init];
    for (NSString * key in arr) {
        id obj=[arr valueForKey:key];
        if ([obj isKindOfClass:[NSDictionary class]]) {
            
            [newDict setObject:[self convertDictionary:obj] forKey:key];
       
        }else if([obj isKindOfClass:[NSArray class]]){
          [newDict setObject:[self convertArray:obj] forKey:key];
        }else if([obj isKindOfClass:[PFFile class]]){
            [newDict setObject:[[VVFile createWithFile:obj] formDictionary] forKey:key];
        }else{
            [newDict setObject:obj forKey:key];
        }
        
    }
    
    return newDict;
    
}
#endif
@end
