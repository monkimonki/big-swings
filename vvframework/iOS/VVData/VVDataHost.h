//
//  VVDataHost.h
//  FGApp
//
//  Created by macmini9 on 5/5/14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VVDataHost : NSObject

+(VVDataHost *) inst;
+(VVDataHost *) create;

@end
