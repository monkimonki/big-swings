//
//  VVParametrsObject.h
//  FGApp
//
//  Created by macmini9 on 4/30/14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VVEntity.h"
#import "VVMutableURLRequest.h"

#ifdef PARSE
#import <Parse/Parse.h>
#endif

enum kMethodTypes{
    
    kMethodTypeGet=1,
    kMethodTypePut,
    KMethodTypePost,
    kMethodTypeDelete
    
};
@interface VVParametrsObject : NSObject{
    NSMutableArray * debugHideKey;
    NSMutableArray * debugSearhcKey;
    NSMutableArray * debugShowKeys;
    
}
+(VVParametrsObject *)create;
+(VVParametrsObject *)createWithEntity:(VVEntity*)entity;


#ifdef PARSE
-(PFQuery *)query;
-(PFObject*)pfObject:(VVObject*)obj;
#endif

-(void)addHideKey:(NSString*)key;
-(void)addSearchString:(NSString *)str forKey:(NSString*)key;
-(void)addShowKey:(NSString*)key;

-(VVMutableURLRequest*)urlRequest;
-(NSString*)stringFromEntity:(VVEntity*)entity;
@property (nonatomic) int pageOrigin;
@property (nonatomic) int pageSize;
@property (retain,nonatomic)NSMutableArray * searchKeys;
@property (nonatomic)NSMutableArray * totalIncludesKeys;
@property (nonatomic, retain) VVEntity * entity;
@property (nonatomic)enum kMethodTypes methodType;
@property (retain,nonatomic)NSMutableArray * additionalParams;
@property (retain,nonatomic)NSMutableArray* lessNumberParams;
@property (retain,nonatomic)NSMutableArray * highNumberParams;

@property (retain,nonatomic) NSString * endPoint;
@property  (retain,nonatomic)NSMutableArray * subQuerry;
@property (retain,nonatomic) NSMutableArray * additionalQueryParms;
@property (retain,nonatomic)NSMutableArray * matchQuerryParams;
@property (retain,nonatomic) NSMutableDictionary * body;
@property (retain,nonatomic) VVObject * objectToSave;
@property (retain,nonatomic) VVObject * objectToDelete;
@property (retain,nonatomic) Class returningObjectClass;
@property (retain,nonatomic) NSMutableArray * orQuerrys;
@property (retain,nonatomic) NSMutableArray * includeKeys;

@property (retain,nonatomic) NSMutableArray * matchesArrParam;

-(void)addAditionalQueryParam:(VVParametrsObject*)obj withKey:(NSString*)key andKeyInMainQuery:(NSString*)mainQueryKey;
-(void)addMathQueryParam:(VVParametrsObject*)obj forKeyInMainQuery:(NSString*)mainQueryKey;
-(void)addAditionalParam:(id)par forKey:(NSString*)key;
-(void)addLessParam:(NSNumber*)par forKey:(NSString*)key;
-(void)addHightParam:(NSNumber*)par forKey:(NSString*)key;
-(void)addArrayMatchesParam:(NSMutableArray*)arr forKey:(NSString*)key;
@end
