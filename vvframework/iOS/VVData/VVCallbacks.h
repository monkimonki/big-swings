//
//  VVCallbacks.h
//  FGApp
//
//  Created by macmini9 on 4/30/14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

typedef void (^VVDownloaderCallback)(BOOL success, VVDownloaderObject * obj, VVError * error);
typedef void (^VVArrayCallback)(BOOL success, NSArray * array);
typedef void (^VVObjectCallback)(BOOL success, id obj);

typedef void (^VVSuccessCallback) (BOOL scucess,VVDownloaderObject * obj,VVError*erorr);


typedef void (^VVDataCallback) (BOOL scucess,VVError*erorr,NSData * data);