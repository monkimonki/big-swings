//
//  VVLog.h
//  ChoooseData
//
//  Created by Aleksandr Padalko on 5/1/14.
//  Copyright (c) 2014 mm7. All rights reserved.
//


#import "NSMutableURLRequest+VVRequestCategory.h"
static NSString * NSStringFromRequest(NSMutableURLRequest* req){
    
    NSString * str=  [NSString stringWithFormat:@"\nHttp method:%@\nHeader:\n%@\nURL:\n%@",req.HTTPMethod,req.allHTTPHeaderFields,req.URL ];
    NSDictionary * dict=[req getDictionaryFromHTTPBody];
    
    if (dict.allKeys.count>0) {
       
        str=[NSString stringWithFormat:@"%@\nBody:\n%@",str,dict];
      
    }
    
    return str;
    
}


static void VVLog(NSString *format, ...)
{
    return;
    va_list args;
    va_start(args, format);
    
    CFShow((__bridge CFStringRef)[[NSString alloc] initWithFormat:format arguments:args]);
    
    va_end(args);
}