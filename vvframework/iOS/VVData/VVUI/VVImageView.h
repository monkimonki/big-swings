//
//  VVImageView.h
//  Chooos
//
//  Created by mm7 on 15.05.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import <UIKit/UIKit.h>
@class VVImageView;
@protocol VVImageViewDelagate <NSObject>

-(void)vvImageView:(VVImageView*)vvImageView didLoadImage:(UIImage*)image;

@end
@class VVFile;
@interface VVImageView : UIImageView
@property (nonatomic)BOOL imageLoaded;
@property (retain,nonatomic)VVFile * vvFile;

@property(retain,nonatomic)NSURL * URL;
@property (retain,nonatomic) NSString * urlString;
#ifdef PARSE

@property (retain,nonatomic)PFFile* file;

#endif
-(void)failToLoad;


@property (assign,nonatomic)id<VVImageViewDelagate> delegate;
@end
