//
//  VVCollectionViewController.m
//  Chooos
//
//  Created by mm7 on 12.05.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "VVCollectionViewController.h"

@interface VVCollectionViewController ()

@end

@implementation VVCollectionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - vvCollectionViewDelegate


-(VVColumnView *)collectionView:(VVCollectionView *)collectionView viewForColumn:(NSUInteger)column atIndexPath:(NSIndexPath *)indexPath andObject:(id)object{
    NSString *CellIdentifer = [NSString stringWithFormat:@"colitem"];
	VVColumnView  * cell=[collectionView getColumnView:CellIdentifer];
	if (!cell) {
        cell=[[VVColumnView alloc]initWithFrame:CGRectMake(0, 0, 320, 64)];
        cell.columnIdentifier=CellIdentifer;
    }
	return  cell;
}

-(NSUInteger)numberOfColumnsInSection:(NSUInteger)section{
    
    
    return 2;
    
}



-(CGFloat)collectionView:(VVCollectionView *)collectionView heightForLoadCellAtIndexPath:(NSIndexPath *)indexPath{
    
    
    return 20;
}
-(CGFloat)collectionView:(VVCollectionView *)collectionView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 64;
}
-(CGFloat)collectionView:(VVCollectionView *)collectionView heightForHeaderInSection:(NSInteger)section{
    return 0;
}
-(void)collectionView:(VVCollectionView *)collectionView didSelectRowAtIndexPath:(NSIndexPath *)indexPath withObject:(VVObject *)object{
    
}
-(UIView *)collectionView:(VVCollectionView *)collectionView viewForHeaderInSection:(NSInteger)section{
    
    
    return nil;
}

-(UITableViewCell *)collectionView:(VVCollectionView *)collectionView loadingCellViewForRowAtIndexPath:(NSIndexPath *)indexPath{
	static NSString *CellIdentifer = @"loadingCell";
	
	UITableViewCell  * cell=[collectionView dequeueReusableCellWithIdentifier:CellIdentifer];
	
	if (!cell) {
		cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifer];
        
        [cell.textLabel setTextAlignment:NSTextAlignmentCenter];
	}
    cell.textLabel.text=@"loading";
	return  cell;
}
-(void)collectionViewDidLoadData:(id)data forHeader:(NSUInteger)section{
    
}

@end
