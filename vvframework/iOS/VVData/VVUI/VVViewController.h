//
//  AAPViewController.h
//  FGApp
//
//  Created by mm7 on 14.04.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "VVTextFieldHelper.h"
#import "VVNavigationController.h"
@interface VVViewController : UIViewController
@property(nonatomic,retain)VVNavigationController *  vvNavigationController;
@property (nonatomic,retain)NSMutableArray * textFieldsArray;
@end
