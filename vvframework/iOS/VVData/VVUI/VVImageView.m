//
//  VVImageView.m
//  Chooos
//
//  Created by mm7 on 15.05.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "VVImageView.h"

#import "VVFile.h"
@implementation VVImageView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
-(void)setVvFile:(VVFile *)vvFile{
    _vvFile=vvFile;
    if (vvFile.url) {
        [self setURL:vvFile.url];
        return;
    }
        #ifdef PARSE
    if (vvFile.file){
        [self setFile:vvFile.file];
    }
    #endif
}

#ifdef PARSE
-(void)setFile:(PFFile *)file{
    _file=file;
        _imageLoaded=NO;
    if (!_vvFile) {
            _vvFile=[VVFile createWithFile:_file];
    }else{
        _vvFile.file=file;
    }

   
    [[VVServer inst]loadFile:_vvFile callback:^(BOOL suc,VVError * er,NSData* data){
        
        
        
        if (suc && data) {
                       _imageLoaded=YES;
             self.image=nil;
            UIImage * img=[[UIImage alloc] initWithData:data];
            
            [self setImage:img];
            [self.delegate vvImageView:self didLoadImage:img];
            img=nil;
            
        }else {
            [self failToLoad];
        }
    }];
}
#endif

-(void)setURL:(NSURL *)URL{
    _URL=URL;
    _imageLoaded=NO;
    _vvFile=[VVFile createWithUrl:_URL ];
    
    [[VVServer inst]loadFile:_vvFile callback:^(BOOL suc,VVError * er,NSData* data){
       
        
        
        if (suc && data) {
            _imageLoaded=YES;
            UIImage * img=[UIImage imageWithData:data];
            
            [self setImage:img];
            
            
        }else {
            [self failToLoad];
        }
    }];
    
    
}

-(void)setUrlString:(NSString *)urlString{
    _urlString=urlString;
    
    VVFile * f=[VVFile createWithUrl:_URL ];
        _imageLoaded=NO;
    [[VVServer inst]loadFile:f callback:^(BOOL suc,VVError * er,NSData* data){
        
        
        
        if (suc && data) {
               _imageLoaded=YES;
            UIImage * img=[[UIImage alloc]initWithData:data];
            
            [self setImage:img];
            
            
        }else {
            [self failToLoad];
        }
    }];
    
    
}

-(void)failToLoad{
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
