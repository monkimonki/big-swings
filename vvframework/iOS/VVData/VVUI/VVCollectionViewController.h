//
//  VVCollectionViewController.h
//  Chooos
//
//  Created by mm7 on 12.05.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "VVViewController.h"
#import "VVCollectionView.h"
@interface VVCollectionViewController : VVViewController<VVCollectionViewDelegate>

@end
