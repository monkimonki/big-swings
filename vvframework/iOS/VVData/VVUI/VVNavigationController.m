//
//  AAPNavigationController.m
//  FGApp
//
//  Created by mm7 on 14.04.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "VVNavigationController.h"
#import "VVViewController.h"

@interface VVNavigationController ()

@end

@implementation VVNavigationController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated
{
	if ([viewController isKindOfClass:[VVViewController class]]) {
		((VVViewController*) viewController).vvNavigationController = self;
	}
	[super pushViewController:viewController animated:animated];
}

@end
