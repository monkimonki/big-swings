//
//  VVListTableViewController.h
//  Chooos
//
//  Created by mm7 on 08.05.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//


#import "VVViewController.h"
#import "VVListTableView.h"
@interface VVListTableViewController : VVViewController<VVListTableViewDelegate>{
    
    NSMutableArray * listTableViewViews;
    
}

@property (retain,nonatomic)VVListTableView *listTableView;
-(void)setViewToHeaderView:(UIView*)v toSection:(NSUInteger)section;
-(void)addViewToCell:(UIView*)v toSection:(NSUInteger)section;
-(void)setViewtoCell:(UIView *)v toSection:(NSUInteger)section atIndex:(NSUInteger)index;


-(UIView*)getViewOfHeaderInSection:(NSUInteger)section;
-(UIView*)getViewOfCellInSection:(NSUInteger)section atIndex:(NSUInteger)index;

-(UIView *)getViewOfCellInIndexPath:(NSIndexPath*)indexPath;



-(void)cleanViews;
-(void)cleanCellsViewInSection:(NSUInteger)section;

@end
