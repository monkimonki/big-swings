//
//  VVCollumnView.h
//  Chooos
//
//  Created by mm7 on 12.05.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VVColumnView : UIView
@property (retain,nonatomic)NSString * columnIdentifier;
@property (nonatomic)BOOL reused;
@end
