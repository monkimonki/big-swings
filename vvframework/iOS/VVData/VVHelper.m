//
//  VVHelper.m
//  ChoooseData
//
//  Created by Aleksandr Padalko on 5/1/14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "VVHelper.h"
@implementation VVObject (Dictionary)

+(VVObject*)objectFromDictionary:(NSDictionary*)dict{
    
    VVObject * object=[[NSClassFromString([VVHelper getClassNameFromEntity:[dict valueForKey:[VVHelper propNameToKey:@"entityName"]]]) alloc]initWithDictionary:dict];
    
    return object;
    
    
}
+(VVObject*)objectFromPFObject:(PFObject*)obj{
    
   return [VVObject objectFromDictionary:[VVHelper dictFromPFObject:obj]];
    

    
    
}
@end

@implementation VVHelper
static NSDictionary * entityKeys;
static NSDictionary * propKeys;
static NSDictionary * classKeys;

-(NSArray*)convertArray:(NSArray*)arr{
    NSMutableArray * newArr=[[NSMutableArray alloc]init];
    for (id obj in arr) {
        if ([obj isKindOfClass:[NSDictionary class]]) {
            [newArr addObject:  [self convertDictionary:obj]];
        }else if([obj isKindOfClass:[NSArray class]]){
            [newArr addObject:[self convertArray:obj]];
        }else if([obj isKindOfClass:[PFFile class]]){
            [newArr addObject:[VVFile createWithFile:obj]];
        }else{
            [newArr addObject:obj];
        }
        
    }
    
    return newArr;
}
-(NSDictionary*)convertDictionary:(NSDictionary*)arr{
    NSMutableDictionary * newDict=[[NSMutableDictionary alloc] init];
    for (NSString * key in arr) {
        id obj=[arr valueForKey:key];
        if ([obj isKindOfClass:[NSDictionary class]]) {
            
            [newDict setObject:[self convertDictionary:obj] forKey:key];
            
        }else if([obj isKindOfClass:[NSArray class]]){
            [newDict setObject:[self convertArray:obj] forKey:key];
        }else if([obj isKindOfClass:[PFFile class]]){
            [newDict setObject:[VVFile createWithFile:obj] forKey:key];
        }else{
            [newDict setObject:obj forKey:key];
        }
        
    }
    
    return newDict;
    
}
+(NSDictionary*)convertDictionaryToVV:(NSDictionary*)dict{
    if (dict.allKeys.count==0) {
        return nil;
    }
    
    if ([[dict valueForKey:@"__type"] isEqualToString:@"File"]) {
        return [VVFile createWithDictionary:dict];
    }else{
          NSMutableDictionary * newDict=[[NSMutableDictionary alloc] init];
    for (NSString * key in dict) {
        id obj=[dict valueForKey:key];
        if ([obj isKindOfClass:[NSDictionary class]]) {
            
            [newDict setObject:[self convertDictionaryToVV:obj] forKey:key];
            
        }else if([obj isKindOfClass:[NSArray class]]){
            [newDict setObject:[self convertArrayToVV:obj] forKey:key];
        }else if([obj isKindOfClass:[PFFile class]]){
            [newDict setObject:[VVFile createWithFile:obj] forKey:key];
        }else{
            [newDict setObject:obj forKey:key];
        }
        

    }
       
         return newDict;
    }
   
}
+(NSArray*)convertArrayToVV:(NSArray*)array{
    NSMutableArray * newArr=[[NSMutableArray alloc]init];
    for (id obj in array) {
        if ([obj isKindOfClass:[NSDictionary class]]) {
            [newArr addObject:  [self convertDictionaryToVV:obj]];
        }else if([obj isKindOfClass:[NSArray class]]){
            [newArr addObject:[self convertArrayToVV:obj]];
        }else if([obj isKindOfClass:[PFFile class]]){
            [newArr addObject:[VVFile createWithFile:obj]];
        }else{
            [newArr addObject:obj];
        }
        
    }
    
    return newArr;
    
    
}

+(NSDictionary*)convertVVDictToDictionary:(NSDictionary*)dict{
    

        NSMutableDictionary * newDict=[[NSMutableDictionary alloc] init];
        for (NSString * key in dict) {
            id obj=[dict valueForKey:key];
            if ([obj isKindOfClass:[NSDictionary class]]) {
                
                [newDict setObject:[self convertVVArrayToDictionary:obj] forKey:key];
                
            }else if([obj isKindOfClass:[NSArray class]]){
                [newDict setObject:[self convertVVArrayToDictionary:obj] forKey:key];
            }else if([obj isKindOfClass:[VVFile class]]){
                [newDict setObject:[(VVFile*)obj formDictionary] forKey:key];
            }else{
                [newDict setObject:obj forKey:key];
            }
            
            
        }
        
        return newDict;
 
    
}
+(NSArray*)convertVVArrayToDictionary:(NSArray*)array{
    NSMutableArray * newArr=[[NSMutableArray alloc]init];
    for (id obj in array) {
        if ([obj isKindOfClass:[NSDictionary class]]) {
            [newArr addObject:  [self convertVVDictToDictionary:obj]];
        }else if([obj isKindOfClass:[NSArray class]]){
            [newArr addObject:[self convertVVArrayToDictionary:obj]];
        }else if([obj isKindOfClass:[VVFile class]]){
            [newArr addObject:[(VVFile*)obj formDictionary]];
        }else{
            [newArr addObject:obj];
        }
        
    }
    
    return newArr;
}
+(NSDictionary*)convertDictionaryToPF:(NSDictionary*)dict{
    if (dict.allKeys.count==0) {
        return nil;
    }
    
  
    
    if ([[dict valueForKey:@"__type"] isEqualToString:@"File"]) {
        return [VVFile createPFFile:dict];
    }else{
        NSMutableDictionary * newDict=[[NSMutableDictionary alloc] init];
        for (NSString * key in dict) {
            id obj=[dict valueForKey:key];
            if ([obj isKindOfClass:[NSDictionary class]]) {
                id val=[self convertDictionaryToPF:obj];
                if (val)
                [newDict setObject:val forKey:key];
                
            }else if([obj isKindOfClass:[NSArray class]]){
                [newDict setObject:[self convertArrayToPF:obj] forKey:key];
            }else if([obj isKindOfClass:[PFFile class]]){
                [newDict setObject:obj forKey:key];
            }else if([obj isKindOfClass:[NSData class]]){
                return [PFFile fileWithName:@"photo.jpg" data:obj contentType:@"jpg"];
            }else{
                [newDict setObject:obj forKey:key];
            }
            
            
        }
        
        return newDict;
    }
}
+(NSArray*)convertArrayToPF:(NSArray*)array{
    NSMutableArray * newArr=[[NSMutableArray alloc]init];
    for (id obj in array) {
        if ([obj isKindOfClass:[NSDictionary class]]) {
            [newArr addObject:  [self convertDictionaryToPF:obj]];
        }else if([obj isKindOfClass:[NSArray class]]){
            [newArr addObject:[self convertArrayToPF:obj]];
        }else{
            [newArr addObject:obj];
        }
        
    }
    return newArr;
}
//}








+(NSString *)getObjectEntity:(NSObject*)obj{
    
    NSString*className=NSStringFromClass(obj.class);
    
    
    
    
    return [self getEntityFromName:className];
}
+(NSString*)propNameToKey:(NSString*)propName{
    
    
    if(!propKeys){
        propKeys=[[NSDictionary alloc]initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"PropKeys" ofType:@".plist"]];
        
    }
    
    
    NSString * val=[propKeys valueForKey:propName];
    
    if (!val) {
        return propName;
    }
    
    return val;
    
}


+(NSString*)keyToPropName:(NSString*)key{
    
    
    if(!propKeys){
        propKeys=[[NSDictionary alloc]initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"PropKeys" ofType:@".plist"]];
        
    }
    
    
    NSString * val=[[propKeys allKeysForObject:key] firstObject];
    
    if (!val) {
        return key;
    }
    
    return val;
    
}
+(NSString*)getEntityFromName:(NSString*)str{
    if (!entityKeys) {
        entityKeys=[[NSDictionary alloc]initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"EntityKeys" ofType:@".plist"]];
    }
    
    
    NSString * result=[entityKeys valueForKey:str];
    
    if (!result) {
        return str;
    }
    
    
    return result;
    
}
+(NSString *)getClassNameFromEntity:(NSString*)str{
    if (!classKeys) {
        classKeys=[[NSDictionary alloc]initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"ClassKeys" ofType:@".plist"]];
    }
    
    
 //   NSString * result=[[entityKeys allKeysForObject:str] firstObject];
   NSString * result=[classKeys valueForKey:str];
    if (!result) {
        
        if (!entityKeys) {
            entityKeys=[[NSDictionary alloc]initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"EntityKeys" ofType:@".plist"]];
        }
        
         result=[[entityKeys allKeysForObject:str] firstObject];
        if (result) {
            return result;
        }
        return str;
    }
    
    
    return result;
}

//
+ (NSData *)formEncodedDataDictionary:(NSDictionary*)dict
{
    NSMutableString *body = [NSMutableString string];
 
    
    for (NSString *key in dict) {
        NSString *value = [dict objectForKey:key];
        if ((id) value == [NSNull null]) continue;
        
        if (body.length != 0)
            [body appendString:@"&"];
        
        if ([value isKindOfClass:[NSString class]])
            value = [self URLEncodedString:value];
        
        [body appendFormat:@"%@=%@", [self URLEncodedString:key], value];
    }

    return [body dataUsingEncoding:NSUTF8StringEncoding];
}
+ (NSString *)URLEncodedString:(NSString *)string
{
    NSMutableString *output = [NSMutableString string];
    const unsigned char *source = (const unsigned char *) [string UTF8String];
    NSInteger sourceLen = strlen((const char *) source);
    for (int i = 0; i < sourceLen; ++i) {
        const unsigned char thisChar = source[i];
        if (thisChar == ' ')
            [output appendString:@"+"];
        else if (thisChar == '.' || thisChar == '-' || thisChar == '_' || thisChar == '~' ||
                 (thisChar >= 'a' && thisChar <= 'z') ||
                 (thisChar >= 'A' && thisChar <= 'Z') ||
                 (thisChar >= '0' && thisChar <= '9'))
            [output appendFormat:@"%c", thisChar];
        else
            [output appendFormat:@"%%%02X", thisChar];
    }
    return output;
}


#pragma mark- local BD search Helper


+(NSMutableArray*)findCollection:(NSString*)colName andSearchKey:(NSString*)searchKey andSearchId:(NSString*)objId andSearchLim:(int)lim
{
    NSMutableArray * arr=[[NSMutableArray alloc]init];
    
    NSArray * searchEntity=[[VVServer inst].localServerData valueForKey:colName];
    
    
    for (VVObject * obj in searchEntity) {
        NSMutableDictionary * dict=[obj logProp];
        if (arr.count>=lim) {
            break;
        }
        if (searchKey) {
            if ( [[[dict valueForKey:searchKey] valueForKey:@"id"] isEqualToString:objId]) {
                
                [dict removeObjectForKey:searchKey];
                        [arr addObject:dict];
            }
            
    
            
        }else{
            
            if ([[dict valueForKey:@"id"] isEqualToString:objId]) {
                [arr addObject:dict];
            }
        }
        
    }
    
    
    
    return arr;
    
    
}
+(void)deleteAllFilesInDerectory:(NSString*)dere{
    NSFileManager *fileMgr = [[NSFileManager alloc] init];
    NSError *error = nil;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];

    NSArray *directoryContents = [fileMgr contentsOfDirectoryAtPath:documentsDirectory error:&error];
    if (error == nil) {
        for (NSString *path in directoryContents) {
            NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:path];
            BOOL removeSuccess = [fileMgr removeItemAtPath:fullPath error:&error];
            if (!removeSuccess) {
                // Error handling
            
            }
        }
    } else {
        // Error handling
        
    }
}
+(NSString*)createFullPath:(NSString*)derec andFileName:(NSString*)fn{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString * fileName;
    
    
    if ([derec length]>0) {
        if (![[NSFileManager defaultManager] fileExistsAtPath:[documentsDirectory stringByAppendingPathComponent:derec]]){
            
          //  [[NSFileManager defaultManager] createDirectoryAtPath:[documentsDirectory stringByAppendingPathComponent:derec] withIntermediateDirectories:NO attributes:nil error:nil];

            
            NSArray * arr=[derec componentsSeparatedByString:@"/"];
            NSString * fPath;
           for (int a=0; a<arr.count; a++) {
               if (a==0) {
                    fPath=[NSString stringWithFormat:@"/%@",[arr objectAtIndex:a]];
                }else{
                   fPath=[NSString stringWithFormat:@"%@/%@",fPath,[arr objectAtIndex:a]];
                }
                if (![[NSFileManager defaultManager] fileExistsAtPath:[documentsDirectory stringByAppendingPathComponent:fPath]]){
 [[NSFileManager defaultManager] createDirectoryAtPath:[documentsDirectory stringByAppendingPathComponent:fPath] withIntermediateDirectories:NO attributes:nil error:nil];
               }
            }
            
            
        }
        
        
        fileName=[NSString stringWithFormat:@"%@/%@",derec,fn];
    }else{
        fileName=fn;
    }
    NSString * last=[documentsDirectory stringByAppendingPathComponent:fileName];
    return  last;
    
}
+(void)saveDataToDerectory:(NSString*)derec andFileName:(NSString*)fn andFile:(NSData*)data{
    
    [data writeToFile:[self createFullPath:derec andFileName:fn] atomically:YES];
}
+(BOOL)isExsistFileInDerectory:(NSString*)derec andFileName:(NSString*)fn{
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:[self createFullPath:derec andFileName:fn]])
        return YES;
    
    return NO;
}
+(NSData*)loadDataFromDerectory:(NSString*)derec andFileName:(NSString*)fn{
    NSData * d=[[NSData alloc] initWithContentsOfFile:[self createFullPath:derec andFileName:fn]];
    
    return d;
}

+(NSMutableDictionary*)dictFromPFObject:(PFObject*)obj{
    NSMutableDictionary* dict=[[NSMutableDictionary alloc]init];
 
    [obj setValue:obj.parseClassName forKey:[VVHelper propNameToKey:@"entityName"]];
    
    NSLog(@"%@",obj.allKeys);
    if (obj.allKeys.count==0 ) {
        
    }else{
    for (NSString* key in obj.allKeys) {
        id val=[obj valueForKey:key];
        if ([val isKindOfClass:[PFObject class]]) {
            val=   [self dictFromPFObject:[obj valueForKey:key]];
        }else if ([val isKindOfClass:[PFFile class]]){
            
            val=[[VVFile alloc]initWithPFFile:val];
            
        }else if ([val isKindOfClass:[NSArray class]]){
            val=  [self convertArray:val];
        }else if([val isKindOfClass:[NSDictionary class]]){
            
            val= [self convertDictionary:val];
            
        }
        
        [dict setValue:val forKey:key];
    }
    [dict setValue:obj.createdAt forKey:@"createdAt"];
    }
    [dict setValue:obj.objectId forKey:[VVHelper propNameToKey:@"objectId"]];
    return dict;
    
}
+(NSArray*)convertArray:(NSArray*)arr{
    NSMutableArray * newArr=[[NSMutableArray alloc]init];
    for (id obj in arr) {
        if ([obj isKindOfClass:[NSDictionary class]]) {
            [newArr addObject:  [self convertDictionary:obj]];
        }else if([obj isKindOfClass:[NSArray class]]){
            [newArr addObject:[self convertArray:obj]];
        }else if([obj isKindOfClass:[PFFile class]]){
            [newArr addObject:[[VVFile createWithFile:obj] formDictionary]];
        }else{
            [newArr addObject:obj];
        }
        
    }
    
    return newArr;
}
+(NSDictionary*)convertDictionary:(NSDictionary*)arr{
    NSMutableDictionary * newDict=[[NSMutableDictionary alloc] init];
    for (NSString * key in arr) {
        id obj=[arr valueForKey:key];
        if ([obj isKindOfClass:[NSDictionary class]]) {
            
            [newDict setObject:[self convertDictionary:obj] forKey:key];
            
        }else if([obj isKindOfClass:[NSArray class]]){
            [newDict setObject:[self convertArray:obj] forKey:key];
        }else if([obj isKindOfClass:[PFFile class]]){
            [newDict setObject:[[VVFile createWithFile:obj] formDictionary] forKey:key];
        }else{
            [newDict setObject:obj forKey:key];
        }
        
    }
    
    return newDict;
    
}
@end
